package PDFSign.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class WriteToTextFile {
	
	File file = new File("C:/client.txt");
	FileWriter fw;
	BufferedWriter bw;
	
	public WriteToTextFile() {
		
		try {
			
			fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public synchronized void write(String txt) {
		
		try {

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			
			bw.write(txt);
			bw.newLine();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public void close() {
		
		try {
			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
}
