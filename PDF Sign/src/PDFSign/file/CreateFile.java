package PDFSign.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class CreateFile {
	
	public File create(String destinationPath, String fileName) throws IOException{
		
		File file = new File(destinationPath+"/"+fileName);

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		return file;
		
	}
	
	
	public Writer getBuffWrite(File file) throws IOException{
		/*FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		return bw;*/
		
		Writer wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsoluteFile()), "ISO8859_1"));		//Set enconding to "ISO8859_1" for write ASCII if hex > 80
		return wr;

		
	}
	

}
