package PDFSign.file;

import java.io.File;
import javax.swing.JFileChooser;

public class BrowseFile {
	
	public String getDirectory() {

        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        String parentPath = f.getAbsolutePath();
        
        return parentPath;
                
    }
    
    
    public String getFilePath() {

        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        String filePath = f.getAbsolutePath();
        
        return filePath;
        
    }

}
