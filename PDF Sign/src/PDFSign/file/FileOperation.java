package PDFSign.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class FileOperation {

	public static boolean Operate(String srFile, String dtFile, int Operation) throws IOException {
		
		/*
		 * Operation
		 *   1 = Move
		 *   2 = Remove
		 * 	 3 = Copy
		 */
		
		 boolean result = false;
	
		 File f1 = new File(srFile);
		 
		 if(Operation==1){
			File f2 = new File(dtFile);
	     	InputStream in = new FileInputStream(f1);
			OutputStream out = new FileOutputStream(f2);
			byte[] buf = new byte[1024];
			
			int len;
			while ((len = in.read(buf)) > 0){
			    out.write(buf, 0, len);
			}
			in.close();
			out.close();
			f1.delete();
			
		}
		else if(Operation==2) {
			f1.delete();
		}
		else if(Operation==3) {
			File f2 = new File(dtFile);
	     	InputStream in = new FileInputStream(f1);
			OutputStream out = new FileOutputStream(f2);
			byte[] buf = new byte[1024];
			
			int len;
			while ((len = in.read(buf)) > 0){
			    out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
		
		result = true;
		
		return result;
		
	}

	
	public boolean isFileExist(String filePath) {
		
		 File f = new File(filePath);
		 return f.exists();
		 
	}
	
	
	
	public File getFile(String filePath) {
		
		return new File(filePath);
		
	}
	
	
	public String getNameFromPath(String filePath) {
		
		return new File(filePath).getName();
		
	}
	
	
	public String getExtensionFromName(String fileName) {
		
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
		    extension = fileName.substring(i+1);
		}
		
		return extension;
		
	}
	
	
	public String getNameWithoutExtension(String fileName) {
		
		String name = "";
		
		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			name = fileName.substring(0, i);
		}
		
		return name;
		
	}
	
	
	public boolean isFileNotUsedByAnotherProcess(String filePath) {
		
		boolean result = true;
		
		/*try {
			
			FileInputStream fis = new FileInputStream(filePath);
			fis.close();
			
		} catch (FileNotFoundException e) {
			if(e.getMessage().contains("(The process cannot access the file because it is being used by another process)")) {
				result = false;
			}
			else {
				e.printStackTrace();
				result = false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			result = false;
		}*/
		
		try {
			FileChannel channel = new RandomAccessFile(new File(filePath), "rw").getChannel();
			FileLock lock = channel.tryLock();
			
			if (lock != null) {
			    try {
			        // read the file
			    	result = true;
			    } finally {
			        lock.release();
			    }
			} else {
			    // some other process has locked the file for some reason
				result = false;
			}
		
			channel.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			result = false;
		} catch (IOException e) {
			e.printStackTrace();
			result = false;
		}
		
		return result;
		
	}
	
	

	public String readTextFile(String srcPath) throws IOException{
		
		String result = "";
        File file = new File(srcPath);
        byte[] bFile = new byte[(int) file.length()];
        FileInputStream fileInputStream = new FileInputStream(file);
        
        fileInputStream.read(bFile);
	    fileInputStream.close();
	    
	    for (int i = 0; i < bFile.length; i++) {
	    	result += String.valueOf((char)bFile[i]);
	    }
		
		return result;
		
	}

	
}
