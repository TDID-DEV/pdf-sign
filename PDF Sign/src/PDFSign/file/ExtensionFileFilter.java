package PDFSign.file;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class ExtensionFileFilter extends FileFilter{

	String Description;
	String[] Extensions;

	
	public ExtensionFileFilter(String Description, String[] Extensions) {
		
		this.Description = Description;
		this.Extensions = Extensions;
		
	}


	public boolean accept(File arg0, String arg1) {
		return false;
	}
	
	
	
	@Override
	public boolean accept(File file) {
		
		if(file.isDirectory())
			return true;
		
		else if(file.isFile()) {
			
			String fileName = file.getName();
			
			
				
			int i;
			for(i=0;i<Extensions.length;i++) {
					
				if(fileName.toLowerCase().endsWith(Extensions[i].toLowerCase())) {
						
					break;
				}
						
			}
				
			if(i<Extensions.length) 
				return true;
		
			else
				return false;
			
		}
		
		else
			return false;
	}

	
	
	@Override
	public String getDescription() {
		return Description;
	}
	
	
}
