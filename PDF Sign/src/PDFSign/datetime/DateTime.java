package PDFSign.datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;



public class DateTime {

	
	public String getMonth(){
		String month = "";
		DateFormat dateFormat = new SimpleDateFormat("MM");
		Date date = new Date();
		month = dateFormat.format(date);
		
		return month;
		
	}
	
	
	
	public String getYear(){
		String year = "";
		DateFormat dateFormat = new SimpleDateFormat("yy", new Locale("en", "TH"));
		Date date = new Date();
		year = dateFormat.format(date);
		
		return year;
		
	}
	
	
	
	public String getDateTime(){
		String dateTime = "";
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss", new Locale("en", "TH"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+7"));
		Date date = new Date();
		dateTime = dateFormat.format(date);
		
		return dateTime;
		
	}
	
	
	
	public String getDateTime(String format){
		String dateTime = "";
		DateFormat dateFormat = new SimpleDateFormat(format, new Locale("en", "TH"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+7"));
		Date date = new Date();
		dateTime = dateFormat.format(date);
		
		return dateTime;
		
	}
	
	
	
	public String getExpiryDateOfCert(int ExpiryTime){
		String month = getMonth();
		String year = getYear();
		String mmyy;
		
		year = String.valueOf((Integer.parseInt(year)+ExpiryTime));
		mmyy = month+year;
		
		return mmyy;
		
	}
	
	
	
}
