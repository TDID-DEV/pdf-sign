package PDFSign.webService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.ProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.spec.InvalidKeySpecException;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.exceptions.BadPasswordException;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.encryption.DecryptPassword;
import PDFSign.errorCode.ErrorCause;
import PDFSign.errorCode.ErrorCode;
import PDFSign.exception.DigSigException;
import PDFSign.file.FileOperation;
import PDFSign.key.SecretKey;
import PDFSign.properties.PropertiesManagement;
import PDFSign.signature.PDFSignature;
import PDFSign.x509certificate.X509Cert;
import iaik.pkcs.pkcs11.Slot;
import PDFSign.token.TokenEnvironment;
import PDFSign.token.TokenKeyStore;
import PDFSign.token.TokenProvider;


//Service Implementation
@WebService(endpointInterface = "PDFSign.webService.SignSignature")
public class SignSignatureImpl implements SignSignature {
	
	final static Logger logger = Logger.getLogger(SignSignatureImpl.class);
	FileOperation fileOperation = new FileOperation();
	
	@Override
	public byte[] sign(String onlineRecordNumber, String fileName, byte[] file) throws DigSigException {
		
		PropertiesManagement prop = new PropertiesManagement();
		PropertiesManagement limitProp = new PropertiesManagement();
		
		boolean shouldRetry = false;
		//boolean shouldAddTokenProviderAgain = false;
		
		byte[] signedPDF = null;
		
		String llx = null;
		String lly = null;
		String urx = null;
		String ury = null;
		String fontSize = null;
		String page = null;
		String reason = null;
		String location = null;
		String signatureImg = null;
		
		try {
			
			// Read properties
			int count = Integer.parseInt(prop.getProperties("digsig.count"));
			
			for (int i = 1; i <= count; i++) {
				
				if(prop.getProperties("digsig."+i+".onlineRecordNumber").equalsIgnoreCase(onlineRecordNumber)) {
					
					llx = prop.getProperties("digsig."+i+".llx");
					lly = prop.getProperties("digsig."+i+".lly");
					urx = prop.getProperties("digsig."+i+".urx");
					ury = prop.getProperties("digsig."+i+".ury");
					fontSize = prop.getProperties("digsig."+i+".fontSize");
					page = prop.getProperties("digsig."+i+".page");
					reason = prop.getProperties("digsig."+i+".reason");
					location = prop.getProperties("digsig."+i+".location");
					signatureImg = prop.getProperties("digsig."+i+".signatureImg");
					String corporateID = prop.getProperties("digsig."+i+".corporateID");
					
					String keyAlias = prop.getProperties("digsig."+i+".keyAlias");
					String hsmSerial = prop.getProperties("digsig."+i+".hsmSerial");
					//Decrypt password
					String hsmPassword = new DecryptPassword().decrypt(prop.getProperties("digsig."+i+".hsmPassword"), new SecretKey().createSecretKey());
					
					String withTSA = prop.getProperties("digsig."+i+".withTSA");
					String tsaURL = prop.getProperties("digsig."+i+".tsaURL");
					String tsaUser = prop.getProperties("digsig."+i+".tsaUser");
					String tsaPassword = "";
					
					if(prop.getProperties("digsig."+i+".tsaPassword").equalsIgnoreCase("")) {
						tsaPassword = "";
					}
					else {
						//Decrypt password
						tsaPassword = new DecryptPassword().decrypt(prop.getProperties("digsig."+i+".tsaPassword"), new SecretKey().createSecretKey());
					}
					

					if(fileName.endsWith(".pdf") || fileName.endsWith(".PDF")) {
						
						int waitTimeForRetry = 3;
						int maximumRetry = 3;
						int numberOfRetries = 0;
						
						boolean isFileEncrypted = false;
						String password = "";
						
						/*TokenProvider tokenProvider = new TokenProvider();
						Provider provider = null;*/
						
						do {
							
							try {
								
								// Get maximum retry and wait time from properties
								maximumRetry = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.maximumRetry"));
								waitTimeForRetry = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.waitTimeForRetry"));
								
								
								
								// sleep before next retry
								if(numberOfRetries > 0) {
									Thread.sleep(waitTimeForRetry*1000);
								}
								
								
								/*if(shouldAddTokenProviderAgain) {
									tokenProvider.retryAddTokenProvider(provider, i);
								}*/
								
								
								// Read cert
								TokenProvider tokenProvider = new TokenProvider();
								TokenKeyStore tokenKeyStore = new TokenKeyStore();
								Provider provider = tokenProvider.getTokenProviderWithHSMSerial(hsmSerial);
								KeyStore ks = tokenKeyStore.createTokenKeyStore(provider, hsmPassword);
								
								
								// Get private key and cert from HSM
								PrivateKey privateKey = (PrivateKey) ks.getKey(keyAlias, hsmPassword.toCharArray());
								Certificate cert = ks.getCertificate(keyAlias);
								Certificate[] chain = ks.getCertificateChain(keyAlias);
								X509Cert x509Cert = new X509Cert(cert.getEncoded());
								x509Cert.checkValidity();		// Check validity signing cert
								
								try {
									// Verify certificate with trust store in license (Test/Prod)
									x509Cert.VerifyCertificate();
								} catch (NullPointerException e1) {
									throw new CertificateException("Cannot verify certificate with trust store in license");
								}

									
								// Check certificate with licenss
								if(x509Cert.checkCertWithLicense() && Main.LicenseFile.IsCorporateTaxInLicenseFile(corporateID)) {
								
							
									// File protection
									password = getPasswordFromFileName(fileName);
									
									
									// Sign
									PDFSignature pdfSig = new PDFSignature(privateKey, chain, password);
									signedPDF = pdfSig.setAppearanceAndSign(file, llx, lly, urx, ury, fontSize, page, reason, location, signatureImg, withTSA, tsaURL, tsaUser, tsaPassword);
									
					    			System.out.println("[SUCCESSFUL] ................ Signing file "+fileName+" complete");
									logger.info("[SUCCESSFUL] Signing "+fileName+" complete");
									
									// Set should retry to false when all done
							    	shouldRetry = false;
							    	//shouldAddTokenProviderAgain = false;
							    	
						    	
								}
								else {
									shouldRetry = false;
									System.out.println("[FAILED] ........................ Transaction error code: " + ErrorCode.ERROR_312+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_312+" | Error cause: "+ErrorCause.ERROR_312+" | File name: "+fileName);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_312);
								}
						    	
						    	
								
							} catch (FileNotFoundException e) {
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ Transaction error code: " + ErrorCode.ERROR_301+" | File name: "+fileName);
								logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								
								throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
								
							} catch (BadPasswordException e) {
								
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_306+" | Error cause: "+ErrorCause.ERROR_306+" | File name: "+fileName);
								logger.error("[FAILED] Error code: " + ErrorCode.ERROR_306+" | Error cause: "+ErrorCause.ERROR_306+" | File name: "+fileName, e);
								
								throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_306);
							
							} catch (IOException e) {
								
								e.printStackTrace();
								
								if(numberOfRetries == maximumRetry) {
								
									if(e.getMessage().equalsIgnoreCase("Connection refused: connect")) {
										
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_314);
										
									}
									else if(e.getMessage().equalsIgnoreCase("Connection timed out: connect")) {
										
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_314);
										
									}
									else if(e.getMessage().equalsIgnoreCase("Network is unreachable: connect")) {
										
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_314);
										
									}
									else if(e.getMessage().equalsIgnoreCase("No route to host: connect")) {
										
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_314);
										
									}
									else if(e.getMessage().equalsIgnoreCase("sun.security.validator.ValidatorException: PKIX path validation failed: java.security.cert.CertPathValidatorException: timestamp check failed")) {
										
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_314);
										
									}
									else {
										
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
										
									}
									
								}
								else {
									shouldRetry = true;
								}
								
								
							} catch(ExceptionConverter e) {
								
								shouldRetry = false;
								e.printStackTrace();
								
								if(e.getMessage().equalsIgnoreCase("Connection refused: connect")) {
									
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_319+" | Error cause: "+ErrorCause.ERROR_319+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_319+" | Error cause: "+ErrorCause.ERROR_319+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_319);
									
								}
								else {
									
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
									
								}
								
							} catch (InterruptedException e) {
								
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
								logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								
								throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
								
							} catch(ProviderException e) {
								e.printStackTrace();
								
								if(e.getMessage().contains("Token has been removed") || e.getMessage().contains("Initialization failed")) {
									
									shouldRetry = false;
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_305);
									
									/*if(numberOfRetries == maximumRetry) {
										shouldRetry = false;
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_305);
									}
									else {
										shouldRetry = true;
										shouldAddTokenProviderAgain = true;
									}*/
									
								}
								else {
									shouldRetry = false;
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
									
								}
								
							} catch (KeyStoreException e) {
								e.printStackTrace();
								
								if(e.getMessage().contains("PKCS11 not found")) {
									
									shouldRetry = false;
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_305);
									
									/*if(numberOfRetries == maximumRetry) {
										shouldRetry = false;
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305);
										logger.error("[FAILED] Error code: " + ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305, e);
										
										throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_305);
									}
									else {
										shouldRetry = true;
										shouldAddTokenProviderAgain = true;
									}*/
									
								}
								else {
									shouldRetry = false;
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
									
								}
								
							} catch (CertificateExpiredException e) {
								
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_302+" | Error cause: "+ErrorCause.ERROR_302+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_302+" | Error cause: "+ErrorCause.ERROR_302+" | File name: "+fileName, e);
					    		
					    		throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_302);
								
								
							} catch (CertificateNotYetValidException e) {
								
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_303+" | Error cause: "+ErrorCause.ERROR_303+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_303+" | Error cause: "+ErrorCause.ERROR_303+" | File name: "+fileName, e);
					    		
					    		throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_303);
								
								
							} catch (CertificateException e) {

								shouldRetry = false;
								e.printStackTrace();
								
								if(e.getMessage().equalsIgnoreCase("Cannot verify certificate with trust store in license")) {
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_310+" | Error cause: "+ErrorCause.ERROR_310+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_310+" | Error cause: "+ErrorCause.ERROR_310+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_310);
								}
								else {
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
									logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
									
									throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
								}
								
							} catch (DocumentException e) {

								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
								logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								
								throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
								
							} catch (GeneralSecurityException e) {
								
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
								logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								
								throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
								
							} catch (Exception e) {
								
								shouldRetry = false;
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
								logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								
								throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
								
							}
							
						} while(shouldRetry && ((numberOfRetries=numberOfRetries+1) <= maximumRetry));
						
					}
					else {
						// Extension file has not been supported
						System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_307+" | Error cause: "+ErrorCause.ERROR_307+" | File name: "+fileName);
						logger.error("[FAILED] Error code: " + ErrorCode.ERROR_307+" | Error cause: "+ErrorCause.ERROR_307+" | File name: "+fileName);
						
						throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_307);
					}
					
					break;
					
				}else {
					
					if(i == count) {
						// return fail not found record number
						System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_316+" | Error cause: "+ErrorCause.ERROR_316.replace("onlineRecordNumber", onlineRecordNumber)+" | File name: "+fileName);
						logger.error("[FAILED] Error code: " + ErrorCode.ERROR_316+" | Error cause: "+ErrorCause.ERROR_316.replace("onlineRecordNumber", onlineRecordNumber)+" | File name: "+fileName);
						
						throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_316);
					}
					
				}
				
			}
			
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
			logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
			throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
			logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
			throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
			logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
			throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
			logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
			throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
			logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
			throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
			logger.error("[FAILED] Error code: " + ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
			throw new DigSigException("Transaction error code: "+ErrorCode.ERROR_301);
		}
		
		
		return signedPDF;
		
	}
	
	
	
	private String getPasswordFromFileName(String fileName) {
		
		String password = "";
		String nameWithoutExtension = fileOperation.getNameWithoutExtension(fileName);
		String[] tmp = nameWithoutExtension.split("\\+\\+");
		
		if(tmp.length > 1) {
			password = tmp[tmp.length-1];
		}
		else {
			password = "";
		}
		
		return password;
		
	}
	
	
	
}
