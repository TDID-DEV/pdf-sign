package PDFSign.webService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import PDFSign.exception.DigSigException;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.RPC)
public interface SignSignature {

	@WebMethod byte[] sign(String onlineRecordNumber, String fileName, byte[] file) throws DigSigException;
	
}
