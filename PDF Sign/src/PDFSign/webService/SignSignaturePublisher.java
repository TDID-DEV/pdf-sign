package PDFSign.webService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import javax.net.ssl.SSLContext;
import javax.xml.ws.Endpoint;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsServer;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.encryption.DecryptPassword;
import PDFSign.key.SecretKey;
import PDFSign.properties.PropertiesManagement;
import PDFSign.thread.MainThread;
import PDFSign.webService.SignSignatureImpl;

//Endpoint publisher
public class SignSignaturePublisher {
	
	final static Logger logger = Logger.getLogger(SignSignaturePublisher.class);
	
	Endpoint ep;
	
	int onlineThreadLimit;
	
	String CONTEXT = "/ws/signSignature";
	
	public SignSignaturePublisher(int onlineCorePoolSize) {
		this.onlineThreadLimit = onlineCorePoolSize;
	}
	
	public void startEndpoint() {
		
		PropertiesManagement connectionProp = new PropertiesManagement();
		PropertiesManagement programProp = new PropertiesManagement();
		boolean isStarted = false;
		
		String webServiceIP = null;
		String webServicePort = null;
		String endpointAddress = null;
		String secureMode = null;
		
		String keyStorePath = null;
		String keyStorePassword = null;
		
		try {
		
			isStarted = false;
			
			webServiceIP = connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.host");
			webServicePort = connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.port");
			secureMode = connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.secureMode");
			//endpointAddress = "http://"+webServiceIP+":"+webServicePort+CONTEXT;
			
			keyStorePath = programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore");
			
			if(programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStorePassword").equalsIgnoreCase("")) {
				keyStorePassword = "";
			}
			else {
				//Decrypt password
				keyStorePassword = new DecryptPassword().decrypt(programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStorePassword"), new SecretKey().createSecretKey());
			}
			
			if((keyStorePath.length() > 0) && (keyStorePassword.length() > 0) && (secureMode.equalsIgnoreCase(Boolean.TRUE.toString())) && Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE) && Main.LicenseFile.getProgramType().equalsIgnoreCase(Constance.PROGRAM_TYPE_PLUS)) {
				
				// Set global trust store
				/*System.setProperty("javax.net.ssl.keyStore", keyStorePath);
				System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);*/
			    
			    // Init a configuration with our SSL context
				//HttpsConfigurator configurator = new HttpsConfigurator(ssl);
			    HttpsConfigurator configurator = new HttpsConfigurator(SSLContext.getDefault());

			    // Create a server on localhost, port 15124 (https port)
			    HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(webServiceIP, Integer.parseInt(webServicePort)), Integer.parseInt(webServicePort));
			    httpsServer.setHttpsConfigurator(configurator);

			    // Create a context so our service will be available under this context
			    HttpContext httpContext = httpsServer.createContext(CONTEXT);
			    httpsServer.start();

			    // Create endpoint address
				endpointAddress = "https://"+webServiceIP+":"+webServicePort+CONTEXT;
			    
			    // Finally, use the created context to publish the service
			    ep = Endpoint.create(new SignSignatureImpl());
			    ep.setExecutor(Executors.newFixedThreadPool(onlineThreadLimit));
			    ep.publish(httpContext);
			}
			else {
				// Create endpoint address
				endpointAddress = "http://"+webServiceIP+":"+webServicePort+CONTEXT;
				
				ep = Endpoint.create(new SignSignatureImpl());
				ep.setExecutor(Executors.newFixedThreadPool(onlineThreadLimit));
				ep.publish(endpointAddress);
			}
			
			isStarted = true;
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} finally {
			if(isStarted) {
				System.out.println("..................................... Create endpoint address : "+endpointAddress);
				logger.info("Create endpoint address : "+endpointAddress);
			}
			else {
				System.out.println("..................................... Failed to create endpoint");
				logger.info("Failed to create endpoint");
			}
		}
		
	}
	
	public void stopEndpoint() {
		ep.stop();
	}
	
}
