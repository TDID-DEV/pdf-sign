package PDFSign.encryption;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import PDFSign.encode.Decoder;

public class DecryptPassword {
	
	public String decrypt(String string, SecretKeySpec key) throws GeneralSecurityException, IOException {
		
		Decoder decoder = new Decoder();
        String iv = string.split(":")[0];
        String property = string.split(":")[1];
        Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        pbeCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(decoder.base64Decode(iv)));
        
        return new String(pbeCipher.doFinal(decoder.base64Decode(property)), "UTF-8");
        
    }

}
