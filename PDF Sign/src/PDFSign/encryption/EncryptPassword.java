package PDFSign.encryption;

import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import PDFSign.encode.Encoder;

public class EncryptPassword {
	
	public String encrypt(String password, SecretKeySpec key) throws GeneralSecurityException, UnsupportedEncodingException {
		
		Encoder encoder = new Encoder();
        Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        pbeCipher.init(Cipher.ENCRYPT_MODE, key);
        AlgorithmParameters parameters = pbeCipher.getParameters();
        IvParameterSpec ivParameterSpec = parameters.getParameterSpec(IvParameterSpec.class);
        byte[] cryptoText = pbeCipher.doFinal(password.getBytes("UTF-8"));
        byte[] iv = ivParameterSpec.getIV();
        
        return encoder.base64Encode(iv) + ":" + encoder.base64Encode(cryptoText);
        
    }

}
