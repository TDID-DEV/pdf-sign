package PDFSign.thread;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.encryption.DecryptPassword;
import PDFSign.errorCode.ErrorCause;
import PDFSign.errorCode.ErrorCode;
import PDFSign.file.FileOperation;
import PDFSign.key.SecretKey;
import PDFSign.properties.PropertiesManagement;
import PDFSign.x509certificate.GetCertValue;
import PDFSign.x509certificate.GetCertificate;
import PDFSign.x509certificate.X509Cert;
import PDFSign.token.TokenKeyStore;
import PDFSign.token.TokenProvider;


public class ThreadPool extends Thread {
	
	final static Logger logger = Logger.getLogger(ThreadPool.class);
	
	FileOperation fileOperation = new FileOperation();
	PropertiesManagement connectionProp = new PropertiesManagement();
	PropertiesManagement limitProp = new PropertiesManagement();
	PropertiesManagement prop = new PropertiesManagement();
	ExecutorService executor;
	String recordNum;
	
	
	public ThreadPool(int recordNum, ExecutorService executor) {
		
		this.executor = executor;
		this.recordNum = String.valueOf(recordNum);
		
	}
	
	
	public void run() {
		
		try {
			
			int waitTimeForNextRound = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.waitTimeForNextRound"))*1000;	// In second
			int delayTime = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.delayTime"))*1000;	// In second
			int maximumRetryCheckThread = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.maximumRetryCheckThread"));
			int waitTimeForNextCheckThread = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.waitTimeForNextCheckThread"))*1000;
			
			String inputFolder = prop.getProperties("digsig."+recordNum+".inputFolder");
			String outputFolder = prop.getProperties("digsig."+recordNum+".outputFolder");
			
			String llx = prop.getProperties("digsig."+recordNum+".llx");
			String lly = prop.getProperties("digsig."+recordNum+".lly");
			String urx = prop.getProperties("digsig."+recordNum+".urx");
			String ury = prop.getProperties("digsig."+recordNum+".ury");
			String fontSize = prop.getProperties("digsig."+recordNum+".fontSize");
			String page = prop.getProperties("digsig."+recordNum+".page");
			String reason = prop.getProperties("digsig."+recordNum+".reason");
			String location = prop.getProperties("digsig."+recordNum+".location");
			String signatureImg = prop.getProperties("digsig."+recordNum+".signatureImg");
			String corporateID = prop.getProperties("digsig."+recordNum+".corporateID");
			
			String keyAlias = prop.getProperties("digsig."+recordNum+".keyAlias");
			String hsmSerial = prop.getProperties("digsig."+recordNum+".hsmSerial");
			//Decrypt password
			String hsmPassword = new DecryptPassword().decrypt(prop.getProperties("digsig."+recordNum+".hsmPassword"), new SecretKey().createSecretKey());
			
			String withTSA = prop.getProperties("digsig."+recordNum+".withTSA");
			String tsaURL = prop.getProperties("digsig."+recordNum+".tsaURL");
			String tsaUser = prop.getProperties("digsig."+recordNum+".tsaUser");
			String tsaPassword = "";
			
			if(prop.getProperties("digsig."+recordNum+".tsaPassword").equalsIgnoreCase("")) {
				tsaPassword = "";
			}
			else {
				//Decrypt password
				tsaPassword = new DecryptPassword().decrypt(prop.getProperties("digsig."+recordNum+".tsaPassword"), new SecretKey().createSecretKey());
			}
			
			
			// Get provider and Key store
			TokenProvider tokenProvider = new TokenProvider();
			TokenKeyStore tokenKeyStore = new TokenKeyStore();
			Provider provider = tokenProvider.getTokenProviderWithHSMSerial(hsmSerial);
			KeyStore ks = tokenKeyStore.createTokenKeyStore(provider, hsmPassword);
			
			
			// Get private key and cert from HSM
			PrivateKey privateKey = (PrivateKey) ks.getKey(keyAlias, hsmPassword.toCharArray());
			Certificate cert = ks.getCertificate(keyAlias);
			X509Cert x509Cert = new X509Cert(cert.getEncoded());
			Certificate[] chain = ks.getCertificateChain(keyAlias);
			
			File folder = new File(inputFolder);
			String[] listOfFileName;
			
			/*int filePerRound = 20;
			filePerRound = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.filePerRound"));*/
			
			// Check validity signing cert
			x509Cert.checkValidity();
			
			// Check certificate with license file
			if(x509Cert.checkCertWithLicense() && Main.LicenseFile.IsCorporateTaxInLicenseFile(corporateID)) {
				
				if(folder.exists()) {
			
					System.out.println("..................................... Start batch processing mode for record number : "+recordNum);
					logger.info("Start batch processing mode for record number : " + recordNum);
				
				
					while(!executor.isShutdown()) {
						
						ArrayList<WorkerThread> worker = new  ArrayList<WorkerThread>();
						listOfFileName = folder.list();
						
						int count = 0;
						
						if(listOfFileName.length > 0 && !executor.isShutdown()){
							
							// Delay before signing
							Thread.sleep(delayTime);
						
							for (int i = 0; (i < listOfFileName.length) /*&& (i < filePerRound)*/; i++) {
								
								String filePath = folder.getAbsoluteFile()+File.separator+listOfFileName[i];
								
								//worker.add(new WorkerThread(executor, recordNum, filePath, outputFolder, llx, lly, urx, ury, fontSize, page, reason, location, signatureImg));
								worker.add(new WorkerThread(executor, privateKey, chain, recordNum, filePath, outputFolder, llx, lly, urx, ury, fontSize, page, reason, location, signatureImg, withTSA, tsaURL, tsaUser, tsaPassword));
								Runnable r = worker.get(i);
								
								// Retry when executor is full
								boolean retry = false;
								do {
									try {
										executor.execute(r);
										retry = false;
									} catch(RejectedExecutionException e) {
										retry = true;
										Thread.sleep(50);
									}
								} while(retry);
								
							}
							
							
							// Check process of thread
							int retryCount = 0;
							while(count != worker.size() && !executor.isShutdown() && (retryCount < maximumRetryCheckThread)) {
								count = 0;
								for (int j = 0; j < worker.size(); j++) {
									if(worker.get(j).isDone()) {
										count++;
									}
								}
								//System.out.println("Process file : "+count+" of "+worker.size());
								Thread.sleep(waitTimeForNextCheckThread);
								retryCount++;
							}
							
						}
						
						// Delay before next round
						Thread.sleep(waitTimeForNextRound);
						System.gc();
						
					}
				
				}
				else {
					System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" Error code: "+ErrorCode.ERROR_318+" | Error cause: "+ErrorCause.ERROR_318.replace("folderPath", folder.getAbsolutePath()));
					logger.info("Failed to start automatic processing mode for record number "+recordNum+" Error code: "+ErrorCode.ERROR_318+" | Error cause: "+ErrorCause.ERROR_318.replace("folderPath", folder.getAbsolutePath()));
				}
			
			}
			else {
				System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" Error code: "+ErrorCode.ERROR_312+" | Error cause: "+ErrorCause.ERROR_312);
				logger.info("Failed to start automatic processing mode for record number "+recordNum+" Error code: "+ErrorCode.ERROR_312+" | Error cause: "+ErrorCause.ERROR_312);
			}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (CertificateException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (KeyStoreException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode for record number "+recordNum+" ,Please check log file");
			logger.error(e.getMessage(), e);
		}
		
	}
	

}
