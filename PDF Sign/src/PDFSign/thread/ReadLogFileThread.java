package PDFSign.thread;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.util.ArrayList;

import PDFSign.Constance;
import PDFSign.panel.MainPanel;
import PDFSign.properties.PropertiesManagement;
import PDFSign.windowsService.ServiceOperation;

public class ReadLogFileThread extends Thread {

	ServiceOperation so = null;
	boolean KEEP_READ = true;
	int WAIT_TIME = 5;	// Second
	
	
	public ReadLogFileThread(ServiceOperation so) {
		
		PropertiesManagement prop = new PropertiesManagement();
		
		this.so = so;
		
		try {
			WAIT_TIME = Integer.parseInt(prop.getProperties(Constance.LIMIT_CON_PATH, "limit.refreshTime"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void run() {
		
		//String filePath = "C:\\Users\\# Fai #\\Desktop\\Signing Gateway\\logs\\"+"Log";
		String filePath = Constance.LOG_FOLDER_PATH+"Log";
		
		while(KEEP_READ) {
		
			try {
				
				long pointer = 0;
				long length = 0;
				
				pointer = getLastPointer(filePath);
				
				while(KEEP_READ) {
					
					RandomAccessFile rf = new RandomAccessFile(filePath, "r");
					
					if(rf.length() < length) {
						pointer = 0;
						rf.seek(pointer);
					}
					else {
						rf.seek(pointer);
					}
					
					ArrayList<String> allLine = readAllLine(rf);
					pointer = rf.getFilePointer();
					
					for(int i=0;i<allLine.size();i++) {
						
						if(allLine.get(i).contains("[SUCCESSFUL]")) {
							String tmp = allLine.get(i).split("- ")[1];		// Cut Datetime and java
							System.out.println(tmp.split("]")[0]+"] ................"+tmp.split("]")[1]);
						}
						else if(allLine.get(i).contains("[FAILED]") ){
							String tmp = allLine.get(i).split("- ")[1];		// Cut Datetime and java
							System.out.println(tmp.split("]")[0]+"] ........................"+tmp.split("]")[1]);
						}
						
					}
					
					length = rf.length();
					rf.close();
					Thread.sleep(WAIT_TIME*1000);
					
				}
				
			
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		}
		
	}
	
	
	@Override
	public synchronized void start() {
		KEEP_READ = true;
		super.start();
		
	}
	
	
	public void stopThread() {
		KEEP_READ = false;
		this.stop();
		
	}
	
	
	public int readPreviousLine(RandomAccessFile rf) {
		
		int counter = 0;
		
		try {
			
			while(rf.readLine() != null){
				counter++;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return counter;
	}
	
	
	public ArrayList<String> readAllLine(RandomAccessFile rf) throws IOException {
		
		ArrayList<String> allLine = new ArrayList<String>();
		String tmp = null;
		
		while(true) {
			tmp = rf.readLine();
			if(tmp != null) {
				allLine.add(tmp);
			}
			else {
				break;
			}
		}
		
		return allLine;
	}
	
	
	public long getLastPointer(String filePath) throws IOException {
		
		long pointer = 0;
		
		RandomAccessFile rf = new RandomAccessFile(filePath, "r");
		System.err.println("Previous line: "+readPreviousLine(rf));
		pointer = rf.getFilePointer();
		rf.close();
		
		return pointer;
		
	}
	
	
}
