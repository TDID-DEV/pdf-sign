package PDFSign.thread;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.ProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.util.concurrent.ExecutorService;

import org.apache.log4j.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.exceptions.BadPasswordException;

import PDFSign.Constance;
import PDFSign.convert.Convert;
import PDFSign.datetime.DateTime;
import PDFSign.errorCode.ErrorCause;
import PDFSign.errorCode.ErrorCode;
import PDFSign.exception.DigSigException;
import PDFSign.file.FileOperation;
import PDFSign.properties.PropertiesManagement;
import PDFSign.signature.PDFSignature;


public class WorkerThread implements Runnable {
	
	final static Logger logger = Logger.getLogger(WorkerThread.class);

	PropertiesManagement limitProp = new PropertiesManagement();
	PropertiesManagement programProp = new PropertiesManagement();
	FileOperation fileOperation = new FileOperation();
	File file;
	
	String PDF_FILE_EXTENSION = ".pdf";
	String outputFolder;
	
	String recordNum;
	String llx;
	String lly;
	String urx;
	String ury;
	String fontSize;
	String page;
	String reason;
	String location;
	String signatureImg;
	String signingCert;
	
	String withTSA;
	String tsaURL;
	String tsaUser;
	String tsaPassword;
	
	String fileName;
	
	PrivateKey privateKey;
	Certificate[] chain;
	
	ExecutorService executor;
	
	boolean isDone = false;
	boolean shouldRetry = false;
	
	
	public WorkerThread(ExecutorService executor, PrivateKey privateKey, Certificate[] chain, String recordNum, String filePath, String outputFolder, String llx, String lly, String urx, String ury, String fontSize, String page, String reason, String location, String signatureImg, String withTSA, String tsaURL, String tsaUser, String tsaPassword) {
		
		this.executor = executor;
		this.privateKey = privateKey;
		this.chain = chain;
		this.recordNum = recordNum;
		this.file = new File(filePath);
		this.fileName = file.getName();
		this.outputFolder = outputFolder;
		this.llx = llx;
		this.lly = lly;
		this.urx = urx;
		this.ury = ury;
		this.fontSize = fontSize;
		this.page = page;
		this.reason = reason;
		this.location = location;
		this.signatureImg = signatureImg;
		this.withTSA = withTSA;
		this.tsaURL = tsaURL;
		this.tsaUser = tsaUser;
		this.tsaPassword = tsaPassword;
		
	}
	
	
	public void run() {
		
		String dateTime = new DateTime().getDateTime("yyyyMMddHHmmssSSS");
		
		try {
		
			boolean isFileNotUsedByAnotherProcess = fileOperation.isFileNotUsedByAnotherProcess(file.getAbsolutePath());
			
			if((fileName.endsWith(".pdf") || fileName.endsWith(".PDF")) && isFileNotUsedByAnotherProcess) {
				
				int waitTimeForRetry = 3;
				int maximumRetry = 3;
				int numberOfRetries = 0;
				
				boolean isFileEncrypted = false;
				String password = "";
				
				do {
				
					if(!executor.isShutdown()) {
						
						try {
							
							// Get maximum retry and wait time from properties
							maximumRetry = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.maximumRetry"));
							waitTimeForRetry = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.waitTimeForRetry"));
							
							
							// sleep before next retry
							if(numberOfRetries > 0) {
								Thread.sleep(waitTimeForRetry*1000);
							}
							
							
							// File protection
							password = getPasswordFromFileName(fileName);
							
							
							// Sign
							PDFSignature pdfSig = new PDFSignature(privateKey, chain, password);
							byte[] signedPDF = pdfSig.setAppearanceAndSign(new Convert().fileToByteArray(file.getAbsolutePath()), llx, lly, urx, ury, fontSize, page, reason, location, signatureImg, withTSA, tsaURL, tsaUser, tsaPassword);
							
			    			FileOutputStream fos = new FileOutputStream(outputFolder+"\\Success\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION);
			    			fos.write(signedPDF);
			    			fos.flush();
			    			fos.close();
			    			
			    			fileOperation.Operate(file.getAbsolutePath(), null, 2);		// Delete input file
					    	
			    			System.out.println("[SUCCESSFUL] ................ Signing file "+fileName+" complete");
							logger.info("[SUCCESSFUL] Signing file "+fileName+" complete");
					    	
					    	// Set should retry to false when all done
					    	shouldRetry = false;
					    	
							
						} catch (FileNotFoundException e) {
							shouldRetry = false;
							e.printStackTrace();
							System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
				    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
							
						} catch (BadPasswordException e) {
							
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_306+" | Error cause: "+ErrorCause.ERROR_306+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_306+" | Error cause: "+ErrorCause.ERROR_306+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
						
						} catch (IOException e) {
							
							e.printStackTrace();
							
							if(numberOfRetries == maximumRetry) {
							
								if(e.getMessage().equalsIgnoreCase("Connection refused: connect")) {
									
									try {
										fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
							    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
									} catch (IOException e1) {
										e1.printStackTrace();
										logger.error(e1.getMessage(), e1);
									}
									
								}
								else if(e.getMessage().equalsIgnoreCase("Connection timed out: connect")) {
									
									try {
										fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
							    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
									} catch (IOException e1) {
										e1.printStackTrace();
										logger.error(e1.getMessage(), e1);
									}
									
								}
								else if(e.getMessage().equalsIgnoreCase("Network is unreachable: connect")) {
									
									try {
										fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
							    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
									} catch (IOException e1) {
										e1.printStackTrace();
										logger.error(e1.getMessage(), e1);
									}
									
								}
								else if(e.getMessage().equalsIgnoreCase("No route to host: connect")) {
									
									try {
										fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
							    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
									} catch (IOException e1) {
										e1.printStackTrace();
										logger.error(e1.getMessage(), e1);
									}
									
								}
								else if(e.getMessage().equalsIgnoreCase("sun.security.validator.ValidatorException: PKIX path validation failed: java.security.cert.CertPathValidatorException: timestamp check failed")) {
									
									try {
										fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName);
							    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314+" | File name: "+fileName, e);
									} catch (IOException e1) {
										e1.printStackTrace();
										logger.error(e1.getMessage(), e1);
									}
									
								}
								else {
									
									try {
										fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
										System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
							    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
									} catch (IOException e1) {
										e1.printStackTrace();
										logger.error(e1.getMessage(), e1);
									}
									
								}
								
							}
							else {
								shouldRetry = true;
							}
							
							
						} catch (ExceptionConverter e) {
							
							shouldRetry = false;
							e.printStackTrace();
							
							if(e.getMessage().equalsIgnoreCase("Connection refused: connect")) {
								
								try {
									fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_319+" | Error cause: "+ErrorCause.ERROR_319+" | File name: "+fileName);
						    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_319+" | Error cause: "+ErrorCause.ERROR_319+" | File name: "+fileName, e);
								} catch (IOException e1) {
									e1.printStackTrace();
									logger.error(e1.getMessage(), e1);
								}
								
							}
							else {
								
								try {
									fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
						    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								} catch (IOException e1) {
									e1.printStackTrace();
									logger.error(e1.getMessage(), e1);
								}
								
							}
							
						} catch (InterruptedException e) {
							
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						} catch(ProviderException e) {
							e.printStackTrace();
							
							if(e.getMessage().contains("Token has been removed") || e.getMessage().contains("Initialization failed")) {
								
								try {
									shouldRetry = false;
									fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName);
						    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName, e);
								} catch (IOException e1) {
									e1.printStackTrace();
									logger.error(e1.getMessage(), e1);
								}
								
							}
							else {
								try {
									shouldRetry = false;
									fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
						    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								} catch (IOException e1) {
									e1.printStackTrace();
									logger.error(e1.getMessage(), e1);
								}
								
							}
							
						} catch (KeyStoreException e) {
							e.printStackTrace();
							
							if(e.getMessage().contains("PKCS11 not found")) {
								
								try {
									shouldRetry = false;
									fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName);
						    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_305+" | Error cause: "+ErrorCause.ERROR_305+" | File name: "+fileName, e);
								} catch (IOException e1) {
									e1.printStackTrace();
									logger.error(e1.getMessage(), e1);
								}
								
							}
							else {
								try {
									shouldRetry = false;
									fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
									System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
						    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
								} catch (IOException e1) {
									e1.printStackTrace();
									logger.error(e1.getMessage(), e1);
								}
								
							}
							
						} catch (CertificateExpiredException e) {
							
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_302+" | Error cause: "+ErrorCause.ERROR_302+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_302+" | Error cause: "+ErrorCause.ERROR_302+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						} catch (CertificateNotYetValidException e) {
							
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_303+" | Error cause: "+ErrorCause.ERROR_303+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_303+" | Error cause: "+ErrorCause.ERROR_303+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						} catch (CertificateException e) {
	
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						} catch (DocumentException e) {
	
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						} catch (GeneralSecurityException e) {
	
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						} catch (Exception e) {
							
							try {
								shouldRetry = false;
								fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName.split(".pdf")[0]+"_"+dateTime+PDF_FILE_EXTENSION, 1);
								e.printStackTrace();
								System.out.println("[FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName);
					    		logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_301+" | Error cause: "+ErrorCause.ERROR_301+" | File name: "+fileName, e);
							} catch (IOException e1) {
								e1.printStackTrace();
								logger.error(e1.getMessage(), e1);
							}
							
						}
						
						
					}
					else {
						shouldRetry = false;
					}
					
					
				} while(shouldRetry && ((numberOfRetries=numberOfRetries+1) <= maximumRetry));
			
			
			}
			else {
				
				try {
				
					if(isFileNotUsedByAnotherProcess == false) {
						//System.out.println("................................................................ [FAILED] ............. "+"Error code: "+ErrorCode.ERROR_217+" | Error cause: "+ErrorCause.ERROR_217);
						logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_317+" | Error cause: "+ErrorCause.ERROR_317+" | File name: "+fileName);
					}
					else {
						fileOperation.Operate(file.getAbsolutePath(), outputFolder+"\\Fail\\"+fileName, 1);
						System.out.println("................................................................ [FAILED] ............. "+"Error code: "+ErrorCode.ERROR_307+" | Error cause: "+ErrorCause.ERROR_307+" | File name: "+fileName);
						logger.error("[FAILED] "+"Error code: "+ErrorCode.ERROR_307+" | Error cause: "+ErrorCause.ERROR_307+" | File name: "+fileName);
					}
					
				} catch (IOException e) {
					e.printStackTrace();
					logger.error(e.getMessage(), e);
				}
				
			}
		
			
		} finally {
			isDone = true;
		}
		
		
	}
	
	
	public boolean isDone() {
		return isDone;
		
	}
	
	
	
	private String getPasswordFromFileName(String fileName) {
		
		String password = "";
		String nameWithoutExtension = fileOperation.getNameWithoutExtension(fileName);
		String[] tmp = nameWithoutExtension.split("\\+\\+");
		
		if(tmp.length > 1) {
			password = tmp[tmp.length-1];
		}
		else {
			password = "";
		}
		
		return password;
		
	}
	
	
	
}
