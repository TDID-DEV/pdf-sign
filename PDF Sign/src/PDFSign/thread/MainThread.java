package PDFSign.thread;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.encryption.DecryptPassword;
import PDFSign.key.SecretKey;
import PDFSign.properties.PropertiesManagement;
import PDFSign.token.TokenProvider;
import PDFSign.webService.SignSignaturePublisher;

public class MainThread extends Thread {
	
	final static Logger logger = Logger.getLogger(MainThread.class);
	
	ArrayList<ThreadPool> threadPoolList = new ArrayList<ThreadPool>();
	//ArrayList<OnlineThreadPool> onlineThreadPoolList = new ArrayList<OnlineThreadPool>();
	
	SignSignaturePublisher ssp = null;
	
	ThreadPoolExecutor executor;
	//ThreadPoolExecutor onlineExecutor;
	int count;
	int onlineThreadLimit = 20;		//Default online thread
	
	
	public MainThread(int count) {
		
		PropertiesManagement limitProp = new PropertiesManagement();
		PropertiesManagement programProp = new PropertiesManagement();
		
		try {
			
			this.count = count;
			int corePoolSize = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.thread"));
			int maximumPoolSize = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.thread"));
			int keepAliveTime = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.keepAliveTime"));
			int workQueue = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.workQueue"));
			
			onlineThreadLimit = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.onlineThread"));
			
			executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(workQueue));
			executor.allowCoreThreadTimeOut(true);
			
			/*executor = Executors.newFixedThreadPool(Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.thread")));
			onlineExecutor = Executors.newFixedThreadPool(Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.onlineThread")));*/
			
			
			
			// Set global trustStore and keyStore
			System.setProperty("javax.net.ssl.keyStore", programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore"));
			if(programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStorePassword").equalsIgnoreCase("")) {
				System.setProperty("javax.net.ssl.keyStorePassword", "");
			}
			else {
				//Decrypt password
				String decryptedKeyStorePassword = new DecryptPassword().decrypt(programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStorePassword"), new SecretKey().createSecretKey());
				System.setProperty("javax.net.ssl.keyStorePassword", decryptedKeyStorePassword);
			}
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		
	}
	
	
	
	public void run() {
		
		PropertiesManagement prop = new PropertiesManagement();
		
		try {
			
		
			for (int i = 1; i <= this.count; i++) {
				
				TokenProvider tokenProvider = new TokenProvider();
				if(tokenProvider.addTokenProviderByRecordNumber(i)) {
					Main.tokenProviderList.add(tokenProvider);
				}
				
				try {
					
					if(prop.getProperties("digsig."+i+".operationMode").equalsIgnoreCase("batch")) {
						ThreadPool tp = new ThreadPool(i, executor);
						tp.start();
						threadPoolList.add(tp);
						
					}
					else {
						// Do nothing
					}
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.out.println("..................................... Failed to start batch processing mode for record number : "+i);
					logger.error(e.getMessage(), e);
				} catch (com.sun.xml.internal.ws.server.ServerRtException e) {
					e.printStackTrace();
					System.out.println("..................................... Failed to start batch processing mode for record number : "+i);
					logger.error(e.getMessage(), e);
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("..................................... Failed to start batch processing mode for record number : "+i);
					logger.error(e.getMessage(), e);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("..................................... Failed to start batch processing mode for record number : "+i);
					logger.error(e.getMessage(), e);
				}
				
			}
			
			
			// Start online
			if(Main.LicenseFile.getProgramEdition().equals(Constance.PROGRAM_EDITION_ENTERPRISE)) {
				ssp = new SignSignaturePublisher(onlineThreadLimit);
				ssp.startEndpoint();
			}
		
		
		
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode");
			logger.error(e1.getMessage(), e1);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode");
			logger.error(e1.getMessage(), e1);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode");
			logger.error(e1.getMessage(), e1);
		} catch (Exception e1) {
			e1.printStackTrace();
			System.out.println("..................................... Failed to start batch processing mode");
			logger.error(e1.getMessage(), e1);
		}
		
		
	}
	
	
	public void terminate() {
		
		try {
		
			executor.shutdown();
			//onlineExecutor.shutdown();
			
			if(threadPoolList.size() > 0) {
				
				for (int i = 0; i < threadPoolList.size(); i++) {
					threadPoolList.get(i).stop();
				}
				
			}
			
			if(Main.tokenProviderList.size() > 0) {
				for (int i = 0; i < Main.tokenProviderList.size(); i++) {
					if(Main.tokenProviderList.get(i).provider != null) {
						Main.tokenProviderList.get(i).removeThisTokenProvider();
					}
					Main.tokenProviderList.get(i).tokenEnv.DestroyModule();
				}
			}
			
			/*if(onlineThreadPoolList.size() > 0) {
				
				for (int i = 0; i < onlineThreadPoolList.size(); i++) {
					
					onlineThreadPoolList.get(i).signingGatewaySocket.close();
					onlineThreadPoolList.get(i).stop();
						
				}
			
			}*/
			
			if((ssp != null) && (Main.LicenseFile.getProgramEdition().equals(Constance.PROGRAM_EDITION_ENTERPRISE))) {
				
				ssp.stopEndpoint();
				ssp = null;
				
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		
	}
	
	

}
