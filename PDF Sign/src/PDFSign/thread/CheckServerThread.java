package PDFSign.thread;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;

import javax.net.ssl.SSLSocketFactory;

import org.apache.log4j.Logger;

import PDFSign.Constance;
import PDFSign.errorCode.ErrorCause;
import PDFSign.errorCode.ErrorCode;
import PDFSign.file.WriteToTextFile;
import PDFSign.generator.TransactionID;
import PDFSign.properties.PropertiesManagement;


public class CheckServerThread extends Thread {

	final static Logger logger = Logger.getLogger(CheckServerThread.class);
	
	ExecutorService executor;
	
	String signingServer_host;
	int signingServer_port;
	
	public CheckServerThread(ExecutorService executor) {
		
		PropertiesManagement connectionProp = new PropertiesManagement();
		
		try {
			this.executor = executor;
			signingServer_host = connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.signingServer.host");
			signingServer_port = Integer.parseInt(connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.signingServer.port"));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		
	}
	
	
	
	public void run() {
		
		PropertiesManagement limitProp = new PropertiesManagement();
		PropertiesManagement programProp = new PropertiesManagement();
		
		String tranID = null;
		
		Socket socket = null;
		
		DataOutputStream dos = null;
		DataInputStream dis = null;
		
		int maximumRetry = 5;
		int waitTimeForRetry = 3;
		int numberOfRetries = 0;
		
		boolean shouldRetry = false;
		
		while(!executor.isShutdown()) {
			
			do {
			
				try {
					tranID = TransactionID.generate();
					byte[] byteForSend;
					
					// Get maximum retry and wait time from properties
					maximumRetry = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.maximumRetry"));
					waitTimeForRetry = Integer.parseInt(limitProp.getProperties(Constance.LIMIT_CON_PATH, "limit.waitTimeForRetry"));
					
					// sleep before next retry
					if(numberOfRetries > 0) {
						Thread.sleep(waitTimeForRetry*1000);
					}
					
					
					//socket = new Socket(host_ip, host_port);
					/*System.setProperty("javax.net.ssl.trustStore", programProp.getProperties(Constance.PROGRAM_CON_PATH, "sg.sslTrustStore"));
					System.setProperty("javax.net.ssl.trustStorePassword", programProp.getProperties(Constance.PROGRAM_CON_PATH, "sg.sslTrustStorePassword"));*/
					SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();
					socket = sf.createSocket(signingServer_host, signingServer_port);
					
					dos = new DataOutputStream(socket.getOutputStream());
					dis = new DataInputStream(socket.getInputStream());
					
					
					
					
					////-- Send --////
					
					ByteBuffer bb = ByteBuffer.allocate(3 + 28);
					bb.put(Constance.MESSAGE_CODE_CHECK.getBytes()).put(tranID.getBytes());
					byteForSend = bb.array();
					
					dos.write(byteForSend, 0, bb.capacity());
					dos.flush();
					bb.clear();
					
					System.out.println("<"+tranID+"> [SUCCESSFUL] ................ Sending check message");
					logger.info("<"+tranID+"> [SUCCESSFUL] Sending check message");
					
					
					
					
					
					////-- Receive --////
					
					// Message Code
					byte[] recBytearray = new byte[3];
					int currentTot = 0;
			    	int bytesRead;
			    	do{
			    		bytesRead = dis.read(recBytearray, 0, 3);
			    		currentTot += bytesRead;
			    		Thread.sleep(100);
			    	} while (currentTot != 3 && !executor.isShutdown());
			    	String recMessageCode = new String(recBytearray);
			    
			    	// Transaction ID
			    	recBytearray = new byte[28];
			    	currentTot = 0;
			    	do{
			    		bytesRead = dis.read(recBytearray, 0, 28);
			    		currentTot += bytesRead;
			    		Thread.sleep(100);
			    	} while (currentTot != 28 && !executor.isShutdown());
			    	String recTranID = new String(recBytearray);
			    	
			    	
			    	if (recMessageCode.equals(Constance.MESSAGE_CODE_CHECK)){
			    		
			    		// Check receive tranID with original tranID
			    		if(recTranID.equalsIgnoreCase(tranID)) {
			    			System.out.println("<"+tranID+"> [SUCCESSFUL] ................ Receive check message");
							logger.info("<"+tranID+"> [SUCCESSFUL] Receive check message");
		    			
			    		}
			    		else {
				    		System.out.println("<"+tranID+"> [FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_313+" | Error cause: "+ErrorCause.ERROR_313);
				    		logger.error("<"+tranID+"> [FAILED] "+"Error code: "+ErrorCode.ERROR_313+" | Error cause: "+ErrorCause.ERROR_313);
			    		}
			    		
			    	}
			    	else {
			    		System.out.println("<"+tranID+"> [FAILED] ........................ Transaction error code: " + recMessageCode);
			    		logger.error("<"+tranID+"> [FAILED] Transaction error code: " + recMessageCode);
			    	}
			    	
					
			    	shouldRetry = false;
			    	numberOfRetries = 0;
			    	
			    	
				} catch (IOException e) {
					
					e.printStackTrace();
					
					if(numberOfRetries == maximumRetry) {
					
						if(e.getMessage().equalsIgnoreCase("Connection refused: connect")) {
							System.out.println("<"+tranID+"> [FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314);
							logger.error("<"+tranID+"> [FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314, e);
						}
						else if(e.getMessage().equalsIgnoreCase("Connection timed out: connect")) {
							System.out.println("<"+tranID+"> [FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314);
							logger.error("<"+tranID+"> [FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314, e);
						}
						else if(e.getMessage().equalsIgnoreCase("Network is unreachable: connect")) {
							System.out.println("<"+tranID+"> [FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314);
							logger.error("<"+tranID+"> [FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314, e);
						}
						else if(e.getMessage().equalsIgnoreCase("No route to host: connect")) {
							System.out.println("<"+tranID+"> [FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314);
							logger.error("<"+tranID+"> [FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314, e);
						}
						else {
							System.out.println("<"+tranID+"> [FAILED] ........................ "+"Error code: "+ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314);
							logger.error("<"+tranID+"> [FAILED] Error code: " + ErrorCode.ERROR_314+" | Error cause: "+ErrorCause.ERROR_314, e);
						}
					
						
					}
					else {
						shouldRetry = true;
					}
					
					
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.error(e.getMessage(), e);
					
				} finally {
					
					try {
						if(socket!=null)
							socket.close();
						if(dos!=null)
							dos.close();
						if(dis!=null)
							dis.close();
						
					} catch (IOException e) {
						e.printStackTrace();
						logger.error(e.getMessage(), e);
					}
					
				}
			
				
			} while(shouldRetry && ((numberOfRetries=numberOfRetries+1) <= maximumRetry));
		
				
			// Loop for sleep thread
	    	int counter = 0;
	    	int waitTime = 1800;
	    	while(!executor.isShutdown() && counter < waitTime) {
	    		
	    		try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
					logger.error(e.getMessage(), e);
				}
	    		counter = counter + 1;
	    		
	    	}
			
			
		}
		
		
	}
	
	
	
}
