package PDFSign.startup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.license.LicenseFileReader;
import PDFSign.license.exception.LicenseFileException;
import PDFSign.lock.JustOneLock;
import PDFSign.properties.PropertiesManagement;


public class Startup {
	
	//final static Logger logger = Logger.getLogger(Startup.class);
	
	
	public static void checkAppLock() {
		
		JustOneLock jol = new JustOneLock(Constance.PROGRAM_NAME);
		
		if(jol.isAppActive()) {
			JOptionPane.showMessageDialog(null, "Application already run", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		
	}
	
	
	
	public static void setupLookAndFeels(){
		
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PDFSign.frame.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PDFSign.frame.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PDFSign.frame.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PDFSign.frame.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
	}
	

	
	public static boolean checkLicenceFile(){
		
		boolean result = true;
		final String LicenseFileName = "LicenseFile.lic";
		
		if(!new File(LicenseFileName).exists()) {
			result = false;
			JOptionPane.showMessageDialog(null, "License file not found", "Error", JOptionPane.ERROR_MESSAGE);
			
		}
		
		return result;
		
	}
	
	
	
	public static boolean licenseFileLoader(){
		
		boolean result = false;
			
		try {
			Main.LicenseFile = new LicenseFileReader(Constance.LICENSE_FILE_PATH);
			
			try{
				// Check license support program
				if(Main.LicenseFile.IsSupportProgram().contains(Constance.PROGRAM_NAME)) {
					result = true;
					System.out.println("License support program");
				}
				else{
					result = false;
					JOptionPane.showMessageDialog(null, "License not support program", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			}catch (Exception e){
				result = false;
				if(e instanceof java.lang.NullPointerException){
					JOptionPane.showMessageDialog(null, "License file is invalid", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			}
			
			
		} catch (LicenseFileException e) {
			result = false;
			e.printStackTrace();
		}
			
		return result;
		
	}
	
	
	/*public static void addProvider(){
		
		try {
			// Load Luna provider
			Class providerClass = Class.forName("com.chrysalisits.crypto.LunaJCAProvider");
			Provider provider = (Provider)providerClass.newInstance();
			Security.addProvider(provider);
			
			providerClass = Class.forName("com.chrysalisits.cryptox.LunaJCEProvider");
			provider = (Provider)providerClass.newInstance();
			Security.addProvider(provider);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage(),e);
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (InstantiationException e) {
			e.printStackTrace();
			logger.error(e.getMessage(),e);
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			logger.error(e.getMessage(),e);
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			if(e.getMessage().equalsIgnoreCase("function 'C_Initialize' returns 0x30")){
				e.printStackTrace();
				logger.error(e.getMessage(),e);
				JOptionPane.showMessageDialog(null, "Load provider error", "Error", JOptionPane.ERROR_MESSAGE);
			}
			else{
				e.printStackTrace();
				logger.error(e.getMessage(),e);
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}*/
	
	
	public static void initialLog() {
		
		PropertyConfigurator.configure(Constance.LOG_CONFIGURATION_PATH);
		
	}
	
	
	
	public static void loadFont() {
		
		try {
			
			byte[] bFont = Files.readAllBytes(new File("./fonts/THSarabunNew.ttf").toPath());
			//Main.font = new Font(BaseFont.createFont("./fonts/THSarabunNew.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
			Main.font = new Font(BaseFont.createFont("./fonts/THSarabunNew.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, bFont, null));
			
		} catch (DocumentException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	
	
}
