package PDFSign.panel;

import java.awt.Image;

import javax.swing.ImageIcon;

import PDFSign.Constance;
import PDFSign.Main;

public class AboutPanel extends javax.swing.JPanel {

	public AboutPanel() {
        initComponents();
        
        ImageIcon imageIcon = new ImageIcon(Constance.IMAGES_FOLDER_PATH+"PDF Sign.png");
        Image image = imageIcon.getImage(); // transform it 
        Image newimg = image.getScaledInstance(80, 80,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        imageIcon = new ImageIcon(newimg);  // transform it back
        logoLabel.setIcon(imageIcon);
        
    }

	
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();
        versionLabel = new javax.swing.JLabel();
        copyrightLabel = new javax.swing.JLabel();
        editionLabel = new javax.swing.JLabel();
        logoLabel = new javax.swing.JLabel();
        editionLabel1 = new javax.swing.JLabel();

        jLabel2.setText("jLabel2");

        nameLabel.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        nameLabel.setText(Constance.PROGRAM_NAME);

        versionLabel.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        versionLabel.setText("Version : "+Constance.PROGRAM_VERSION);

        copyrightLabel.setText("Copy Rights 2017 by Thai Digital ID Company Limited All rights reserve");

        editionLabel.setText("Program edition : "+Main.LicenseFile.getProgramEdition());

        editionLabel1.setText("Program type : "+Main.LicenseFile.getProgramType());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(copyrightLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(logoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(versionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editionLabel)
                    .addComponent(editionLabel1)
                    .addComponent(nameLabel))
                .addGap(107, 107, 107))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(versionLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editionLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editionLabel1))
                    .addComponent(logoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(copyrightLabel))
        );
    }// </editor-fold>
    
    
	// Variables declaration - do not modify
    private javax.swing.JLabel copyrightLabel;
    private javax.swing.JLabel editionLabel;
    private javax.swing.JLabel editionLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel versionLabel;
    // End of variables declaration
	
}
