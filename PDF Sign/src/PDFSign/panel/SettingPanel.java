package PDFSign.panel;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.encryption.EncryptPassword;
import PDFSign.file.BrowseFile;
import PDFSign.file.ExtensionFileFilter;
import PDFSign.file.FileOperation;
import PDFSign.key.SecretKey;
import PDFSign.properties.PropertiesManagement;
import PDFSign.token.Token;
import PDFSign.token.TokenEnvironment;
import PDFSign.x509certificate.GetCertValue;
import PDFSign.x509certificate.GetCertificate;
import PDFSign.x509certificate.GetX509Certificate;
import PDFSign.x509certificate.X509Cert;
import iaik.pkcs.pkcs11.Slot;
import iaik.pkcs.pkcs11.TokenException;
import iaik.pkcs.pkcs11.objects.RSAPrivateKey;
import iaik.pkcs.pkcs11.objects.RSAPublicKey;
import iaik.pkcs.pkcs11.objects.X509PublicKeyCertificate;



public class SettingPanel extends javax.swing.JPanel {
	
	private String INPUT_FOLDER = "Input";
	private String OUTPUT_FOLDER = "Output";
	private int ONLINE_REECORD_NUMBER_LIMIT = 6;
	
	private DefaultTableModel tm;
	private String operationMode = "batch";
	private String page = "first";
	
	private Slot[] slots;
	public TokenEnvironment tokenEnv = new TokenEnvironment();

	public SettingPanel() {
        initComponents();
        
        tm = (DefaultTableModel) recordTable.getModel();
        
        imgLabel.setIcon(new ImageIcon(Constance.IMAGES_FOLDER_PATH+"Position.png"));
        
        
        // Set enable
		specifiedPageTextField.setEnabled(false);
		keyStoreComboBox.setEnabled(false);
		addKeyStoreButton.setEnabled(false);
		removeKeyStoreButton.setEnabled(false);
    	keyStorePasswordField.setEnabled(false);
    	onlineRecordNumberTextField.setEnabled(false);
    	
    	tsaURLTextField.setEnabled(false);
    	tsaUserTextField.setEnabled(false);
    	tsaPasswordField.setEnabled(false);
    	
		
		if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_STANDARD)) {
			onlineRadioButton.setEnabled(false);
			onlineRecordNumberTextField.setEnabled(false);
			webServiceHostTextField.setEnabled(false);
			webServicePortTextField.setEnabled(false);
			webServiceChangeButton.setEnabled(false);
			sslKeyStoreCheckBox.setEnabled(false);
			jLabel20.setEnabled(false);
			
			keyStoreComboBox.setEnabled(false);
			addKeyStoreButton.setEnabled(false);
			removeKeyStoreButton.setEnabled(false);
			keyStorePasswordField.setEnabled(false);
			
			tsaCheckBox.setEnabled(false);
			jLabel2.setEnabled(false);
			jLabel3.setEnabled(false);
			jLabel4.setEnabled(false);
			jLabel27.setEnabled(false);
			jLabel21.setEnabled(false);
			
			jLabel11.setEnabled(false);
			httpLabel.setEnabled(false);
			jLabel19.setEnabled(false);
			jLabel26.setEnabled(false);
			jLabel25.setEnabled(false);
		}
		else if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE) && Main.LicenseFile.getProgramType().equalsIgnoreCase(Constance.PROGRAM_TYPE_BASIC)) {
			sslKeyStoreCheckBox.setEnabled(false);
			jLabel20.setEnabled(false);
			
			tsaCheckBox.setEnabled(false);
			jLabel2.setEnabled(false);
			jLabel3.setEnabled(false);
			jLabel4.setEnabled(false);
			jLabel27.setEnabled(false);
		}
        
		
		try {
			setHSMComBox();
			getRecordFromProp();
			getWebServiceSettingFromProp();
			setSignatureImgComboBox();
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		
    }

    
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        batchRadioButton = new javax.swing.JRadioButton();
        onlineRadioButton = new javax.swing.JRadioButton();
        inputFolderTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        browseInputButton = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        outputFolderTextField = new javax.swing.JTextField();
        browseOutputButton = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        onlineRecordNumberTextField = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        llyTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        llxTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        urxTextField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        uryTextField = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        fontSizeTextField = new javax.swing.JTextField();
        firstPageRadioButton = new javax.swing.JRadioButton();
        lastPageRadioButton = new javax.swing.JRadioButton();
        specifiedPageRadioButton = new javax.swing.JRadioButton();
        specifiedPageTextField = new javax.swing.JTextField();
        imgLabel = new javax.swing.JLabel();
        hsmComboBox = new javax.swing.JComboBox();
        hsmSlotComboBox = new javax.swing.JComboBox();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        hsmPasswordField = new javax.swing.JPasswordField();
        jPanel4 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        locationTextField = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        reasonTextField = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        signatureImgComboBox = new javax.swing.JComboBox();
        addSignatureImgButton = new javax.swing.JButton();
        removeSignatureImgButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        tsaCheckBox = new javax.swing.JCheckBox();
        tsaURLTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tsaUserTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tsaPasswordField = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        recordTable = new javax.swing.JTable();
        recordSaveButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        retrieveButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        httpLabel = new javax.swing.JLabel();
        webServiceHostTextField = new javax.swing.JTextField();
        webServicePortTextField = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        webServiceChangeButton = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        sslKeyStoreCheckBox = new javax.swing.JCheckBox();
        addKeyStoreButton = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        keyStorePasswordField = new javax.swing.JPasswordField();
        jLabel27 = new javax.swing.JLabel();
        keyStoreComboBox = new javax.swing.JComboBox();
        removeKeyStoreButton = new javax.swing.JButton();

        buttonGroup1.add(batchRadioButton);
        batchRadioButton.setSelected(true);
        batchRadioButton.setText("Batch");
        batchRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(onlineRadioButton);
        onlineRadioButton.setText("Online");
        onlineRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onlineRadioButtonActionPerformed(evt);
            }
        });

        jLabel7.setText("Input Folder");

        browseInputButton.setText("Browse");
        browseInputButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseInputButtonActionPerformed(evt);
            }
        });

        jLabel9.setText("Output Folder");

        browseOutputButton.setText("Browse");
        browseOutputButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseOutputButtonActionPerformed(evt);
            }
        });

        jLabel21.setText("Online Record Number (6 digits)");

        onlineRecordNumberTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                onlineRecordNumberTextFieldKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(batchRadioButton)
                                    .addComponent(onlineRadioButton))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(outputFolderTextField))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(18, 18, 18)
                                        .addComponent(inputFolderTextField)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(browseInputButton)
                                    .addComponent(browseOutputButton))))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(onlineRecordNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(785, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(batchRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(inputFolderTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseInputButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(outputFolderTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseOutputButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(onlineRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(onlineRecordNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(94, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Mode", jPanel1);

        jLabel6.setText("Hardware Security Module");

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.blue, null), "Appearance"));

        jLabel1.setText("Lower Left Y");

        llyTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                llyTextFieldKeyTyped(evt);
            }
        });

        jLabel10.setText("Lower Left X");

        llxTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                llxTextFieldKeyTyped(evt);
            }
        });

        jLabel13.setText("Upper Right X");

        urxTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                urxTextFieldKeyTyped(evt);
            }
        });

        jLabel14.setText("Upper Right Y");

        uryTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                uryTextFieldKeyTyped(evt);
            }
        });

        jLabel16.setText("Font Size");

        fontSizeTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                fontSizeTextFieldKeyTyped(evt);
            }
        });

        buttonGroup2.add(firstPageRadioButton);
        firstPageRadioButton.setSelected(true);
        firstPageRadioButton.setText("Display In First Page");
        firstPageRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                firstPageRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup2.add(lastPageRadioButton);
        lastPageRadioButton.setText("Display In Last Page");
        lastPageRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lastPageRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup2.add(specifiedPageRadioButton);
        specifiedPageRadioButton.setText("Display In Specified Page");
        specifiedPageRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                specifiedPageRadioButtonActionPerformed(evt);
            }
        });

        specifiedPageTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                specifiedPageTextFieldKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(imgLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(96, 96, 96)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(llxTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(urxTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel14)))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(llyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(uryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel16)
                        .addGap(18, 18, 18)
                        .addComponent(fontSizeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(firstPageRadioButton)
                    .addComponent(lastPageRadioButton)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(specifiedPageRadioButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(specifiedPageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(96, 96, 96))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(firstPageRadioButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lastPageRadioButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(specifiedPageRadioButton)
                            .addComponent(specifiedPageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(urxTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14)
                            .addComponent(uryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(fontSizeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(llxTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(jLabel1)
                            .addComponent(llyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(imgLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        hsmComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "----- Please select HSM -----" }));
        hsmComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hsmComboBoxActionPerformed(evt);
            }
        });

        hsmSlotComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "----- Please select slot -----" }));

        jLabel29.setText("Slot");

        jLabel30.setText("Password");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(14, 14, 14)
                        .addComponent(hsmComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(hsmSlotComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(hsmPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(hsmComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hsmSlotComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel30)
                    .addComponent(hsmPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(71, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Signature", jPanel5);

        jLabel15.setText("Location (Optional)");

        jLabel17.setText("Reason (Optional)");

        jLabel18.setText("Signature Image (Optional)");

        signatureImgComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "----- Please select signature image (Optional) -----" }));

        addSignatureImgButton.setText("Add Image");
        addSignatureImgButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSignatureImgButtonActionPerformed(evt);
            }
        });

        removeSignatureImgButton.setText("Remove Image");
        removeSignatureImgButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeSignatureImgButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("URL");

        tsaCheckBox.setText("Timestamp");
        tsaCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tsaCheckBoxActionPerformed(evt);
            }
        });

        jLabel3.setText("Username");

        jLabel4.setText("Password");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(18, 18, 18)
                        .addComponent(signatureImgComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(addSignatureImgButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeSignatureImgButton))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(locationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(reasonTextField))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(tsaCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tsaURLTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tsaUserTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addGap(14, 14, 14)
                        .addComponent(tsaPasswordField, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel17)
                    .addComponent(locationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reasonTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(signatureImgComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSignatureImgButton)
                    .addComponent(removeSignatureImgButton)
                    .addComponent(jLabel18))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tsaCheckBox)
                    .addComponent(jLabel2)
                    .addComponent(tsaURLTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(tsaUserTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(tsaPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(139, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Signature (Optional)", jPanel4);

        recordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No.", "Corporate ID", "urx", "ury", "llx", "lly", "Font Size", "Page", "Online Record Number", "Input Path", "Output Path", "HSM Serial", "Key Alias"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(recordTable);
        recordTable.getColumnModel().getColumn(0).setMaxWidth(35);
        recordTable.getColumnModel().getColumn(1).setPreferredWidth(120);
        recordTable.getColumnModel().getColumn(1).setMaxWidth(120);
        recordTable.getColumnModel().getColumn(2).setMaxWidth(35);
        recordTable.getColumnModel().getColumn(3).setMaxWidth(35);
        recordTable.getColumnModel().getColumn(4).setMaxWidth(35);
        recordTable.getColumnModel().getColumn(5).setMaxWidth(35);
        recordTable.getColumnModel().getColumn(6).setMaxWidth(55);
        recordTable.getColumnModel().getColumn(7).setMaxWidth(40);
        recordTable.getColumnModel().getColumn(8).setPreferredWidth(120);
        recordTable.getColumnModel().getColumn(8).setMaxWidth(120);

        recordSaveButton.setText("Save");
        recordSaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recordSaveButtonActionPerformed(evt);
            }
        });

        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        retrieveButton.setText("Retrieve");
        retrieveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retrieveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(recordSaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(retrieveButton)
                .addGap(18, 18, 18)
                .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(380, 380, 380))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(recordSaveButton)
                    .addComponent(deleteButton)
                    .addComponent(retrieveButton))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Record configuration", jPanel2);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.blue, null), "Web Service"));

        jLabel11.setText("Web Service URL");

        httpLabel.setText("http://");

        webServicePortTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                webServicePortTextFieldKeyTyped(evt);
            }
        });

        jLabel24.setText(":");

        jLabel25.setText("/ws/signSignature");

        webServiceChangeButton.setText("Change Setting");
        webServiceChangeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                webServiceChangeButtonActionPerformed(evt);
            }
        });

        jLabel26.setText("(Port)");

        jLabel19.setText("(Host name / IP address)");

        sslKeyStoreCheckBox.setText("SSL Key Store");
        sslKeyStoreCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sslKeyStoreCheckBoxActionPerformed(evt);
            }
        });

        addKeyStoreButton.setText("Add Key Store");
        addKeyStoreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addKeyStoreButtonActionPerformed(evt);
            }
        });

        jLabel20.setText("SSL Key Store Password");

        jLabel27.setText("(In SSL key store consist : SSL certificate)");

        keyStoreComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "----- Please select SSL key store -----" }));

        removeKeyStoreButton.setText("Remove Key Store");
        removeKeyStoreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeKeyStoreButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(sslKeyStoreCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(keyStoreComboBox, 0, 390, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(addKeyStoreButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(removeKeyStoreButton)
                                .addGap(18, 18, 18))))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(10, 10, 10)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(webServiceChangeButton)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(httpLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addComponent(webServiceHostTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel24)
                                .addGap(6, 6, 6)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel26))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(webServicePortTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel25)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(keyStorePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sslKeyStoreCheckBox)
                    .addComponent(keyStorePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(addKeyStoreButton)
                    .addComponent(keyStoreComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(removeKeyStoreButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(httpLabel)
                    .addComponent(webServiceHostTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(webServicePortTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jLabel26))
                .addGap(18, 18, 18)
                .addComponent(webServiceChangeButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(325, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Program configuration", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 45, Short.MAX_VALUE))
        );
    }// </editor-fold>

	
	
    private void browseInputButtonActionPerformed(java.awt.event.ActionEvent evt) {                                         

    	String inputPath = new BrowseFile().getDirectory();
    	inputFolderTextField.setText(inputPath+"\\"+INPUT_FOLDER);
    	
    }

    
    
    private void browseOutputButtonActionPerformed(java.awt.event.ActionEvent evt) {                                         

    	String outputPath = new BrowseFile().getDirectory();
    	outputFolderTextField.setText(outputPath+"\\"+OUTPUT_FOLDER);
    	
    }

    
    
    private void recordSaveButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	if(checkField()) {
    		
    		PropertiesManagement prop = new PropertiesManagement();
    		PropertiesManagement providerProp = new PropertiesManagement();
        	Token token = null;
        	String keyAlias = null;
        	
        	for (int i = 0; i < slots.length; i++) {
				
    			try {
    				
					if (slots[i].getToken().getTokenInfo().getLabel().trim().equals(hsmSlotComboBox.getSelectedItem().toString().trim())) {
						tokenEnv.SetSlotActive(slots[hsmSlotComboBox.getSelectedIndex()]);
						token = tokenEnv.ConnectTokenWithActiveSlot();
					}
					
				} catch (TokenException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					hsmPasswordField.setText("");
				}
    			
			}
        	
        	
        	token.IsUserPinLock();
    		String tokenException = token.Authentication(hsmPasswordField.getText());
    		
    		
    		if(tokenException == null) {
    			
    			try {
					
					ArrayList<X509Certificate> Cert = token.getCertificate();
					X509Certificate selectedCert = null;
					
					if(Cert == null) {
						JOptionPane.showMessageDialog(null, "No Certificate In Token, Please Remove Token", "Error", JOptionPane.ERROR_MESSAGE);
						
					}
					else {
						
						// Select cert
						int selectedIndex = 0;
						String selectedDN = null;
						String certSerial = null;
						
						if(Cert.size() > 1) {
							ArrayList<String> certDN = new ArrayList<String>();
							
							for (int i = 0; i < Cert.size(); i++) {
								certDN.add(Cert.get(i).getSubjectDN().getName()+","+Cert.get(i).getSerialNumber().toString(16));
							}
							
							selectedDN = (String) JOptionPane.showInputDialog(null, "Certificate : ", "Select Certificate", JOptionPane.PLAIN_MESSAGE, null, certDN.toArray(), certDN.toArray()[0]);
							
							for (int i = 0; i < Cert.size(); i++) {
								if((Cert.get(i).getSubjectDN().getName()+","+Cert.get(i).getSerialNumber().toString(16)).equals(selectedDN)){
									selectedIndex = i;
									certSerial = Cert.get(i).getSerialNumber().toString(16);
								}
							}
						}
						else {
							selectedDN = Cert.get(0).getSubjectDN().toString();
							certSerial = Cert.get(0).getSerialNumber().toString(16);
						}
						
						if(selectedDN != null) {
							
							selectedCert = Cert.get(selectedIndex);
							X509Cert x509Cert = new X509Cert(selectedCert.getEncoded());
							String corporateID = x509Cert.get_OrganizationIdentifier();
							x509Cert.checkValidity();													// Check certificate validity
							x509Cert.VerifyCertificate();												// Verify certificate with chain
							
							if(Main.LicenseFile.IsCorporateTaxInLicenseFile(corporateID)) {
								
								ArrayList<String> KeyLabelOnToken = getDigitalID(token, certSerial);
								if(KeyLabelOnToken.size()>1) {
									String[] possibilities = new String[KeyLabelOnToken.size()];
									KeyLabelOnToken.toArray(possibilities);
									keyAlias = (String) JOptionPane.showInputDialog(null, "Select signing key","Key Selection For Signing", JOptionPane.PLAIN_MESSAGE, null, possibilities, possibilities[0]);
								}
								else if(KeyLabelOnToken.size() == 1) {
									keyAlias = KeyLabelOnToken.get(0);
								}
								
								if(keyAlias != null) {
								
									int count = Integer.parseInt(prop.getProperties("digsig.count"));
									int thisRecordCount = count+1;
									
									prop.setProperties("digsig.count", String.valueOf(thisRecordCount));
									prop.setProperties("digsig."+thisRecordCount+".corporateID", corporateID);
									prop.setProperties("digsig."+thisRecordCount+".operationMode", operationMode);
									prop.setProperties("digsig."+thisRecordCount+".location", locationTextField.getText().trim());
									prop.setProperties("digsig."+thisRecordCount+".reason", reasonTextField.getText().trim());
									
									prop.setProperties("digsig."+thisRecordCount+".urx", urxTextField.getText().trim());
									prop.setProperties("digsig."+thisRecordCount+".ury", uryTextField.getText().trim());
									prop.setProperties("digsig."+thisRecordCount+".llx", llxTextField.getText().trim());
									prop.setProperties("digsig."+thisRecordCount+".lly", llyTextField.getText().trim());
									prop.setProperties("digsig."+thisRecordCount+".fontSize", fontSizeTextField.getText().trim());
									
									prop.setProperties("digsig."+thisRecordCount+".keyAlias", keyAlias);
									prop.setProperties("digsig."+thisRecordCount+".hsmSerial", token.getSerialNumber().trim());
									prop.setProperties("digsig."+thisRecordCount+".hsmDLL", providerProp.getProperties(Constance.PROVIDER_CON_PATH, "provider."+(hsmComboBox.getSelectedIndex())+".path"));
									// Encrypt password
									String encryptedPassword = new EncryptPassword().encrypt(hsmPasswordField.getText().trim(), new SecretKey().createSecretKey());
									prop.setProperties("digsig."+thisRecordCount+".hsmPassword", encryptedPassword);
									
									if(signatureImgComboBox.getSelectedIndex() != 0 && !signatureImgComboBox.getSelectedItem().toString().contains("----- Please select")) {
										prop.setProperties("digsig."+thisRecordCount+".signatureImg", signatureImgComboBox.getSelectedItem().toString().trim());
									}
									else {
										prop.setProperties("digsig."+thisRecordCount+".signatureImg", "");
									}
									
									if(specifiedPageRadioButton.isSelected()) {
										prop.setProperties("digsig."+thisRecordCount+".page", specifiedPageTextField.getText().trim());
									}
									else {
										prop.setProperties("digsig."+thisRecordCount+".page", page);
									}
									
									if(operationMode.equalsIgnoreCase("batch")) {
										prop.setProperties("digsig."+thisRecordCount+".inputFolder", inputFolderTextField.getText().trim());
										prop.setProperties("digsig."+thisRecordCount+".outputFolder", outputFolderTextField.getText().trim());
										prop.setProperties("digsig."+thisRecordCount+".onlineRecordNumber", "");
										createFolder();
									}
									else {
										prop.setProperties("digsig."+thisRecordCount+".inputFolder", "");
										prop.setProperties("digsig."+thisRecordCount+".outputFolder", "");
										prop.setProperties("digsig."+thisRecordCount+".onlineRecordNumber", onlineRecordNumberTextField.getText().trim());
									}
									
									if(tsaCheckBox.isSelected()) {
										prop.setProperties("digsig."+thisRecordCount+".withTSA", "true");
										prop.setProperties("digsig."+thisRecordCount+".tsaURL", tsaURLTextField.getText().trim());
										prop.setProperties("digsig."+thisRecordCount+".tsaUser", tsaUserTextField.getText().trim());
										
										if(tsaPasswordField.getText().trim().equalsIgnoreCase("")) {
											prop.setProperties("digsig."+thisRecordCount+".tsaPassword", "");
										}
										else {
											// Encrypt password
											String encryptedTSAPassword = new EncryptPassword().encrypt(tsaPasswordField.getText().trim(), new SecretKey().createSecretKey());
											prop.setProperties("digsig."+thisRecordCount+".tsaPassword", encryptedTSAPassword);
										}
									}
									else {
										prop.setProperties("digsig."+thisRecordCount+".withTSA", "false");
										prop.setProperties("digsig."+thisRecordCount+".tsaURL", "");
										prop.setProperties("digsig."+thisRecordCount+".tsaUser", "");
										prop.setProperties("digsig."+thisRecordCount+".tsaPassword", "");
									}
									
									clearRecordInTable();
									getRecordFromProp();
									
									
								
								}
								else {
									JOptionPane.showMessageDialog(null, "Not found key alias in token", "Error", JOptionPane.ERROR_MESSAGE);
								}
								
							}
							else {
				    			JOptionPane.showMessageDialog(this, "Your corporate ID not match with license file", "Error", JOptionPane.ERROR_MESSAGE);
				    		}
							
							
						}
						
						
					}
					
				
				} catch (NumberFormatException e) {
					
					if(e.getMessage().equalsIgnoreCase("null")) {
						e.printStackTrace();
					}
					else {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (SignatureException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Verify signature not pass", "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (NullPointerException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "License not allow to use this certificate, Please check license or certificate (Test/Production)", "Error", JOptionPane.ERROR_MESSAGE);
				
				} catch (CertificateExpiredException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Your certificate has expired", "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (CertificateNotYetValidException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Your certificate not yet valid", "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (CertificateEncodingException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (InvalidKeyException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (KeyStoreException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (CertificateException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (NoSuchProviderException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (TokenException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (InvalidKeySpecException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} catch (GeneralSecurityException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					
				} finally {
					token.LogoutSession();
					tokenEnv.DestroyModule();
					resetValueInPanel();
					
				}
			
    		}
			else {
				if(tokenException.equals("CKR_PIN_LOCKED")){
					JOptionPane.showMessageDialog(null, "Etoken password is blocked", "Error", JOptionPane.ERROR_MESSAGE);  //pin lock
					hsmPasswordField.setText("");
				}
				else if(tokenException.equals("CKR_PIN_INCORRECT")){
					JOptionPane.showMessageDialog(null, "Password incorrect", "Error", JOptionPane.ERROR_MESSAGE);  //wrong pin
					hsmPasswordField.setText("");
				}
				else {
					JOptionPane.showMessageDialog(null, tokenException, "Error", JOptionPane.ERROR_MESSAGE);  //wrong pin
					hsmPasswordField.setText("");
				}
				
			}
		
    	}
    	
    }

    
    
    public void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	try {
    		PropertiesManagement prop = new PropertiesManagement();
			int count = Integer.parseInt(prop.getProperties("digsig.count"));
		
	    	int[] selectRows = recordTable.getSelectedRows();
	    	int selectRow;
	    	
	    	for (int i = selectRows.length-1 ; i >= 0 ; i--) {
	    		
	    		selectRow = selectRows[i]+1;
	    		
	    		prop.deleteProperties("digsig."+selectRow+".corporateID");
	    		prop.deleteProperties("digsig."+selectRow+".operationMode");
	    		prop.deleteProperties("digsig."+selectRow+".inputFolder");
	    		prop.deleteProperties("digsig."+selectRow+".outputFolder");
	    		prop.deleteProperties("digsig."+selectRow+".onlineRecordNumber");
	    		prop.deleteProperties("digsig."+selectRow+".signatureImg");
	    		prop.deleteProperties("digsig."+selectRow+".location");
	    		prop.deleteProperties("digsig."+selectRow+".reason");
	    		prop.deleteProperties("digsig."+selectRow+".urx");
	    		prop.deleteProperties("digsig."+selectRow+".ury");
	    		prop.deleteProperties("digsig."+selectRow+".llx");
	    		prop.deleteProperties("digsig."+selectRow+".lly");
	    		prop.deleteProperties("digsig."+selectRow+".fontSize");
	    		prop.deleteProperties("digsig."+selectRow+".page");
	    		prop.deleteProperties("digsig."+selectRow+".keyAlias");
	    		prop.deleteProperties("digsig."+selectRow+".hsmSerial");
	    		prop.deleteProperties("digsig."+selectRow+".hsmDLL");
	    		prop.deleteProperties("digsig."+selectRow+".hsmPassword");
	    		prop.deleteProperties("digsig."+selectRow+".withTSA");
				prop.deleteProperties("digsig."+selectRow+".tsaURL");
				prop.deleteProperties("digsig."+selectRow+".tsaUser");
				prop.deleteProperties("digsig."+selectRow+".tsaPassword");
	    		
	    		
	    		for (int j=0; j < count-selectRow; j++) {
	    			prop.changePropertiesKey("digsig."+(selectRow+1+j), "digsig."+(selectRow+j));
	    		}
	    		
	    		count = count-1;
	    		prop.setProperties("digsig.count", String.valueOf(count));
	    		
	    	}
	    	
	    	clearRecordInTable();
			getRecordFromProp();
			
    	
    	} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
    	
    }
    
    
    
    private void retrieveButtonActionPerformed(java.awt.event.ActionEvent evt) {
        
        try {
        	clearSettingField();
        	
    		PropertiesManagement prop = new PropertiesManagement();
		
	    	int[] selectRows = recordTable.getSelectedRows();
	    	int selectRow;
	    	
	    	for (int i = selectRows.length-1 ; i >= 0 ; i--) {
	    		
	    		selectRow = selectRows[i]+1;
	    		
	    		String operationMode = prop.getProperties("digsig."+selectRow+".operationMode");
	    		if(operationMode.equalsIgnoreCase("batch")) {
	    			this.operationMode = operationMode;
	    			batchRadioButton.setSelected(true);
	    			browseInputButton.setEnabled(true);
	    			browseOutputButton.setEnabled(true);
	    			inputFolderTextField.setEnabled(true);
	    			outputFolderTextField.setEnabled(true);
	    			onlineRecordNumberTextField.setEnabled(false);
	    			
	    			inputFolderTextField.setText(prop.getProperties("digsig."+selectRow+".inputFolder"));
	    			outputFolderTextField.setText(prop.getProperties("digsig."+selectRow+".outputFolder"));
	    		}
	    		else if(operationMode.equalsIgnoreCase("online")) {
	    			this.operationMode = operationMode;
	    			onlineRadioButton.setSelected(true);
	    			browseInputButton.setEnabled(false);
	    			browseOutputButton.setEnabled(false);
	    			inputFolderTextField.setEnabled(false);
	    			outputFolderTextField.setEnabled(false);
	    			onlineRecordNumberTextField.setEnabled(true);
	    			
	    			onlineRecordNumberTextField.setText(prop.getProperties("digsig."+selectRow+".onlineRecordNumber"));
	    		}
	    		else {
	    			// Do nothing
	    		}
	    		
	    		String signatureImg = prop.getProperties("digsig."+selectRow+".signatureImg");
	    		for (int j = 0; j < signatureImgComboBox.getItemCount(); j++) {
					if(signatureImg.equals(signatureImgComboBox.getItemAt(j).toString())) {
						signatureImgComboBox.setSelectedIndex(j);
						break;
					}
				}
	    		
	    		locationTextField.setText(prop.getProperties("digsig."+selectRow+".location"));
	    		reasonTextField.setText(prop.getProperties("digsig."+selectRow+".reason"));
	    		urxTextField.setText(prop.getProperties("digsig."+selectRow+".urx"));
	    		uryTextField.setText(prop.getProperties("digsig."+selectRow+".ury"));
	    		llxTextField.setText(prop.getProperties("digsig."+selectRow+".llx"));
	    		llyTextField.setText(prop.getProperties("digsig."+selectRow+".lly"));
	    		fontSizeTextField.setText(prop.getProperties("digsig."+selectRow+".fontSize"));
	    		
	    		String page = prop.getProperties("digsig."+selectRow+".page");
	    		this.page = page;
	    		if(page.equalsIgnoreCase("first")) {
	    			firstPageRadioButton.setSelected(true);
	    			specifiedPageTextField.setEnabled(false);
	    		}
	    		else if(page.equalsIgnoreCase("last")) {
	    			lastPageRadioButton.setSelected(true);
	    			specifiedPageTextField.setEnabled(false);
	    		}
	    		else {
	    			specifiedPageRadioButton.setSelected(true);
	    			specifiedPageTextField.setEnabled(true);
	    			specifiedPageTextField.setText(page);
	    		}
	    		
	    		String withTSA = prop.getProperties("digsig."+selectRow+".withTSA");
	    		if(withTSA.equalsIgnoreCase("true")) {
	    			tsaCheckBox.setSelected(true);
	    			tsaURLTextField.setEnabled(true);
	    			tsaUserTextField.setEnabled(true);
	    			tsaPasswordField.setEnabled(true);
	    			
	    			tsaURLTextField.setText(prop.getProperties("digsig."+selectRow+".tsaURL"));
	    			tsaUserTextField.setText(prop.getProperties("digsig."+selectRow+".tsaUser"));
	    			
	    		}
	    		else if(withTSA.equalsIgnoreCase("false")) {
	    			tsaCheckBox.setSelected(false);
	    			tsaURLTextField.setEnabled(false);
	    			tsaUserTextField.setEnabled(false);
	    			tsaPasswordField.setEnabled(false);
	    			
	    		}
				
	    		
	    	}
			
    	
    	} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
        
    }
    
    
    
    public void clearSettingField() {
    	
    	onlineRecordNumberTextField.setText("");
    	inputFolderTextField.setText("");
		outputFolderTextField.setText("");
		specifiedPageTextField.setText("");
		tsaCheckBox.setSelected(false);
		tsaURLTextField.setText("");
		tsaUserTextField.setText("");
		tsaPasswordField.setText("");
		
		signatureImgComboBox.setSelectedIndex(0);
		
		hsmComboBox.setSelectedIndex(0);
		hsmSlotComboBox.setSelectedIndex(0);
		hsmSlotComboBox.removeAllItems();
		hsmSlotComboBox.addItem("----- Please select slot -----");
		hsmPasswordField.setText("");
    	
    }
    
    
    
    private void webServiceChangeButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	PropertiesManagement connectionProp = new PropertiesManagement();
    	PropertiesManagement programProp = new PropertiesManagement();
    	
        if(checkWSSettingField()) {
        	
        	try {
        		
        		/*// Read previous data in prop file before save
        		prop.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.host");
        		prop.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.port");*/
        		
        		if(sslKeyStoreCheckBox.isSelected()) {
    	    			
	    			// Write data to prop file
        			programProp.setProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore",Constance.SSL_FOLDER_PATH+keyStoreComboBox.getSelectedItem().toString().trim());
            		// Encrypt password
					String encryptedKeyStorePassword = new EncryptPassword().encrypt(keyStorePasswordField.getText().trim(), new SecretKey().createSecretKey());
					programProp.setProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStorePassword",encryptedKeyStorePassword);
            		
            		connectionProp.setProperties(Constance.CONNECTION_CON_PATH, "connection.webService.host", webServiceHostTextField.getText().trim());
            		connectionProp.setProperties(Constance.CONNECTION_CON_PATH, "connection.webService.port", webServicePortTextField.getText().trim());
            		connectionProp.setProperties(Constance.CONNECTION_CON_PATH, "connection.webService.secureMode", Boolean.TRUE.toString());
            		
            		JOptionPane.showMessageDialog(this, "Saved", "Save", JOptionPane.INFORMATION_MESSAGE);
        			
        		}
        		else {
        			// Write data to prop file
            		connectionProp.setProperties(Constance.CONNECTION_CON_PATH, "connection.webService.host", webServiceHostTextField.getText().trim());
            		connectionProp.setProperties(Constance.CONNECTION_CON_PATH, "connection.webService.port", webServicePortTextField.getText().trim());
            		connectionProp.setProperties(Constance.CONNECTION_CON_PATH, "connection.webService.secureMode", Boolean.FALSE.toString());
        			
        			programProp.setProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore","");
            		programProp.setProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStorePassword","");
            		
            		JOptionPane.showMessageDialog(this, "Saved", "Save", JOptionPane.INFORMATION_MESSAGE);
        		}
	        	
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} finally {
				keyStorePasswordField.setText("");
			}
        	
        }
        
    }
    
    
    
    
    private void onlineRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	operationMode = "online";
        browseInputButton.setEnabled(false);
        browseOutputButton.setEnabled(false);
        inputFolderTextField.setEnabled(false);
		outputFolderTextField.setEnabled(false);
        onlineRecordNumberTextField.setEnabled(true);
        
    }
    
    
    

    private void batchRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	operationMode = "batch";
        browseInputButton.setEnabled(true);
        browseOutputButton.setEnabled(true);
        inputFolderTextField.setEnabled(true);
		outputFolderTextField.setEnabled(true);
        onlineRecordNumberTextField.setEnabled(false);
        
    }
    
    
    
    private void sslKeyStoreCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
    	
        if(sslKeyStoreCheckBox.isSelected()) {
        	keyStoreComboBox.setEnabled(true);
        	addKeyStoreButton.setEnabled(true);
        	removeKeyStoreButton.setEnabled(true);
        	keyStorePasswordField.setEnabled(true);
        	httpLabel.setText("https://");
        }
        else {
        	keyStoreComboBox.setEnabled(false);
        	addKeyStoreButton.setEnabled(false);
        	removeKeyStoreButton.setEnabled(false);
        	keyStorePasswordField.setEnabled(false);
        	httpLabel.setText("http://");
        }
        
    }
    
    
    
    private void addKeyStoreButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                     

    	FileOperation file = new FileOperation();
    	String path = new BrowseFile().getFilePath();
    	String name = file.getNameFromPath(path);
    	String[] filter = {".p12",".jks",".P12",".JKS"};
    	
    	try {
    		
    		if(new ExtensionFileFilter("Key Store File", filter).accept(file.getFile(path))) {
    			
	    		if(!file.isFileExist(Constance.SSL_FOLDER_PATH+name)) {
	    			file.Operate(path, Constance.SSL_FOLDER_PATH+name, 3);
	    		}
	    		else {
	    			JOptionPane.showMessageDialog(this, "This key store added to program already", "Error", JOptionPane.ERROR_MESSAGE);
	    		}
	    		
    		}
    		else {
    			JOptionPane.showMessageDialog(this, "Please browse key store file (.p12/.jks)", "Error", JOptionPane.ERROR_MESSAGE);
    		}
			
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		finally {
			try {
				setKeyStoreComboBox();
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
    	
    }
    
    

    private void removeKeyStoreButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	FileOperation file = new FileOperation();
    	String fileName = keyStoreComboBox.getSelectedItem().toString();
    	
    	try {
    		
			file.Operate(Constance.SSL_FOLDER_PATH+fileName, null, 2);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				setKeyStoreComboBox();
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
    	
    }
    
    
    
    private void firstPageRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {
        page = "first";
        specifiedPageTextField.setEnabled(false);
    }

    
    
    private void lastPageRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	page = "last";
    	specifiedPageTextField.setEnabled(false);
    }

    
    
    private void specifiedPageRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {
        page = "specified";
        specifiedPageTextField.setEnabled(true);
    }

    
    
    private void specifiedPageTextFieldKeyTyped(java.awt.event.KeyEvent evt) {
    	
    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }
    
    
    
    private void addSignatureImgButtonActionPerformed(java.awt.event.ActionEvent evt) {

    	FileOperation file = new FileOperation();
    	String path = new BrowseFile().getFilePath();
    	String name = file.getNameFromPath(path);
    	String[] filter = {".jpg",".png",".JPG",".PNG"};
    	
    	try {
    		
    		if(new ExtensionFileFilter("Signature image file", filter).accept(file.getFile(path))) {
    			
	    		if(!file.isFileExist(Constance.SIGNATURE_IMG_FOLDER_PATH+name)) {
	    			file.Operate(path, Constance.SIGNATURE_IMG_FOLDER_PATH+name, 3);
	    		}
	    		else {
	    			JOptionPane.showMessageDialog(this, "This image added to program already", "Error", JOptionPane.ERROR_MESSAGE);
	    		}
	    		
    		}
    		else {
    			JOptionPane.showMessageDialog(this, "Please browse image file (.jpg/.png)", "Error", JOptionPane.ERROR_MESSAGE);
    		}
			
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		finally {
			setSignatureImgComboBox();
		}
    	
    }
    
    
    
    private void removeSignatureImgButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                        

    	FileOperation file = new FileOperation();
    	String fileName = signatureImgComboBox.getSelectedItem().toString();
    	
    	try {
    		
			file.Operate(Constance.SIGNATURE_IMG_FOLDER_PATH+fileName, null, 2);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			setSignatureImgComboBox();
		}
    	
    }
    
    
    
    private void urxTextFieldKeyTyped(java.awt.event.KeyEvent evt) {
    	
    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }

    
    
    private void uryTextFieldKeyTyped(java.awt.event.KeyEvent evt) {

    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }

    
    
    private void llxTextFieldKeyTyped(java.awt.event.KeyEvent evt) {

    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }

    
    
    private void llyTextFieldKeyTyped(java.awt.event.KeyEvent evt) {

    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }

    
    
    private void fontSizeTextFieldKeyTyped(java.awt.event.KeyEvent evt) {

    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }
    
    
    
    private void webServicePortTextFieldKeyTyped(java.awt.event.KeyEvent evt) {
       	
    	if(isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }
    
    
    
    private void onlineRecordNumberTextFieldKeyTyped(java.awt.event.KeyEvent evt) {
    	
    	if(isNotOverLimit(onlineRecordNumberTextField,ONLINE_REECORD_NUMBER_LIMIT) && isDigits(evt)) {
    		// Do nothing
    	}
    	else {
    		evt.consume();
    	}
    	
    }
    
    
    
    private void hsmComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        
    	PropertiesManagement prop = new PropertiesManagement();
    	
    	int selectedIndex = hsmComboBox.getSelectedIndex();
    	ArrayList<String> availableDriver = new ArrayList<String>();
    	
    	if(selectedIndex!=0) {
    	
	    	try {
				String dll = prop.getProperties(Constance.PROVIDER_CON_PATH, "provider."+selectedIndex+".path");
				availableDriver = tokenEnv.FindAvailableDriver(dll);
				
				if(availableDriver.size()>0) {
					setSlotComboBox(dll);
				}
				else {
					JOptionPane.showMessageDialog(null, "No any token driver install", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (IOException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
    	
		
    	}
    	
    }
    
    
    
    private void tsaCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
        
        if(tsaCheckBox.isSelected()) {
        	tsaURLTextField.setEnabled(true);
        	tsaUserTextField.setEnabled(true);
        	tsaPasswordField.setEnabled(true);
        }
        else {
        	tsaURLTextField.setEnabled(false);
        	tsaUserTextField.setEnabled(false);
        	tsaPasswordField.setEnabled(false);
        }
        
    }
    
    
    
    
    public void setHSMComBox() throws NumberFormatException, FileNotFoundException, IOException {
    	
    	PropertiesManagement prop = new PropertiesManagement();
    	int count = Integer.parseInt(prop.getProperties(Constance.PROVIDER_CON_PATH, "provider.count"));
    	
    	if(count > 0) {
    		
    		for (int i = 1; i <= count; i++) {
    			hsmComboBox.addItem(prop.getProperties(Constance.PROVIDER_CON_PATH, "provider."+i+".name"));
			}
    		
    	}
    	
    }
    
    
    
    public void setSlotComboBox(String dll) {
    	
    	hsmSlotComboBox.removeAllItems();
    	
		try {
			tokenEnv.LoadDll(dll);
	    	slots = tokenEnv.getSlotsList();
	    	
	    	hsmSlotComboBox.removeAllItems();
	    	
	    	if(slots.length > 0) {
	    		
		    	for (int i = 0; i < slots.length; i++) {
		    		hsmSlotComboBox.addItem(slots[i].getToken().getTokenInfo().getLabel().trim());
				}
		    	
	    	}
	    	else {
	    		//slotComboBox.addItem("----- Please select slot -----");
	    	}
			
		} catch (TokenException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
    	
    	
    	
    }
    
    
    
    public void setSignatureImgComboBox() {
    	
    	File f = new File(Constance.SIGNATURE_IMG_FOLDER_PATH);
    	File[] files = f.listFiles();
    	
    	if(files.length > 0) {
    		signatureImgComboBox.removeAllItems();
    		signatureImgComboBox.addItem("----- Please select signature image (Optional) -----");
    		for (int i = 0; i < files.length; i++) {
    			signatureImgComboBox.addItem(files[i].getName());
			}
    		
    	}
    	else {
    		signatureImgComboBox.removeAllItems();
    		signatureImgComboBox.addItem("----- Please select signature image (Optional) -----");
    	}
    	
    }
    
    
    
    public void setKeyStoreComboBox() throws FileNotFoundException, IOException {
    	
    	PropertiesManagement programProp = new PropertiesManagement();
    	File f = new File(Constance.SSL_FOLDER_PATH);
    	File[] files = f.listFiles();
    	
    	if(files.length > 0) {
    		keyStoreComboBox.removeAllItems();
    		keyStoreComboBox.addItem("----- Please select SSL key store -----");
    		for (int i = 0; i < files.length; i++) {
    			keyStoreComboBox.addItem(files[i].getName());
			}
    		
    		if(!programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore").equalsIgnoreCase("")) {
    			keyStoreComboBox.setSelectedItem(programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore"));
    		}
    		
    	}
    	else {
    		keyStoreComboBox.removeAllItems();
    		keyStoreComboBox.addItem("----- Please select SSL key store -----");
    	}
    	
    }
    
    
    
    public boolean checkField() {
    	
    	boolean result = true;
    	
    	
    	if(onlineRadioButton.isSelected()) {
    		
    		int NUM_OF_CASE = 2;
    		
    		for (int i = 1; i <= NUM_OF_CASE; i++) {
        		
        		switch (i){
        		
	        		case 1 : 
		    			if (onlineRecordNumberTextField.getText().length() != ONLINE_REECORD_NUMBER_LIMIT) {
		    				JOptionPane.showMessageDialog(this, "Online record number must be "+ONLINE_REECORD_NUMBER_LIMIT+" digits", "Error", JOptionPane.ERROR_MESSAGE);
			    			result = false;
			    			break;
		    			}
	        		case 2 : 
		    			if (isDuplicateOnlineRecordNumber(onlineRecordNumberTextField.getText())) {
		    				JOptionPane.showMessageDialog(this, "Online record number is duplicate", "Error", JOptionPane.ERROR_MESSAGE);
			    			result = false;
			    			break;
		    			}
    	    		
        		}
        		
        		if(result == false) {
        			break;
        		}
        		
    		}
    		
    	}
    	else {
    		
    		int NUM_OF_CASE = 2;
    		
    		for (int i = 01; i <= NUM_OF_CASE; i++) {
        		
        		switch (i){
        		
    	    		case 1 : 
    	    			if (inputFolderTextField.getText().length() == 0) {
    	    				JOptionPane.showMessageDialog(this, "Please browse your input folder", "Error", JOptionPane.ERROR_MESSAGE);
    	    				result = false;
    	    				break;
    	    			} 
    	    			else {
    	    				
    	    				try {
    	    					
    	    					PropertiesManagement prop = new PropertiesManagement();
								int count = Integer.parseInt(prop.getProperties("digsig.count"));
								if(count > 0) {
									for (int j = 0; j < count; j++) {
										if(prop.getProperties("digsig."+count+".inputFolder").equalsIgnoreCase(inputFolderTextField.getText().trim())) {
											JOptionPane.showMessageDialog(this, "Input folder already exists", "Error", JOptionPane.ERROR_MESSAGE);
											result = false;
				    	    				break;
										}
									}
								}
								
							} catch (NumberFormatException | IOException e) {
								e.printStackTrace();
								JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								result = false;
	    	    				break;
							}
    	    				
    	    			}
    	    		case 2 : 
    	    			if (outputFolderTextField.getText().length() == 0) {
    	    				JOptionPane.showMessageDialog(this, "Please browse your output folder", "Error", JOptionPane.ERROR_MESSAGE);
    	    				result = false;
    	    				break;
    	    			}
    	    		
        		}
        		
        		if(result == false) {
        			break;
        		}
        		
    		}
    		
    	}
    	
    	
    	
    	// General check
    	if(result) {
	    	int NUM_OF_CASE = 10;
			
			for (int i = 1; i <= NUM_OF_CASE; i++) {
	    		
	    		switch (i){
	    		
		    		case 1 :
		    			if(hsmComboBox.getSelectedItem().toString().trim().equalsIgnoreCase("----- Please select HSM -----")) {
		    				JOptionPane.showMessageDialog(this, "Please select HSM", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 2 :
		    			if(hsmSlotComboBox.getSelectedItem().toString().trim().equalsIgnoreCase("") || hsmSlotComboBox.getSelectedItem().toString().trim().equalsIgnoreCase("----- Please select slot -----")) {
		    				JOptionPane.showMessageDialog(this, "Please select HSM slot", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 3 :
		    			if(hsmPasswordField.getPassword().length <= 0) {
		    				JOptionPane.showMessageDialog(this, "Please enter a password", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
	    				}
		    		case 4 : 
		    			if (urxTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified Upper Right X", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 5 : 
		    			if (uryTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified Upper Right Y", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 6 : 
		    			if (llxTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified Lower Left X", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 7 : 
		    			if (llyTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified Lower Left Y", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 8 : 
		    			if (fontSizeTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified font size", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 9 : 
		    			if (specifiedPageRadioButton.isSelected() && specifiedPageTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified page", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		case 10 :
		    			if(tsaCheckBox.isSelected() && tsaURLTextField.getText().length() == 0) {
		    				JOptionPane.showMessageDialog(this, "Please specified TSA URL", "Error", JOptionPane.ERROR_MESSAGE);
		    				result = false;
		    				break;
		    			}
		    		
	    		}
	    		
	    		if(result == false) {
	    			break;
	    		}
	    		
			}
			
    	}
    	
    	
    	return result;
    	
    }
    
    
    
    public void getWebServiceSettingFromProp() throws FileNotFoundException, IOException {
    	
    	PropertiesManagement connectionProp = new PropertiesManagement();
    	PropertiesManagement programProp = new PropertiesManagement();
    	
    	webServiceHostTextField.setText(connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.host"));
    	webServicePortTextField.setText(connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.port"));
    	
    	setKeyStoreComboBox();
    	String[] tmp = programProp.getProperties(Constance.PROGRAM_CON_PATH, "program.sslKeyStore").split("/");
    	String fileName = tmp[tmp.length-1];
    	for (int i = 0; i < keyStoreComboBox.getItemCount(); i++) {
			if(keyStoreComboBox.getItemAt(i).toString().equalsIgnoreCase(fileName)) {
				keyStoreComboBox.setSelectedIndex(i);
				break;
			}
		}
    	
    	if(connectionProp.getProperties(Constance.CONNECTION_CON_PATH, "connection.webService.secureMode").equalsIgnoreCase(Boolean.TRUE.toString()) && Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE) && Main.LicenseFile.getProgramType().equalsIgnoreCase(Constance.PROGRAM_TYPE_PLUS)) {
    		sslKeyStoreCheckBox.setSelected(true);
    		removeKeyStoreButton.setEnabled(true);
        	keyStorePasswordField.setEnabled(true);
        	addKeyStoreButton.setEnabled(true);
        	keyStoreComboBox.setEnabled(true);
        	httpLabel.setText("https://");
    	}
    	
    }
    
    
    
    public void getRecordFromProp() throws NumberFormatException, FileNotFoundException, IOException {
    	
    	PropertiesManagement prop = new PropertiesManagement();
        int count = Integer.valueOf(prop.getProperties("digsig.count"));
        
        if(count > 0){
            
            for(int i = 1;i<=count;i++){
            	String corporateID = prop.getProperties("digsig."+i+".corporateID");
            	String urx = prop.getProperties("digsig."+i+".urx");
            	String ury = prop.getProperties("digsig."+i+".ury");
            	String llx = prop.getProperties("digsig."+i+".llx");
            	String lly = prop.getProperties("digsig."+i+".lly");
            	String fontSize = prop.getProperties("digsig."+i+".fontSize");
            	String page = prop.getProperties("digsig."+i+".page");
            	String onlineRecordNumber = prop.getProperties("digsig."+i+".onlineRecordNumber");
            	String inputFolder = prop.getProperties("digsig."+i+".inputFolder");
            	String outputFolder = prop.getProperties("digsig."+i+".outputFolder");
            	String hsmSerial = prop.getProperties("digsig."+i+".hsmSerial");
            	String keyAlias = prop.getProperties("digsig."+i+".keyAlias");
            	
                Object[] data = {i, corporateID, urx, ury, llx, lly, fontSize, page, onlineRecordNumber, inputFolder, outputFolder, hsmSerial, keyAlias};
                tm.addRow(data);

            }
            
        }
        
    }
    
    
    
    
    public void clearRecordInTable(){
    	
    	int rowCount = tm.getRowCount();
    	if(rowCount > 0){
			for (int i = 0; i < rowCount; i++) {
				tm.removeRow(0);
			}
    	}
    	else{
    		//Do nothing
    	}
    		
    }
    
    
    
    
    public void createFolder(){
    	
    	//// Create Input folder
    	File folder = new File(inputFolderTextField.getText().trim());
        folder.mkdir();
        
    	
    	//// Create Output folder
    	folder = new File(outputFolderTextField.getText().trim());
    	if(folder.mkdir()){
    		folder = new File(outputFolderTextField.getText().trim()+"/Success");
            folder.mkdir();
            folder = new File(outputFolderTextField.getText().trim()+"/Fail");
            folder.mkdir();
    	}
    	
	}
    
    
    
    public boolean isIPAddress(String ip) {
    	
    	String IPADDRESS_PATTERN = 
    		"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    	Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
    	Matcher matcher = pattern.matcher(ip);
    	
    	return matcher.matches();
    	
    }
    
    
    
    public boolean isNotOverLimit(JTextField field, int limit) {
    	
    	boolean result = true;
    	
    	if(field.getText().length()>=limit) {
    		result = false;
    	}
    	
    	return result;
    	
    }
    
    
    
    
    public boolean isDigits(java.awt.event.KeyEvent evt) {
    	
    	boolean result = true;
    	
        char c = evt.getKeyChar();
        if(!(Character.isDigit(c))){
        	result = false;
        }
        
        return result;
        
    }
    
    
    
    public boolean isDuplicateOnlineRecordNumber(String onlineRecordNumber) {
    	
    	boolean result = false;
    	int count = tm.getRowCount();
    	
    	for (int i = 0; i < count; i++) {
			
    		if(tm.getValueAt(i, 8) != null) {
	    		if(onlineRecordNumber.equalsIgnoreCase(tm.getValueAt(i, 8).toString())) {
	    			result = true;
	    			break;
	    		}
    		}
    		
		}
    	
    	return result;
    	
    }
    
    
    
    
    public void resetValueInPanel() {
    	
    	hsmComboBox.setSelectedIndex(0);
    	hsmSlotComboBox.removeAllItems();
    	hsmSlotComboBox.addItem("----- Please select slot -----");
    	hsmPasswordField.setText("");
    	
    	tsaPasswordField.setText("");
    	
    }
    
    
    
    
    public boolean checkWSSettingField() {
    	
    	boolean result = true;
		int NUM_OF_CASE = 4;
		
		for (int i = 1; i <= NUM_OF_CASE; i++) {
    		
    		switch (i){
    		
	    		/*case 1 :
	    			if(!isIPAddress(webServiceIPTextField.getText().trim())) {
	    				JOptionPane.showMessageDialog(this, "Please specified Web Service IP in format", "Error", JOptionPane.ERROR_MESSAGE);
	    				result = false;
	    				break;
	    			}*/
    			case 1 :
	    			if(webServicePortTextField.getText().trim().equalsIgnoreCase("")) {
	    				JOptionPane.showMessageDialog(this, "Please specified Web Service port in format", "Error", JOptionPane.ERROR_MESSAGE);
	    				result = false;
	    				break;
	    			}
    			case 2 :
	    			if(Integer.parseInt(webServicePortTextField.getText()) < 0 && Integer.parseInt(webServicePortTextField.getText()) > 65535) {
	    				JOptionPane.showMessageDialog(this, "Please specified Web Service port in format", "Error", JOptionPane.ERROR_MESSAGE);
	    				result = false;
	    				break;
	    			}
    			case 3 :
	    			if(sslKeyStoreCheckBox.isSelected() && keyStoreComboBox.getSelectedIndex() == 0 && keyStoreComboBox.getSelectedItem().toString().contains("----- Please select")) {
	    				JOptionPane.showMessageDialog(this, "Please select your SSL key store", "Error", JOptionPane.ERROR_MESSAGE);
	    				result = false;
	    				break;
	    			}
    			case 4 :
	    			if(sslKeyStoreCheckBox.isSelected() && keyStorePasswordField.getText().equalsIgnoreCase("")) {
	    				JOptionPane.showMessageDialog(this, "Please specified SSL key store password", "Error", JOptionPane.ERROR_MESSAGE);
	    				result = false;
	    				break;
	    			}
	    		
    		}
    		
    		if(result == false) {
    			break;
    		}
    		
		}
    	
    	
    	return result;
    	
    }
    
    
    
    public ArrayList<String> getDigitalID(Token token, String certSerial) throws TokenException, CertificateException {
		
		
		
		ArrayList<String> DigitalID = new ArrayList<String>();
		
		RSAPrivateKey RSAPrivateKeyTemplate = new RSAPrivateKey();
		Object[] PrivateKeyObjects = token.FindObject(RSAPrivateKeyTemplate);
		
		for(int i=0;i<PrivateKeyObjects.length;i++) {
			
			RSAPrivateKey RSAPrivateKey = (RSAPrivateKey) PrivateKeyObjects[i];
		
			
			RSAPublicKey RSAPublicKeyTemplate = new RSAPublicKey();
			
			
			 MessageDigest md = null;
			try {
				md = MessageDigest.getInstance("SHA1");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 md.update(new String(RSAPrivateKey.getLabel().getCharArrayValue()).getBytes());
			 //CertTemplate.getId().setByteArrayValue(md.digest());
			 
			 
			RSAPublicKeyTemplate.getId().setByteArrayValue(md.digest());
			RSAPublicKeyTemplate.getLabel().setCharArrayValue(RSAPrivateKey.getLabel().getCharArrayValue());
			
			Object[] PublicKeyObjects = token.FindObject(RSAPublicKeyTemplate);
			
			if(PublicKeyObjects == null)
				continue;
			
			for(int j=0;j<PublicKeyObjects.length;j++){
				
				RSAPublicKey RSAPublicKey = (RSAPublicKey) PublicKeyObjects[j];
				
				X509PublicKeyCertificate CertTemplate = new X509PublicKeyCertificate();
				CertTemplate.getId().setByteArrayValue(RSAPublicKey.getId().getByteArrayValue());
				CertTemplate.getLabel().setCharArrayValue(RSAPublicKey.getLabel().getCharArrayValue());
				//CertTemplate.getSerialNumber().setByteArrayValue(certSerial.getBytes());
				
				Object[] CertObjects = token.FindObject(CertTemplate);
				
				if(CertObjects == null)
					continue;
				
				for(int k=0;k<CertObjects.length;k++) {
					
					X509PublicKeyCertificate X509PublicKeyCertificate = (X509PublicKeyCertificate)CertObjects[k];
					
					CertificateFactory certFac = CertificateFactory.getInstance("X.509");
					byte[] encCert = X509PublicKeyCertificate.getValue().getByteArrayValue();
					X509Certificate x509Cert = (X509Certificate)certFac.generateCertificate(new ByteArrayInputStream(encCert));		// Convert to X509 Certificate
					
					// Compare the serial number of certificate
					if(x509Cert.getSerialNumber().toString(16).equals(certSerial)) {
						DigitalID.add(new String(X509PublicKeyCertificate.getLabel().getCharArrayValue()));
						
					}
					
				}
				
			}
			
			
		}
		

		return DigitalID;
		
	}
    

    
    
    // Variables declaration - do not modify
    private javax.swing.JButton addKeyStoreButton;
    private javax.swing.JButton addSignatureImgButton;
    private javax.swing.JRadioButton batchRadioButton;
    private javax.swing.JButton browseInputButton;
    private javax.swing.JButton browseOutputButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton deleteButton;
    private javax.swing.JRadioButton firstPageRadioButton;
    private javax.swing.JTextField fontSizeTextField;
    private javax.swing.JComboBox hsmComboBox;
    private javax.swing.JPasswordField hsmPasswordField;
    private javax.swing.JComboBox hsmSlotComboBox;
    private javax.swing.JLabel httpLabel;
    private javax.swing.JLabel imgLabel;
    private javax.swing.JTextField inputFolderTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JComboBox keyStoreComboBox;
    private javax.swing.JPasswordField keyStorePasswordField;
    private javax.swing.JRadioButton lastPageRadioButton;
    private javax.swing.JTextField llxTextField;
    private javax.swing.JTextField llyTextField;
    private javax.swing.JTextField locationTextField;
    private javax.swing.JRadioButton onlineRadioButton;
    private javax.swing.JTextField onlineRecordNumberTextField;
    private javax.swing.JTextField outputFolderTextField;
    private javax.swing.JTextField reasonTextField;
    private javax.swing.JButton recordSaveButton;
    private javax.swing.JTable recordTable;
    private javax.swing.JButton removeKeyStoreButton;
    private javax.swing.JButton removeSignatureImgButton;
    private javax.swing.JButton retrieveButton;
    private javax.swing.JComboBox signatureImgComboBox;
    private javax.swing.JRadioButton specifiedPageRadioButton;
    private javax.swing.JTextField specifiedPageTextField;
    private javax.swing.JCheckBox sslKeyStoreCheckBox;
    private javax.swing.JCheckBox tsaCheckBox;
    private javax.swing.JPasswordField tsaPasswordField;
    private javax.swing.JTextField tsaURLTextField;
    private javax.swing.JTextField tsaUserTextField;
    private javax.swing.JTextField urxTextField;
    private javax.swing.JTextField uryTextField;
    private javax.swing.JButton webServiceChangeButton;
    private javax.swing.JTextField webServiceHostTextField;
    private javax.swing.JTextField webServicePortTextField;
    // End of variables declaration
}
