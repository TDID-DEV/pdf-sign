package PDFSign.panel;

import iaik.pkcs.pkcs11.Slot;
import iaik.pkcs.pkcs11.TokenException;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.security.SignatureException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Logger;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.datetime.DateTime;
import PDFSign.properties.PropertiesManagement;
import PDFSign.thread.MainThread;
import PDFSign.thread.ReadLogFileThread;
import PDFSign.token.Token;
import PDFSign.token.TokenEnvironment;
import PDFSign.windowsService.ServiceOperation;
import PDFSign.x509certificate.GetCertValue;
import PDFSign.x509certificate.X509Cert;


public class MainPanel extends javax.swing.JPanel implements Runnable {
	
final static Logger logger = Logger.getLogger(MainPanel.class);
	
	GetCertValue getCertValue = new GetCertValue();
	TokenEnvironment TokenEnv = new TokenEnvironment();
	public static Token Token = null;
	ServiceOperation so = null;
	
	Thread TokenRemove,TokenInsert;
	
	ArrayList<String> AvailableDll;
	
	private boolean enableRun = true;
	
	Slot[] slots;
	
	MainThread mt;
	
	

	public MainPanel() {

		initComponents();
		
		
		if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE)) {
			
			so = new ServiceOperation();
			loginButton.setEnabled(false);
	        passwordField.setEnabled(false);
			
			if(so.isServiceStart()) {
				jTabbedPane1.setSelectedIndex(1);
				
				Document doc = this.textPane.getDocument();
		        SimpleAttributeSet set = new SimpleAttributeSet();
    			StyleConstants.setFontSize(set, 12);
		        try {
					doc.insertString(doc.getLength(), "["+new DateTime().getDateTime()+"] ..................................... Service "+Constance.PROGRAM_NAME+" is running"+"\r\n", set);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
			
			
			// Start ReadLogFileThread
			ReadLogFileThread rl = new ReadLogFileThread(so);
	        rl.start();
			
			
		}
		else {
	        stopButton.setEnabled(false);
	        loginButton.setEnabled(false);
	        passwordField.setEnabled(false);
	        
		}
		
        
		
        AvailableDll = TokenEnv.FindAvailableDriverToken();
        
        if(AvailableDll.size()==0){
        	JOptionPane.showMessageDialog(null, "No any token driver install", "Error", JOptionPane.ERROR_MESSAGE);
        	
        }
		else {
			
			int c;
			
			for(c=0;c<AvailableDll.size();c++) {
			
				TokenEnv.LoadDll(AvailableDll.get(c));
				
				if(TokenEnv.isTokenOnSlot()) {
					break;	
				}
				TokenEnv.DestroyModule();
			
			}
			
			TokenInsert = new Thread(this);
			
			if(c<AvailableDll.size()) {
				OnInsertToken();
				TokenInsert.start();
				
			}
			
			else {
				TokenEnv.LoadDll(AvailableDll.get(0));
				TokenInsert = new Thread(this);
				TokenInsert.start();
				
			}
			
		}
        
    }

	
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        passwordLabel = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        loginButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        loginSlotComboBox = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        startButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        textPane = new javax.swing.JTextPane();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(1045, 595));

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(1050, 550));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.blue, null), "Login"));

        passwordLabel.setText("Password : ");

        passwordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                passwordFieldKeyPressed(evt);
            }
        });

        loginButton.setText("Login");
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("Slot :");

        loginSlotComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "----- Please insert token -----" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(374, 374, 374)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(passwordLabel)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(loginButton))
                    .addComponent(loginSlotComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(385, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(loginSlotComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordLabel)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(loginButton))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(394, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Configuration", jPanel1);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.blue, null), "Processing"));

        startButton.setText("Start");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        stopButton.setText("Stop");
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(textPane);

        jLabel1.setText("Date time");

        jLabel2.setText("Result");

        jLabel3.setText("Description");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(411, Short.MAX_VALUE)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(412, 412, 412))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 993, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel1)
                        .addGap(94, 94, 94)
                        .addComponent(jLabel2)
                        .addGap(122, 122, 122)
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startButton)
                    .addComponent(stopButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Processing", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1045, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 45, Short.MAX_VALUE))
        );
    }// </editor-fold>
	

	
	public void run() {
		
		while(enableRun){
			
			if(TokenEnv.isSlotActive()) {
				if(TokenEnv.TokenRemoveListener()) {
					OnRemoveToken();
					
				}
				
			}
			else {
				if(TokenEnv.TokenInsertListener()) 	{
					OnInsertToken();
					
				}
			}
		
			try {
				Thread.sleep(250);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}					
		
	}
	
    
    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE)) {
    		
    		if(so.isServiceStart()) {
    			JOptionPane.showMessageDialog(null, "Please stop "+Constance.PROGRAM_NAME+" on windows services", "Error", JOptionPane.ERROR_MESSAGE);
    			passwordField.setText("");
    		
    		}
    		else {
    			
    			if(passwordField.getText().length() > 0) {
    	    		
    	    		if(startButton.isEnabled()) {
    	    			TokenEnv.SetSlotActive(slots[loginSlotComboBox.getSelectedIndex()]);
    	    			Token = TokenEnv.ConnectTokenWithActiveSlot();
    	    			AuthenticationProcess();
    	    		}
    	    		else {
    	    			JOptionPane.showMessageDialog(null, "Please stop automatic processing mode", "Error", JOptionPane.ERROR_MESSAGE);
    	    			passwordField.setText("");
    	    		}
    				
    			}
    			else{
    				JOptionPane.showMessageDialog(null, "Please enter your token pin", "Error", JOptionPane.ERROR_MESSAGE);
    			}
    			
    		}
    		
    	}
    	else {
    	
	    	if(passwordField.getText().length() > 0) {
	    		
	    		if(startButton.isEnabled()) {
	    			TokenEnv.SetSlotActive(slots[loginSlotComboBox.getSelectedIndex()]);
	    			Token = TokenEnv.ConnectTokenWithActiveSlot();
	    			AuthenticationProcess();
	    		}
	    		else {
	    			JOptionPane.showMessageDialog(null, "Please stop automatic processing mode", "Error", JOptionPane.ERROR_MESSAGE);
	    			passwordField.setText("");
	    		}
				
			}
			else{
				JOptionPane.showMessageDialog(null, "Please enter your token pin", "Error", JOptionPane.ERROR_MESSAGE);
			}
    	
    	}
    	
    }

    
    
    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE)) {
    		if(so.isServiceStart()) {
    			System.out.println("..................................... Service "+Constance.PROGRAM_NAME+" is already running");
    		}
    		else {
	    		so.startWindowsService();
	    		System.out.println("..................................... Start service "+Constance.PROGRAM_NAME);
    		}
    		
    	}
    	else {
	    	PropertiesManagement prop = new PropertiesManagement();
	    	
	    	try {
	    		
	    		int count = Integer.parseInt(prop.getProperties("digsig.count"));
	    		
	    		if(count > 0) {
					
					mt = new MainThread(count);
					mt.start();
		        	
		        	startButton.setEnabled(false);
		    		stopButton.setEnabled(true);
		    		jTabbedPane1.setEnabled(false);
		    		
		    		//System.out.println("..................................... Start automatic processing mode");
					logger.info("Start automatic processing mode");
		    		
				} else {
					JOptionPane.showMessageDialog(null, "Please create record before start program", "Error", JOptionPane.ERROR_MESSAGE);
				}
	    		
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    		startButton.setEnabled(true);
	    		stopButton.setEnabled(false);
	    		jTabbedPane1.setEnabled(true);
	    	}
    	
    	}
    	
		
    }

    
    
    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE)) {
    		if(so.getServiceState().equalsIgnoreCase("Stopped")) {
				System.out.println("..................................... Service "+Constance.PROGRAM_NAME+" has stopped");
			}
    		else {
	    		so.stopWindowsService();
				System.out.println("..................................... Stop service "+Constance.PROGRAM_NAME);
    		}
    		
    	}
    	else {
	    	mt.terminate();
	    	mt.stop();
	    	
	    	startButton.setEnabled(true);
			stopButton.setEnabled(false);
			jTabbedPane1.setEnabled(true);
			
			System.out.println("..................................... Stop automatic processing mode");
			logger.info("Stop automatic processing mode");
		
    	}
		
    }
    
    
    
    private void passwordFieldKeyPressed(java.awt.event.KeyEvent evt) {
    	
    	int key = evt.getKeyCode();
    	if (key == KeyEvent.VK_ENTER) {
    		loginButtonActionPerformed(null);
    	}

    }
    
    
    
    
    public void AuthenticationProcess() {
		
    	if(TokenEnv.isSlotActive()) {
			
			Token.IsUserPinLock();
			String TokenException = Token.Authentication(passwordField.getText());
			
			if(TokenException == null) {
				
				ArrayList<X509Certificate> Cert = Token.getCertificate();
				if(Cert == null) {
					JOptionPane.showMessageDialog(null, "No Certificate In Token, Please Remove Token", "Error", JOptionPane.ERROR_MESSAGE);
					passwordField.setText("");
					Token.LogoutSession();
					
				}else{
					
					try {
					
						/*// Select cert
						int selectedIndex = 0;
						
						if(Cert.size() > 1) {
							ArrayList<String> certDN = new ArrayList<String>();
							String selectedDN;
							
							for (int i = 0; i < Cert.size(); i++) {
								certDN.add(Cert.get(i).getSubjectDN().getName());
							}
							
							selectedDN = (String) JOptionPane.showInputDialog(null, "Certificate DN :", "Select Certificate", JOptionPane.PLAIN_MESSAGE, null, certDN.toArray(), certDN.toArray()[0]);
							
							if(selectedDN != null) {
								
								for (int i = 0; i < Cert.size(); i++) {
									if(Cert.get(i).getSubjectDN().getName().equals(selectedDN)){
										selectedIndex = i;
									}
								}
							
							}
							else {
								throw new Exception("No certificate selected");
								
							}
							
						}*/
						
						
						// Select cert
						int selectedIndex = 0;
						String selectedDN = null;
						String certSerial = null;
						
						if(Cert.size() > 1) {
							ArrayList<String> certDN = new ArrayList<String>();
							
							for (int i = 0; i < Cert.size(); i++) {
								certDN.add(Cert.get(i).getSubjectDN().getName()+","+Cert.get(i).getSerialNumber().toString(16));
							}
							
							selectedDN = (String) JOptionPane.showInputDialog(null, "Certificate : ", "Select Certificate", JOptionPane.PLAIN_MESSAGE, null, certDN.toArray(), certDN.toArray()[0]);
							
							for (int i = 0; i < Cert.size(); i++) {
								if((Cert.get(i).getSubjectDN().getName()+","+Cert.get(i).getSerialNumber().toString(16)).equals(selectedDN)){
									selectedIndex = i;
									certSerial = Cert.get(i).getSerialNumber().toString(16);
								}
							}
						}
						else {
							selectedDN = Cert.get(0).getSubjectDN().toString();
							certSerial = Cert.get(0).getSerialNumber().toString(16);
						}
						
						
						if(selectedDN != null) {
						
							Cert.get(selectedIndex).checkValidity();										// Check certificate validity
							X509Cert x509Cert = new X509Cert(Cert.get(selectedIndex).getEncoded());
							x509Cert.VerifyCertificate();													// Verify certificate with chain
							boolean resultCheckCertWithLicense = x509Cert.checkCertWithLicense();			// Check certificate with license
							boolean resultCheckCertEU = x509Cert.isEnterpriseUserCert();					// Check certificate is EU
							
							
							if(resultCheckCertWithLicense && resultCheckCertEU){
								Login();
							}
							else{
								if(!resultCheckCertWithLicense) {
									JOptionPane.showMessageDialog(null, "Your corporate ID not match with license file", "Error", JOptionPane.ERROR_MESSAGE);
									passwordField.setText("");
									Token.LogoutSession();
								}
								else if(!resultCheckCertEU){
									JOptionPane.showMessageDialog(null, "Your certificate is not Enterprise user certificate", "Error", JOptionPane.ERROR_MESSAGE);
									passwordField.setText("");
									Token.LogoutSession();
								}
								
							}
						
						}
						else {
							passwordField.setText("");
							Token.LogoutSession();
						}
						
						/*if(!resultCertCheck) {
							//JOptionPane.showMessageDialog(null, "Your certificate DN does not allowed to login", "Error", JOptionPane.ERROR_MESSAGE);
							passwordField.setText("");
							
						}else
							Login();*/
					
					} catch (CertificateExpiredException e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						passwordField.setText("");
						Token.LogoutSession();
						
					} catch (CertificateNotYetValidException e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						passwordField.setText("");
						Token.LogoutSession();
						
					} catch (SignatureException e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, "Verify signature not pass", "Error", JOptionPane.ERROR_MESSAGE);
						passwordField.setText("");
						Token.LogoutSession();
						
					} catch (NullPointerException e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, "License not allow to use this certificate to login, Please check license or certificate (Test/Production)", "Error", JOptionPane.ERROR_MESSAGE);
						passwordField.setText("");
						Token.LogoutSession();
					
					} catch (Exception e){
						e.printStackTrace();
						if(e.getMessage().equalsIgnoreCase("No certificate selected")) {
							passwordField.setText("");
							Token.LogoutSession();
						}
						else {
							JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
							passwordField.setText("");
							Token.LogoutSession();
						}
						
					}
					
				}
			
			}else {
				
				if(TokenException.equals("CKR_PIN_LOCKED")){
					JOptionPane.showMessageDialog(null, "Etoken password is blocked", "Error", JOptionPane.ERROR_MESSAGE);  //pin lock
					passwordField.setText("");
					Token.LogoutSession();
				}
				else if(TokenException.equals("CKR_PIN_INCORRECT")){
					JOptionPane.showMessageDialog(null, "Authentication Failed", "Error", JOptionPane.ERROR_MESSAGE);  //wrong pin
					passwordField.setText("");
					Token.LogoutSession();
				}
				
			}
			
		}
		
		
	}
	
	
	
	
	public void OnInsertToken() {
		
		//System.out.println("Insert Token");
		
		int i=0;
		
		for(i=0;i<AvailableDll.size();i++) {
			
			if(i>0) {
				TokenEnv.DestroyModule();	
				TokenEnv.LoadDll(AvailableDll.get(i-1));
						
			}
			
			setSlotComboBox();
			
			if((Token != null)&&(Token.getManufacture()!=null)) {
		
				break;
			}
			
		}
		
		if(slots.length > 0) {
			passwordField.setEnabled(true);
			loginButton.setEnabled(true);
			
		}
		else {
			passwordField.setEnabled(false);
			loginButton.setEnabled(false);
		}
		
	}
	
	
	
	public void OnRemoveToken() {
		
		//System.out.println("Token Remove");
		
		if(slots.length == 0) {
			loginButton.setEnabled(false);
			passwordField.setEnabled(false);
		}
		
		Logout();
		Token = null;
		TokenEnv.ClearSlotActive();
		
		OnInsertToken();
		
	}
	
	
	
	public void setSlotComboBox() {
    	
		try {
			
	    	slots = TokenEnv.getSlotsList();
	    	loginSlotComboBox.removeAllItems();
	    	
	    	if(slots.length > 0) {
	    		
		    	for (int i = 0; i < slots.length; i++) {
		    		loginSlotComboBox.addItem(slots[i].getToken().getTokenInfo().getLabel());
				}
		    	
	    	}
	    	else {
	    		loginSlotComboBox.addItem("----- Please insert token -----");
	    	}
			
		} catch (TokenException e) {
			e.printStackTrace();
			loginSlotComboBox.removeAllItems();
		}
    	
    	
    }
	
	
	
	
	public void Login(){
		passwordField.setText("");
		Main.mf.switchCardLayout(Constance.SETTING_PANEL_CARD);
		
	}
	
	
	
	
	public void Logout(){

		if(Token != null) {
			Token.LogoutSession();
		}
		Main.mf.switchCardLayout(Constance.MAIN_PANEL_CARD);
		
	}
	
    
    

	// Variables declaration - do not modify
	private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton loginButton;
    private javax.swing.JComboBox loginSlotComboBox;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JButton startButton;
    private javax.swing.JButton stopButton;
    public static javax.swing.JTextPane textPane;
    // End of variables declaration
	
}
