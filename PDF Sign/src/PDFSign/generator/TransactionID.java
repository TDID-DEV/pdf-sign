package PDFSign.generator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import PDFSign.Main;
import PDFSign.datetime.DateTime;
import PDFSign.properties.PropertiesManagement;


public class TransactionID {
	
	
	public static synchronized String generate() {
		
		String tranID = null;
		
		try {
			
			ArrayList<String> corporateTax = Main.LicenseFile.getCorporateTax();
			tranID = corporateTax.get(0)+String.valueOf(new DateTime().getDateTime("yyMMddHHmmssSSS"));
			
			Thread.sleep(1);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return tranID;
		
	}
	

	public static synchronized String generate(String recordNumber) {
		
		PropertiesManagement prop = new PropertiesManagement();
		String tranID = null;
		
		try {
			
			String corporateTax = prop.getProperties("sg."+recordNumber+".corporateID");
			tranID = corporateTax+String.valueOf(new DateTime().getDateTime("yyMMddHHmmssSSS"));
			
			Thread.sleep(1);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return tranID;
		
	}
	
	
}
