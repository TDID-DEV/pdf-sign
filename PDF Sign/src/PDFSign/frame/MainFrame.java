package PDFSign.frame;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Logger;

import PDFSign.Constance;
import PDFSign.Main;
import PDFSign.datetime.DateTime;
import PDFSign.file.ExtensionFileFilter;
import PDFSign.file.FileOperation;
import PDFSign.frame.MainFrame;
import PDFSign.panel.AboutPanel;
import PDFSign.panel.MainPanel;
import PDFSign.panel.NullPanel;
import PDFSign.panel.SettingPanel;
import PDFSign.properties.PropertiesManagement;


public class MainFrame extends javax.swing.JFrame {
	
	final static Logger logger = Logger.getLogger(MainFrame.class);
	
	int LIMIT_LINE_IN_CONSOLE;
	int lineCount = 0;

	public MainFrame() {
		
        initComponents();
        getCharacterLimitInConsole();
        redirectSystemStreams();
        
        if(Main.checkLicenceFile) {
	        String licenseVersion = "";
	        if(Main.LicenseFile.IsLicenseTest()){
	        	licenseVersion = "(Test)";
	        }
	        this.setTitle(Constance.PROGRAM_NAME+" "+licenseVersion);	// Set title
        }
        else {
        	this.setTitle(Constance.PROGRAM_NAME+" "+"(No license)");	// Set title
        }
        
        ImageIcon img = new ImageIcon(Constance.IMAGES_FOLDER_PATH+"PDF Sign.png");	// Load icon
        this.setIconImage(img.getImage());		// Set icon
        
        // Set Frame to center of screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        
    }

    
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        displayPanel = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        importLicneseMenuItem = new javax.swing.JMenuItem();
        logoutMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();
        nullPanel = new NullPanel();
        
        displayPanel.setLayout(new java.awt.CardLayout());
        displayPanel.add(nullPanel, "card1");
        
        
        if(Main.checkLicenceFile && Main.licenseFileLoader){
	        mainPanel = new MainPanel();
	        settingPanel = new SettingPanel();
	        
	        displayPanel.add(mainPanel, "card2");
	        displayPanel.add(settingPanel, "card3");
        }

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);


        fileMenu.setText("File");

        importLicneseMenuItem.setText("Import license");
        importLicneseMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importLicneseMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(importLicneseMenuItem);

        logoutMenuItem.setText("Logout");
        logoutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(logoutMenuItem);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        jMenuBar1.add(fileMenu);

        helpMenu.setText("Help");

        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(displayPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(displayPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>

	
	
	private void getCharacterLimitInConsole() {
		
		try {
			
			PropertiesManagement prop = new PropertiesManagement();
			LIMIT_LINE_IN_CONSOLE = Integer.parseInt(prop.getProperties(Constance.LIMIT_CON_PATH, "limit.lineInConsole"));
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		
	}
    
    
	private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
    	JOptionPane.showOptionDialog(null, new AboutPanel(), "About", JOptionPane.DEFAULT_OPTION,JOptionPane.CLOSED_OPTION, null, new Object[]{}, null);
    }
    
    

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
    	System.exit(0);
    }
    
    
    
    private void logoutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
    	MainPanel.Token.LogoutSession();
    	Main.mf.switchCardLayout(Constance.MAIN_PANEL_CARD);
    }
    
    
    
    
    private void importLicneseMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
    	JFileChooser chooser = new JFileChooser();
		
		 String[] AllowExtension = {".lic", ".LIC"};
		
		chooser.setFileFilter(new ExtensionFileFilter("License File(*.lic)", AllowExtension));
		chooser.setAcceptAllFileFilterUsed(false);
		int returnVal = chooser.showOpenDialog(null);
		 if(returnVal == JFileChooser.APPROVE_OPTION) {
			 
			 String LicenseFilePath = chooser.getSelectedFile().getAbsolutePath();
			 
			 try {
				FileOperation.Operate(LicenseFilePath, System.getProperty("user.dir")+File.separator+"LicenseFile.lic", 3);
				JOptionPane.showMessageDialog(null, "Successful License Installation! Program will be restarted to take effect");
				restartApplication();
				
			} catch (IOException e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, "License installation failed", "Error", JOptionPane.ERROR_MESSAGE);
				
			}
			 
		 }
    }
    
    
    
    
    public void switchCardLayout(String cardName){
    	CardLayout cl = (CardLayout) MainFrame.displayPanel.getLayout();
    	cl.show(MainFrame.displayPanel, cardName);
    	
    	//Set frame size from PreferredSize of destination panel and logout menu bar
    	if(cardName.equalsIgnoreCase(Constance.NULL_PANEL_CARD)){
    		this.setSize(MainFrame.nullPanel.getPreferredSize());
    		logoutMenuItem.setEnabled(false);
    	}
    	else if(cardName.equalsIgnoreCase(Constance.MAIN_PANEL_CARD)){
    		this.setSize(MainFrame.mainPanel.getPreferredSize());
    		logoutMenuItem.setEnabled(false);
    	}
    	else if(cardName.equalsIgnoreCase(Constance.SETTING_PANEL_CARD)){
    		this.setSize(MainFrame.settingPanel.getPreferredSize());
    		logoutMenuItem.setEnabled(true);
    	}
    	
    	
    }
    
    
    
    public void restartApplication() throws IOException{
	  
		String curDir = System.getProperty("user.dir");
		
		File Dir = new File(curDir);
		
		String[] TotalFiles = Dir.list();
		
		for(int i=0;i<TotalFiles.length;i++) {
			
			String FileName = TotalFiles[i];
			
			if(FileName.toLowerCase().endsWith(".exe") && FileName.contains(Constance.PROGRAM_NAME)) {
				
				//Runtime.getRuntime().exec( curDir + "\\"+ FileName );
				Process process = new ProcessBuilder(curDir + "\\"+ FileName).start();
				System.exit(0);
			}
			
		}
	   
		JOptionPane.showMessageDialog(null, "Can't find Executable file, program will exit", "Error", JOptionPane.ERROR_MESSAGE);
		System.exit(0);
	   	  
	      
	}
    
    
    
    private void updateTextPane(final String text) {
    	
    	SwingUtilities.invokeLater(new Runnable() {
    		
    		public void run() {
    			
    			Document doc = MainPanel.textPane.getDocument();
    			String newline = System.getProperty("line.separator");
    	    	SimpleAttributeSet set = new SimpleAttributeSet();
    			StyleConstants.setFontSize(set, 12);
    			//StyleConstants.setBold(set, true);
    			//StyleConstants.setForeground(set, Color.BLUE);
    			
    			// Set font color
    			if(text.contains("[SUCCESSFUL]")) {
    				StyleConstants.setForeground(set, new Color(0,190,0));	//green color
    			}
    			else if(text.contains("[FAILED]")) {
    				StyleConstants.setForeground(set, Color.RED);
    			}
    			else {
    				// Default color (black)
    			}
    			
    			try {
    				
    				// Remove line when over limit
    				//if(doc.getText(0, doc.getLength()).split(newline).length > LIMIT_LINE_IN_CONSOLE) {
    				if(lineCount >= LIMIT_LINE_IN_CONSOLE) {
    					
    					// Remove line
    					int count = 1;
    					while((!doc.getText(0, count).contains(newline)) && (count < 1000)) {
    						count++;
    					}
    					doc.remove(0, count);
    					lineCount--;
    					
    				}
    				
    				if(text.contains(newline)) {
    					doc.insertString(doc.getLength(), text, set);
    					lineCount++;
    				}
    				else {
    					doc.insertString(doc.getLength(), "["+new DateTime().getDateTime()+"] "+text, set);
    				}
    				
    				
    			} catch (BadLocationException e) {
    				throw new RuntimeException(e);
    			}
    			
    			MainPanel.textPane.setCaretPosition(doc.getLength() - 1);
    			
    		}
    		
    	});
    	
    }
	
    
	private void redirectSystemStreams() {
		
		OutputStream out = new OutputStream() {
		    @Override
		    public void write(int b) throws IOException {
		    	updateTextPane(String.valueOf((char) b));
		    }
		 
		    @Override
		    public void write(byte[] b, int off, int len) throws IOException {
		    	updateTextPane(new String(b, off, len));
		    }
		 
		    @Override
		    public void write(byte[] b) throws IOException {
		      write(b, 0, b.length);
		    }
		    
		};
	 
	  System.setOut(new PrintStream(out, true));
	  //System.setErr(new PrintStream(out, true));
	  
	}
    
    
    

    
    // Variables declaration - do not modify
    private javax.swing.JMenuItem aboutMenuItem;
    private static javax.swing.JPanel displayPanel;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenuItem importLicneseMenuItem;
    private javax.swing.JMenuItem logoutMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuBar jMenuBar1;
    private static PDFSign.panel.NullPanel nullPanel;
    private static PDFSign.panel.MainPanel mainPanel;
    private static PDFSign.panel.SettingPanel settingPanel;
    // End of variables declaration
}
