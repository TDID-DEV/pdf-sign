package PDFSign.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.TreeSet;

import PDFSign.Constance;

public class PropertiesManagement {
	
	
	Properties prop = new Properties();
	
	
	public String getProperties(String key) throws FileNotFoundException, IOException{
		
		String value = "";		
		prop.load(new FileInputStream(Constance.PROPERTIES_PATH));
		
		value = prop.getProperty(key);
		
		return value;
		
	}
	
	
	
	public String getProperties(String propertiesPath, String key) throws FileNotFoundException, IOException{
		
		String value = "";		
		prop.load(new FileInputStream(propertiesPath));
		
		value = prop.getProperty(key);
		
		return value;
		
	}
	
	
	
	public void setProperties(String key, String value) throws FileNotFoundException, IOException{
		
		prop.load(new FileInputStream(Constance.PROPERTIES_PATH));
		OutputStream output = new FileOutputStream(Constance.PROPERTIES_PATH);
		prop.setProperty(key, value);
		//prop.store(output, null);
		putWithSort(prop, output);
		
	}

	
	
	public void setProperties(String propertiesPath, String key, String value) throws FileNotFoundException, IOException{
		
		prop.load(new FileInputStream(propertiesPath));
		OutputStream output = new FileOutputStream(propertiesPath);
		prop.setProperty(key, value);
		//prop.store(output, null);
		putWithSort(prop, output);
		
	}
	
	
	
	public void deleteProperties(String key) throws FileNotFoundException, IOException{
		
		prop.load(new FileInputStream(Constance.PROPERTIES_PATH));
		OutputStream output = new FileOutputStream(Constance.PROPERTIES_PATH);
		prop.remove(key);
		prop.store(output, null);
		
	}
	
	
	
	public void deleteProperties(String propertiesPath, String key) throws FileNotFoundException, IOException{
		
		prop.load(new FileInputStream(propertiesPath));
		OutputStream output = new FileOutputStream(propertiesPath);
		prop.remove(key);
		prop.store(output, null);
		
	}
	
	
	
	public void changePropertiesKey(String regex, String replacement) throws IOException {
		
		prop.load(new FileInputStream(Constance.PROPERTIES_PATH));
		OutputStream output = new FileOutputStream(Constance.PROPERTIES_PATH);
		
		for (final String key : prop.stringPropertyNames()) {
			if (key.startsWith(regex)) {
				String newKey = key.replaceFirst(regex, replacement);
				prop.setProperty(newKey, (String) prop.remove(key));
				
			}
			
		}
		
		prop.store(output, null);
		
	}
	
	
	private void putWithSort(Properties prop, OutputStream output) throws IOException {
		
		Properties tmp = new Properties() {
		    @Override
		    public synchronized Enumeration<Object> keys() {
		        return Collections.enumeration(new TreeSet<Object>(super.keySet()));
		    }
		};
		tmp.putAll(prop);
		tmp.store(output, null);
		
	}
	

}
