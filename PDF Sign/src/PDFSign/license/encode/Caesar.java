package PDFSign.license.encode;

public class Caesar {
	
	final int Start_ASC_Dec = 33;
	final int End_ASC_Dec = 126;
	final int Mod_Value = (End_ASC_Dec - Start_ASC_Dec)+1;  
	
	public String Apply(String text, Integer shift) 
	{ 
	    char[] chars = text.toCharArray(); 
	    
	    for (int i=0; i < chars.length; i++) 
	    { 
	    	  
	    	  char c = chars[i]; 
	          if (c >= Start_ASC_Dec && c <= End_ASC_Dec) 
	          { 
	        	  
	              // Change base (ASC Start = Start_ASC_Value)
	             int x = c - Start_ASC_Dec;
	        	  
	           
	             x = (x + shift) % 94; 
	             
	              if(x>=0)
	            	  chars[i] = (char) (x + Start_ASC_Dec);
	              else 
	            	  chars[i] = (char) (x + (End_ASC_Dec+1)); 
	          } 

	        
	        
	    } 
	    return new String(chars); 
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String PlainText = "abc";
		
		
		Caesar Caesar = new Caesar();
		//System.out.println(Caesar.Apply(PlainText,CaesarConst.ShiftIterator));
		//System.out.println(Caesar.Apply("PSUPbUggsnNvvl", -265));
		System.out.println(Caesar.Apply("ace", null));
		
		//System.out.println(93%94);
	
		
	}
	
	

}
