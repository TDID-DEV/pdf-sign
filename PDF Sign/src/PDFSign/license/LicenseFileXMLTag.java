package PDFSign.license;

public class LicenseFileXMLTag {
	
	
	public static final String XML_RootLicenseTag = "License_file";
	public static final String XML_TrustListTag = "Trust_Lists";
	public static final String XML_RootCATag = "Root_CA";
	public static final String XML_SubCATag = "Sub_CA";
	public static final String XML_DistinguishNameTag = "DN";
	public static final String XML_CertificateTag = "Cert";
	public static final String XML_CorporateTag = "CorporateTax";
	public static final String XML_SecureSignEnable = "SecureSignEnable";
	public static final String XML_SecureVerEnable = "SecureVerEnable";
	public static final String XML_SupportPKCS11DeviceTag = "PKCS11_Device";
	public static final String XML_SupportPKCS11DeviceDetailTag = "Device";
	public static final String XML_SupportProgramTag = "Program";
	public static final String XML_ProgramEditionTag = "ProgramEdition";
	public static final String XML_ProgramTypeTag = "ProgramType";
	

}
