package PDFSign.license;



import iaik.security.provider.IAIK;
import iaik.utils.Base64InputStream;
import iaik.utils.Base64OutputStream;
import iaik.utils.Util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;


import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import PDFSign.license.encode.Caesar;
import PDFSign.license.exception.LicenseFileException;
import PDFSign.xml.XMLFetchLicense;





public class LicenseFileReader {
	
	
	private KeyStore KeyStore;
	//private String CorporateTax;
	
	private ArrayList<String> CorporateTaxs;
	
	private boolean SecureSignEnable;
	private boolean SecureVerEnable;
	
	private ArrayList<String> SupportDevice;
	
	private String SupportProgram;
	private String ProgramEdition;
	private String ProgramType;
	
	final int CaesarShift = -50;
	
	public LicenseFileReader(String LicenseFilePath) throws LicenseFileException  {
		
		
		try {
			
			
			KeyStore =  java.security.KeyStore.getInstance(java.security.KeyStore.getDefaultType());
			
			KeyStore.load(null, null);
			CorporateTaxs = new ArrayList<String>();
			SupportDevice = new ArrayList<String>();
			
			
		
			 String EncodeText = readFile(LicenseFilePath);
			 String Base64Text = decodeCaesar(EncodeText, CaesarShift);
			 byte[] XML_Bytes = Util.Base64Decode(Base64Text.getBytes());
			 
			 System.out.println(new String(XML_Bytes));
		
			 readLicenseContent(XML_Bytes);
			 
			/* 
			FileInputStream xx = new FileInputStream(LicenseFilePath);
			byte[] bb = new byte[xx.available()];
			xx.read(bb);
			xx.close();
			readLicenseContent(bb);
			*/
	
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new LicenseFileException("License File exception");
		}
		
		
		
	}
	
	
	
	private String decodeCaesar(String EncodeText, int Shift) {
		
		
		Caesar Caesar = new Caesar();
		String Text = Caesar.Apply(EncodeText, Shift);
		
		return Text; 
	}
	
	
	
	
	private void readLicenseContent(byte[] XMLContentBytes) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, CertificateException, KeyStoreException {
		
		String DN = "", Base64Cert;
	
		XMLFetchLicense Fetcher = new XMLFetchLicense(new ByteArrayInputStream(XMLContentBytes));
		
		XPathExpression expr1 = Fetcher.CompileXPathExpression("/"+LicenseFileXMLTag.XML_RootLicenseTag);
		NodeList RootLicenseNode = Fetcher.Execute(expr1);	
		
		NodeList LicenseDetailNode = RootLicenseNode.item(0).getChildNodes();
		
		int LicenseDetail = LicenseDetailNode.getLength();
		
	
		for(int LicenseDetailCount=0;LicenseDetailCount<LicenseDetail;LicenseDetailCount++) {
			
			
			
			if(LicenseDetailCount%2 == 1) {
				
				
				
				String LicenseDetailTagName = LicenseDetailNode.item(LicenseDetailCount).getNodeName();
				
				if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_TrustListTag)) {
					
					NodeList TrustListsNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					int TotalCADetail = TrustListsNode.getLength();
					
					for(int CADetailCount=0;CADetailCount<TotalCADetail;CADetailCount++) {
						
						String CADetailTagName = TrustListsNode.item(CADetailCount).getNodeName();
						
						if(CADetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_RootCATag)) {
							
							NodeList RootCADetailNode = TrustListsNode.item(CADetailCount).getChildNodes();
							
							int TotalCertDetail = RootCADetailNode.getLength();
							
							for(int CertDetailCount=0;CertDetailCount<TotalCertDetail;CertDetailCount++) {
								
								String CertDetailTagName = RootCADetailNode.item(CertDetailCount).getNodeName();
								
								if(CertDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_DistinguishNameTag)) {
									
									NodeList DNNode = RootCADetailNode.item(CertDetailCount).getChildNodes();
									//System.out.println(DN.item(0).getNodeValue());
									DN = DNNode.item(0).getNodeValue().trim();
									
								}
								
								else if(CertDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_CertificateTag)) {
									
									NodeList CertNode = RootCADetailNode.item(CertDetailCount).getChildNodes();
									//System.out.println(DN.item(0).getNodeValue());
									Base64Cert  = CertNode.item(0).getNodeValue();
									//System.out.println(Base64Cert.trim());
									//X509Cert Cert = new X509Cert(Util.Base64Decode(Base64Cert.trim().getBytes()));
									//X509Cert Cert = new X509Cert(Util.Base64Decode(Base64Cert.trim().getBytes()));
									
									CertificateFactory CertFactory = CertificateFactory.getInstance("X509");
									X509Certificate Cert = (X509Certificate) CertFactory.generateCertificate(new ByteArrayInputStream(Util.Base64Decode(Base64Cert.trim().getBytes())));
									
									
									KeyStore.setCertificateEntry(DN, Cert);
								}
								
			
							}
							
		
						}
						
						
						else if(CADetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_SubCATag)) {
							
							NodeList SubCADetailNode = TrustListsNode.item(CADetailCount).getChildNodes();
							
							int TotalCertDetail = SubCADetailNode.getLength();
							
							for(int CertDetailCount=0;CertDetailCount<TotalCertDetail;CertDetailCount++) {
								
								String CertDetailTagName = SubCADetailNode.item(CertDetailCount).getNodeName();
								
								if(CertDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_DistinguishNameTag)) {
									
									NodeList DNNode = SubCADetailNode.item(CertDetailCount).getChildNodes();
									//System.out.println(DNNode.item(0).getNodeValue());
									DN = DNNode.item(0).getNodeValue().trim();
									
								}
								
								else if(CertDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_CertificateTag)) {
									
									NodeList CertNode = SubCADetailNode.item(CertDetailCount).getChildNodes();
									//System.out.println(DN.item(0).getNodeValue());
									Base64Cert  = CertNode.item(0).getNodeValue();
									//System.out.println(Base64Cert);
									//X509Cert Cert = new X509Cert(Util.Base64Decode(Base64Cert.trim().getBytes()));
									CertificateFactory CertFactory = CertificateFactory.getInstance("X509");
									X509Certificate Cert = (X509Certificate) CertFactory.generateCertificate(new ByteArrayInputStream(Util.Base64Decode(Base64Cert.trim().getBytes())));
									
									KeyStore.setCertificateEntry(DN, Cert);
									
									
								}
								
			
							}
							
							
						}
						
						
						
					}
						
					
					
					
				}
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_CorporateTag)) {
					
					NodeList CorporateTaxNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					//System.out.println(CorporateTaxNode.getLength());
					//String CorporateTax = CorporateTaxNode.item(0).getNodeValue().trim();
					
					
					
					//System.out.println("xx"+CorporateTaxNode.item(0).getNodeValue());
					
					for(int i=0;i<CorporateTaxNode.getLength();i++){
						//System.out.println(CorporateTaxNode.item(0).getNodeValue());
						String CorporateTax = CorporateTaxNode.item(0).getNodeValue().trim();
						System.out.println(CorporateTax);
						CorporateTaxs.add(CorporateTax);
						
					}
					
					//CorporateTaxs.add(CorporateTax);
					
				}
				
				
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_SupportPKCS11DeviceTag)) {
					
					
					
					NodeList PKCS11DeviceNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					int TotalDevice = PKCS11DeviceNode.getLength();
					
					for(int DeviceCount=0;DeviceCount<TotalDevice;DeviceCount++) {
						

						String DeviceTagName = PKCS11DeviceNode.item(DeviceCount).getNodeName();
						
						
						if(DeviceTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_SupportPKCS11DeviceDetailTag)) {
							
							NodeList DeviceDetailNode = PKCS11DeviceNode.item(DeviceCount).getChildNodes();
							//System.out.println(DNNode.item(0).getNodeValue());
							String Device = DeviceDetailNode.item(0).getNodeValue().trim();
							//System.out.println(Device);
							//SupportDevice.add(Device.toLowerCase());
							SupportDevice.add(Device);
							
						}

					}
					
				}
				
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_SupportProgramTag)) {
					
					NodeList SupportProgramNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					SupportProgram = SupportProgramNode.item(0).getNodeValue().trim();
					
				}
				
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_ProgramEditionTag)) {
					
					NodeList ProgramEditionNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					ProgramEdition = ProgramEditionNode.item(0).getNodeValue().trim();
					
				}
				
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_ProgramTypeTag)){
					
					NodeList ProgramTypeNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					ProgramType = ProgramTypeNode.item(0).getNodeValue().trim();
					
				}
				
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_SecureSignEnable)) {
					
					NodeList SecureSignNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					SecureSignEnable = Boolean.parseBoolean(SecureSignNode.item(0).getNodeValue().trim());
					//System.out.println(CorporateTaxNode.item(0).getNodeValue());
					
				}
				
				
				else if(LicenseDetailTagName.equalsIgnoreCase(LicenseFileXMLTag.XML_SecureVerEnable)) {
					
					NodeList SecureVerNode = LicenseDetailNode.item(LicenseDetailCount).getChildNodes();
					
					SecureVerEnable = Boolean.parseBoolean(SecureVerNode.item(0).getNodeValue().trim());
					//System.out.println(CorporateTaxNode.item(0).getNodeValue());
					
				}
				
			
			}
		}
		
			
		
		
	}
	
		
	
	
	
	public KeyStore getKeyStore() {
		
		
		return this.KeyStore;
		
	}
	
	
	
	
	public boolean IsLicenseTest() {
		
		final String License_TestPharsing = "test";
		
		
		try {
			
			Enumeration<String> KeyAlias = KeyStore.aliases();
			
	
			while(KeyAlias.hasMoreElements()) {
				
				String Cert_Alias = KeyAlias.nextElement();
				
				if(Cert_Alias.contains(License_TestPharsing)) {
			
					return true;
					
				}
				
				
			}
			
			
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	public String IsSupportProgram() {
		
		return this.SupportProgram;
	}
	
	
	public String getProgramEdition() {
		
		return this.ProgramEdition;
	}
	
	
	public String getProgramType() {
		
		return this.ProgramType;
		
	}
	
	
	public ArrayList<String> getCorporateTax() {
		
		return CorporateTaxs;
		
	}
	
	
	public boolean IsSecureSignEnable() {
		
		return this.SecureSignEnable;
	}
	
	
	public boolean IsSecureVerEnable() {
		
		return this.SecureVerEnable;
	}
	
	
	public ArrayList<String> getSupportPKCS11Device() {
		
		return this.SupportDevice;
	}
	
	
	
	public boolean IsCorporateTaxInLicenseFile(String CorporateTax) {
		
		//System.out.println("Is corporate tax in license file : "+CorporateTaxs.contains(CorporateTax));
		
		return CorporateTaxs.contains(CorporateTax);
		
	}
	
	
		
	 public static void main(String[] args) {
		 
		
		 
		
			 
			/*
			try {
				LicenseFileReader xx = new LicenseFileReader("C:\\Users\\admin_user\\Desktop\\LicenseFile_yah.lic");
				//LicenseFileReader xx = new LicenseFileReader("C:\\Users\\admin_user\\Desktop\\Test_XML.xml");
				//System.out.println(xx.getCorporateTax());
				//System.out.println(xx.IsCorporateTaxInLicenseFile("41010303002"));
				//Enumeration<String> xxx = xx.getKeyStore().aliases();
				//while(xxx.hasMoreElements())
					//System.out.println(xxx.nextElement());
			} catch (LicenseFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			
		
		 
		 
		
	///////////// Gen license	 
		 
		 try {
			 
			 
			String xx = readFile("C:\\Users\\admin_user\\Desktop\\Test_XML.xml");
			System.out.println(xx);
			byte[] bytes = (Util.Base64Encode(xx.getBytes()));
			
			String xxx = new String(bytes);
			
			
			 Caesar Caesar = new Caesar();
			 String after = Caesar.Apply(xxx, 50);
			 
			FileOutputStream fos=new FileOutputStream("C:\\Users\\admin_user\\Desktop\\LicenseFile.lic");
			fos.write(after.getBytes());
			
			 

			 
			 
			 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		 
		 
	 }
	 
	 

	
		 
	 private static String readFile(String filename) throws IOException {
		   String lineSep = System.getProperty("line.separator");
		   BufferedReader br = new BufferedReader(new FileReader(filename));
		   String nextLine = "";
		   StringBuffer sb = new StringBuffer();
		   while ((nextLine = br.readLine()) != null) {
		     sb.append(nextLine);
		     //
		     // note:
		     //   BufferedReader strips the EOL character
		     //   so we add a new one!
		     //
		     sb.append(lineSep);
		   }
		   
		   return sb.toString();
		}


	

}
