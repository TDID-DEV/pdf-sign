package PDFSign.xml;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLFetchLicense {
	
	
	private Document doc;
	//private XPathExpression expr;
	private XPath xpath;
	
	public XMLFetchLicense(String XMLFilePath) throws ParserConfigurationException, SAXException, IOException {
		
		
		 DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();	
		 DocumentBuilder builder = domFactory.newDocumentBuilder();
		 doc = builder.parse(XMLFilePath);
		 
		 
		 XPathFactory factory = XPathFactory.newInstance();	
		 xpath = factory.newXPath();
		
		
	}
	
	
	
	
	public XMLFetchLicense(InputStream is) throws ParserConfigurationException, SAXException, IOException {
		
		
		 DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();	
		 DocumentBuilder builder = domFactory.newDocumentBuilder();
		 doc = builder.parse(is);
		 
		 
		 XPathFactory factory = XPathFactory.newInstance();	
		 xpath = factory.newXPath();
		
		
	}
	
	
	public XPathExpression CompileXPathExpression(String Expression) throws XPathExpressionException {
		
		
		
		return  xpath.compile(Expression);
		
	}
	
	
	public NodeList Execute(XPathExpression expr) throws XPathExpressionException {
		
		Object result = expr.evaluate(doc, XPathConstants.NODESET);
		
		NodeList nodes = (NodeList) result;
		
		return nodes;
	}

}
