package PDFSign.convert;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

public class Convert {
	
	public String hexToASCII(String hex){
		
		if(hex.length()%2 != 0){
			System.err.println("requires EVEN number of chars");
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for( int i=0; i < hex.length()-1; i+=2 ){
			String output = hex.substring(i, (i + 2));
			int decimal = Integer.parseInt(output, 16);
			sb.append((char)decimal);
		}
		return sb.toString();
		 
	}
	
	
	public String asciiToHex(String ascii) {
		
		char[] chars = ascii.toCharArray();
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++){
			hex.append(Integer.toHexString((int) chars[i]));
		}
		return hex.toString();
	
	}
	
	public byte[] hexToByteArray(String s) {
		
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
		//return new BigInteger(s,16).toByteArray();
	    
	}
	
	
	public String byteArrayToHex(byte[] bytes) {
		
		char[] hexArray = "0123456789ABCDEF".toCharArray();
	    char[] hexChars = new char[bytes.length * 2];
	    
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	    
	}
	
	
	public String hexToDec(String hex){
		
		int intDec = Integer.parseInt(hex, 16);
		String dec = String.valueOf(intDec);
		return dec;
		
	}
	
	
	public String decToHex(String dec){
		
		int intDec = Integer.parseInt(dec);
		String hex = Integer.toHexString(intDec);
		return hex;
		
	}
	
	
	public String bigIntToHex(String bigInt){
		
		String hex = new BigInteger(bigInt).toString(16);
		return hex;
		
	}
	
	
	public BigInteger hexToBigInt(String hex){
		
		BigInteger bigInt = new BigInteger(hex, 16);
		return bigInt;
		
	}
	
	
	
	public String arrayListToString(ArrayList<String> list){
		
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			result.append(list.get(i).toString().trim());
		}
		
		return result.toString();
		
		
	}
	
	
	
	public ArrayList<String> bytesStringToArrayList(String str){
		
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < str.length(); i=i+2) {
			list.add(str.substring(i, i+2));
		}
		
		return list;
		
	}
	
	
	
	public String[] arrayListToArray(ArrayList<String> list){
		
		return list.toArray(new String[list.size()]);
		
	}
	
	
	
	public String aliasToHex(String datetime){
		
		//Convert alias to HEX
		Convert convert = new Convert();
		String result = "";
		result = "0h" + convert.asciiToHex(datetime);			//Prefix "0h" for writing with HEX
		
		return result;
		
	}
	
	
	public byte[] fileToByteArray(String filePath) {
		
		FileInputStream fileInputStream=null;

        File file = new File(filePath);

        byte[] bFile = new byte[(int) file.length()];

        try {
            //convert file into array of bytes
		    fileInputStream = new FileInputStream(file);
		    fileInputStream.read(bFile);
		    fileInputStream.close();
		    
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        return bFile;
		
	}
	
	
	

}
