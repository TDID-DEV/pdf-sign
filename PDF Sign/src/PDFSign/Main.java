package PDFSign;

import java.util.ArrayList;

import com.itextpdf.text.Font;

import PDFSign.Constance;
import PDFSign.license.LicenseFileReader;
import PDFSign.startup.Startup;
import PDFSign.token.TokenProvider;


public class Main {
		
	public static PDFSign.frame.MainFrame mf;
	public static LicenseFileReader LicenseFile;
	
	public static boolean checkLicenceFile = false;
	public static boolean licenseFileLoader = false;
	
	public static Font font;
	
	public static ArrayList<TokenProvider> tokenProviderList = new ArrayList<TokenProvider>();
	
	public static void main(String[] args) {
		
		Startup.checkAppLock();
		Startup.setupLookAndFeels();
		//Startup.initialLog();
		Startup.loadFont();
		checkLicenceFile = Startup.checkLicenceFile();
		licenseFileLoader = Startup.licenseFileLoader();
		
		mf = new PDFSign.frame.MainFrame();
		mf.setVisible(true);
		
		if(checkLicenceFile && licenseFileLoader){
			
			if(LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE)) {
				// Do not initial log
			}
			else {
				Startup.initialLog();
			}
			
			mf.switchCardLayout(Constance.MAIN_PANEL_CARD);				//Set to login panel card
			
		}
		else{
			mf.switchCardLayout(Constance.NULL_PANEL_CARD);				//Set to null panel card
		}

	}

}
