package PDFSign.windowsService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import PDFSign.Constance;
import PDFSign.properties.PropertiesManagement;
import PDFSign.startup.Startup;
import PDFSign.thread.MainThread;
import PDFSign.thread.ReadLogFileThread;


public class ServiceOperation {

	final static Logger logger = Logger.getLogger(ServiceOperation.class);
	MainThread mt = null;
	
	final String STATE_RUNNING = "Running";
	
	
	public void init() {
		
		//Startup.checkAppLock();
		Startup.initialLog();
		Startup.checkLicenceFile();
		Startup.licenseFileLoader();
		Startup.loadFont();
		
	}
	
	
	
	
	public void start() {
		
		PropertiesManagement prop = new PropertiesManagement();
    	
    	try {
    		
    		int count = Integer.parseInt(prop.getProperties("digsig.count"));
    		
    		if(count > 0) {
				
    			// Start MainThread
				mt = new MainThread(count);
				mt.start();
	    		
				logger.info("Start automatic processing mode");
	    		
			} else {
				logger.error("Please create record before start program");
				stopWindowsService();
			}
    		
    	} catch (IOException e) {
    		e.printStackTrace();
    		stopWindowsService();
    	}
        		
	}
	
	
	
	
	
	public void stop() {
		
		try {
		
			if(mt != null) {
				mt.terminate();
		    	mt.stop();
			}
			
		} finally {
			logger.info("Stop automatic processing mode");
			
			System.exit(0);
		}
			
	}
	
	
	
	public void startWindowsService() {
		
		Runtime rt = Runtime.getRuntime();
		
		try {
			rt.exec("net start \""+Constance.PROGRAM_NAME+"\"");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		
	}
	
	
	
	public void stopWindowsService() {
		
		Runtime rt = Runtime.getRuntime();
		
		try {
			rt.exec("net stop \""+Constance.PROGRAM_NAME+"\"");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		
	}
	
	
	
	public boolean isServiceStart() {
		
		boolean result = false;
		
		try {
		
			if(getServiceState().equalsIgnoreCase(STATE_RUNNING)) {
				result = true;
			}
			else {
				result = false;
			}
		
		} catch(NullPointerException e) {
			e.printStackTrace();
			logger.error("Not found "+Constance.PROGRAM_NAME+" on windows services", e);
			JOptionPane.showMessageDialog(null, "Not found "+Constance.PROGRAM_NAME+" on windows services");
		}
		
		return result;
		
	}
	
	
	
	public String getServiceState() {
		
		String state = null;
		
		try {
			Process p = Runtime.getRuntime().exec("sc query \""+Constance.PROGRAM_NAME+"\"");
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			
			while(line!=null) {
				
				if(line.trim().startsWith("STATE")) {
					
					if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("1"))
				        //System.out.println("Stopped");
						state = "Stopped";
				    else if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("2"))
				    	//System.out.println("Starting....");
				    	state = "Starting....";
				    else if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("3"))
				    	//System.out.println("Stopping....");
				    	state = "Stopping....";
				    else if (line.trim().substring(line.trim().indexOf(":")+1,line.trim().indexOf(":")+4).trim().equals("4"))
				    	//System.out.println("Running");
				    	state = "Running";
					
				}
				
				line=reader.readLine(); 
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return state;
		
	}
	
	
}
