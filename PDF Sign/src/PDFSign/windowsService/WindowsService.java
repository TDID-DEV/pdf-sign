package PDFSign.windowsService;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.EventLog;
import org.boris.winrun4j.ServiceException;

import PDFSign.Constance;
import PDFSign.Main;

public class WindowsService extends AbstractService {

	final static Logger logger = Logger.getLogger(WindowsService.class);

	@Override
	public int serviceMain(String[] arg0) throws ServiceException {
		
		ServiceOperation so = new ServiceOperation();
		
		so.init();
		
		if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE)) {
			
			so.start();
			
			while(!this.shutdown){
				
				//***** Looping when service start *****//
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
			
			so.stop();
		
		}
		else if(Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_STANDARD)) {
			
			//JOptionPane.showMessageDialog(null, "License is not an enterprise edition", "Error", JOptionPane.ERROR_MESSAGE);
			//EventLog.report("Signing Gateway", EventLog.ERROR, "License is not an enterprise edition");
			logger.info("License is not an enterprise edition");
			so.stop();
			
		}
		
		
		return 0;
		
	}

	
	
	
	
}
