package PDFSign.encode;

import java.util.Base64;

public class Encoder {
	
	public String base64Encode(byte[] bytes) {
		
        return Base64.getEncoder().encodeToString(bytes);
        
    }

}
