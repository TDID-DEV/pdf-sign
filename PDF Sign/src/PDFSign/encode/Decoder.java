package PDFSign.encode;

import java.io.IOException;
import java.util.Base64;

public class Decoder {

	public byte[] base64Decode(String property) throws IOException {
		
        return Base64.getDecoder().decode(property);
        
    }
	
}
