package PDFSign.exception;

public class DigSigException extends Exception {

	public DigSigException(String message) {
		super(message);
	}
	
}
