package PDFSign.key;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import PDFSign.Constance;

public class SecretKey {

	public SecretKeySpec createSecretKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        PBEKeySpec keySpec = new PBEKeySpec(Constance.KEY_PASSWORD.toCharArray(), Constance.SALT, Constance.ITERATION_COUNT, Constance.KEY_LENGTH);
        javax.crypto.SecretKey keyTmp = keyFactory.generateSecret(keySpec);
        
        return new SecretKeySpec(keyTmp.getEncoded(), "AES");
        
    }
	
}
