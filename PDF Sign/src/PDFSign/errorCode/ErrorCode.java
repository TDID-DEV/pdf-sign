package PDFSign.errorCode;

public class ErrorCode {

	public static String ERROR_301 = "301";
	public static String ERROR_302 = "302";
	public static String ERROR_303 = "303";
	public static String ERROR_304 = "304";
	public static String ERROR_305 = "305";
	public static String ERROR_306 = "306";
	public static String ERROR_307 = "307";
	public static String ERROR_308 = "308";
	public static String ERROR_309 = "309";
	public static String ERROR_310 = "310";
	public static String ERROR_311 = "311";
	public static String ERROR_312 = "312";
	public static String ERROR_313 = "313";
	public static String ERROR_314 = "314";
	public static String ERROR_315 = "315";
	public static String ERROR_316 = "316";
	public static String ERROR_317 = "317";
	public static String ERROR_318 = "318";
	public static String ERROR_319 = "319";
	
}
