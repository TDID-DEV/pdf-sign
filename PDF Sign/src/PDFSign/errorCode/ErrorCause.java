package PDFSign.errorCode;

public class ErrorCause {

	public static String ERROR_301 = "The program is unable to sign";
	public static String ERROR_302 = "Certificate has expired";
	public static String ERROR_303 = "Certificate is not yet valid";
	public static String ERROR_304 = "Enterprise Certificate has not been found in Enterprise HSM/Token";
	public static String ERROR_305 = "The program is unable to connect to HSM/Token";
	public static String ERROR_306 = "The program is unable to sign due to the input file has been encrypted";
	public static String ERROR_307 = "Your extension file has not been supported";
	public static String ERROR_308 = "This client IP Address (clientIP) is not allowed to access";
	public static String ERROR_309 = "Not found this reference ID (refID)";
	public static String ERROR_310 = "The program is unable to sign due to the license is not support this certificate";
	public static String ERROR_311 = "File name is incorrect";
	public static String ERROR_312 = "Corporate ID does not match with license file";
	public static String ERROR_313 = "Transaction ID does not match";
	public static String ERROR_314 = "The program unable to connect Signing Server";
	public static String ERROR_315 = "Signing Server is unable to connect database";
	public static String ERROR_316 = "Not found this online record number (onlineRecordNumber)";
	public static String ERROR_317 = "The process cannot access the file because it is being used by another process";
	public static String ERROR_318 = "The folder is missing (folderPath)";
	public static String ERROR_319 = "The program is unable to connect Timestamp Server";
	
}
