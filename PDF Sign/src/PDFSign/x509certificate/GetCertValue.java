package PDFSign.x509certificate;


public class GetCertValue {
	
	
	
	public String getCN(String DnPattern) {
		int start,end;
		start = DnPattern.indexOf("cn=");
		
		if(start<0) {
			start = DnPattern.indexOf("CN=");
			
			if(start<0)
				return null;
			
		}
		
		end = DnPattern.indexOf(",",start);
		return DnPattern.substring(start+3,end).trim();
		
	}
	
	
	public String getO(String DnPattern){
		int start,end;
		start = DnPattern.indexOf("o=");
		
		if(start<0) {
			start = DnPattern.indexOf("O=");
			
			if(start<0)
				return null;
			
		}
		
		end = DnPattern.indexOf(",",start);
		return DnPattern.substring(start+2,end).trim();
		
	}
	
	
	public String getOU(String DnPattern){
		int start,end;
		start = DnPattern.indexOf("ou=");
		
		if(start<0) {
			start = DnPattern.indexOf("OU=");
			
			if(start<0)
				return null;
			
		}
		
		end = DnPattern.indexOf(",",start);
		return DnPattern.substring(start+3,end).trim();
		
	}
	
	
	public String getC(String DnPattern){
		int start,end;
		start = DnPattern.indexOf("c=");
		
		if(start<0) {
			start = DnPattern.indexOf("C=");
			
			if(start<0)
				return null;
			
		}
		
		return DnPattern.substring(start+2,start+4).trim();
		
	}
	

}
