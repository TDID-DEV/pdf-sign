package PDFSign.x509certificate;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class GetX509Certificate {
	
	public X509Certificate getFromFile(String filePath) throws CertificateException, IOException {
		
		X509Certificate cert = null;
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePath));
		while (in.available() > 0) {
		    cert = (X509Certificate) cf.generateCertificate(in);
		}
		in.close();
		
		return cert;
		
	}

}
