package PDFSign.x509certificate;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;


public class GetCertificate {

	public Certificate getFromFile(String filePath) throws CertificateException, IOException {
		
		Certificate cert = null;
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePath));
		while (in.available() > 0) {
		    cert = cf.generateCertificate(in);
		}
		in.close();
		
		return cert;
		
	}
	
	
}
