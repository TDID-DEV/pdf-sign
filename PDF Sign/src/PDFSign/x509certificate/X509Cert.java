package PDFSign.x509certificate;



import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;

import PDFSign.Main;
import iaik.asn1.CodingException;
import iaik.asn1.ObjectID;
import iaik.asn1.structures.DistributionPoint;
import iaik.asn1.structures.Name;
import iaik.asn1.structures.PolicyInformation;
import iaik.x509.V3Extension;
import iaik.x509.X509Certificate;
import iaik.x509.X509ExtensionException;
import iaik.x509.X509ExtensionInitException;
import iaik.x509.extensions.CRLDistributionPoints;
import iaik.x509.extensions.CertificatePolicies;

public class X509Cert extends X509Certificate {
	
	
	
	public X509Cert(byte[] array) throws CertificateException {
		
		super(array);
		
	}
	
	
	public X509Cert(InputStream inStream) throws CertificateException, IOException  {
		
		super(inStream);
		
		   		   
	}
	
	
	public String[] getCRLDistributionPoint() throws X509ExtensionInitException {
		
		ArrayList<String> distributionpointStr = new ArrayList<String>();
		
		// get CRLDistributionPoints extension
		CRLDistributionPoints cRLDistributionPoints = (CRLDistributionPoints) super.getExtension(CRLDistributionPoints.oid);
		
		if(cRLDistributionPoints!=null) {
		
			Enumeration e = cRLDistributionPoints.getDistributionPoints();
			
			while (e.hasMoreElements()) {
				   
				DistributionPoint dp = (DistributionPoint)e.nextElement();
				//System.out.println(dp.getDistributionPointName().toString());
				 
				distributionpointStr.add(dp.getDistributionPointName().toString());
				   	   
				   
			}   
		}
		int distributionpointAmount = distributionpointStr.size();
		
		if(distributionpointAmount>0) {
			
			String[] distributionPoint = new String[distributionpointAmount];
			distributionpointStr.toArray(distributionPoint);
			distributionpointStr = null;
			
			return distributionPoint;
			
		}
		else
			return null;
		
	}
	
	
	
	public PolicyInformation[] getPolicyInfomation() {
		
		final String Cert_Policy_OID = "2.5.29.32";
		
	
		try {
			
			V3Extension V3Extension = this.getExtension(new ObjectID(Cert_Policy_OID));
			
			CertificatePolicies CertPolicy = (CertificatePolicies) V3Extension;
			PolicyInformation[] inf = CertPolicy.getPolicyInformation();
			
			return inf;
			
		} catch (X509ExtensionInitException e) {
			//e.printStackTrace();
			return null;
		}
		
		
		
	}
	
	
	public String get_Email() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.emailAddress);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public String get_Givenname() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.givenName);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public String get_Surname() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.surName);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	public String get_Commonname() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.commonName);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public String get_OrganizationUnit() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.organizationalUnit);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}


	public String get_Organization() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.organization);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public String get_OrganizationIdentifier() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(new ObjectID("2.5.4.97"));
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public String get_Locality() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.locality);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public String get_State() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.stateOrProvince);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	
	
	public String get_Country() {
		
		try {
			
			Name name = new Name(this.getSubjectX500Principal().getEncoded());
			return name.getRDN(ObjectID.country);
			
		} catch (CodingException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	
	/*public String get_CorporateID() {
		
		final String corporateID_OID = "2.16.764.1.1.3.2.2";	// CorporateID OID
		
		try {
			
			V3Extension v3Extension = super.getExtension(new ObjectID(corporateID_OID));
			return v3Extension.toASN1Object().getValue().toString();
			
		} catch (X509ExtensionInitException e) {
			e.printStackTrace();
		} catch (X509ExtensionException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}*/
	
	
	
	
	public boolean checkCertWithLicense() {
		
		boolean result = false;
		
		String corporateID = get_OrganizationIdentifier();
		result = Main.LicenseFile.IsCorporateTaxInLicenseFile(corporateID);
		
		return result;
		
	}
	
	
	
	
	public boolean isEnterpriseUserCert() {
		
		boolean result = false;

		final String enterpriseUserCert_OID = "2.16.764.1.1.2.1.10002.1";	// enterprise user cert OID
		
		result = getPolicyInfomation()[0].toString().contains(enterpriseUserCert_OID);
		
		return result;
		
	}
	
	
	
	
	public void VerifyCertificate() throws KeyStoreException, CertificateEncodingException, CertificateException, InvalidKeyException, SignatureException, NoSuchProviderException, NoSuchAlgorithmException {
		
		 KeyStore KeyStore = Main.LicenseFile.getKeyStore();
		 
		
		 X509Cert AuthorityCert = this;
		 X509Cert VerifyCert = this;

		 do {

			 VerifyCert = AuthorityCert;
			 
			 VerifyCert.checkValidity();
			 
			 String SubjectDN = VerifyCert.getSubjectDN().getName().toLowerCase();
			 String IssuerDN = VerifyCert.getIssuerDN().getName().toLowerCase();
			 	 
			 //System.out.println("Verify Cert DN: "+SubjectDN);
			 //System.out.println("Authority Cert DN: "+IssuerDN);
			 
			 Certificate CertFromKeyStore = KeyStore.getCertificate(IssuerDN);
			 AuthorityCert = new X509Cert(CertFromKeyStore.getEncoded());
			 
			 
			 if((AuthorityCert.getSubjectDN().getName()).equals(VerifyCert.getIssuerDN().getName())) {
			 
				 VerifyCert.verify(AuthorityCert.getPublicKey());	
			 }
			 
			
			 
		 }
		 while(!VerifyCert.getSubjectDN().getName().equals(VerifyCert.getIssuerDN().getName()));
		
	}
	
	
	

}
