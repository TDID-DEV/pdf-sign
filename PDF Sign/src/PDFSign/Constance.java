package PDFSign;

public class Constance {
	
	public static final String PROGRAM_NAME = "PDF Sign";
	public static final String PROGRAM_VERSION = "4.6";
	
	public static final String PROGRAM_EDITION_ENTERPRISE = "Enterprise";
	public static final String PROGRAM_EDITION_STANDARD = "Standard";
	public static final String PROGRAM_TYPE_PLUS = "Plus";
	public static final String PROGRAM_TYPE_BASIC = "Basic";
	
	public static final String NULL_PANEL_CARD = "card1";
	public static final String MAIN_PANEL_CARD = "card2";
	public static final String SETTING_PANEL_CARD = "card3";
	
	public static final String LICENSE_FILE_PATH = "./LicenseFile.lic";
	public static final String LOG_CONFIGURATION_PATH = "./logConfiguration.con";
	public static final String PROPERTIES_PATH = "./prop.properties";
	public static final String PROGRAM_CON_PATH = "./program.con";
	public static final String CONNECTION_CON_PATH = "./connection.con";
	public static final String LIMIT_CON_PATH = "./limit.con";
	public static final String PROVIDER_CON_PATH = "./provider.con";
	public static final String CERTIFICATE_FOLDER_PATH = "./certificates/";
	public static final String SSL_FOLDER_PATH = "./SSL/";
	public static final String SIGNATURE_IMG_FOLDER_PATH = "./signature images/";
	public static final String IMAGES_FOLDER_PATH = "./images/";
	public static final String TMP_FOLDER_PATH = "./tmp/";
	public static final String LOG_FOLDER_PATH = "./logs/";
	
	public static final String MESSAGE_CODE_SIGN = "000";
	public static final String MESSAGE_CODE_CHECK = "099";
	
	public static final String DIGEST_ALG = "SHA512";
	public static final String SIG_ALG = "RSA";
	
	public static final int MAX_FIND_OBJECT = 999;
	
	// Secret Key
	public static final String KEY_PASSWORD = "password";
	public static final int ITERATION_COUNT = 40000;
	public static final int KEY_LENGTH = 128;
	public static final byte[] SALT = new String("12345678").getBytes();

}
