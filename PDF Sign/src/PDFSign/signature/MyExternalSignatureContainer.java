package PDFSign.signature;

import java.io.InputStream;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.security.ExternalSignatureContainer;

public class MyExternalSignatureContainer implements ExternalSignatureContainer {

protected byte[] sig;
	
    public MyExternalSignatureContainer(byte[] sig) {
        this.sig = sig;
    }
    
    public byte[] sign(InputStream is) {
        return sig;
    }
    
    public void modifySigningDictionary(PdfDictionary signDic) {
    	
    }
	
}
