package PDFSign.signature;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;

import PDFSign.Constance;
import PDFSign.Main;


public class PDFSignature {
	
	private final String FIELD_NAME = "Signature";
	
	PrivateKey privateKey;
	private Certificate[] chain;
	private PdfSignatureAppearance appearance;
	private String filePassword;
	//private InputStream rangeStream;
	
	public PDFSignature(PrivateKey privateKey, Certificate[] chain, String filePassword) {
		
		this.privateKey = privateKey;
		this.chain = chain;
		this.filePassword = filePassword;
		
	}
	
	
	
	public byte[] setAppearanceAndSign(byte[] src, String llx, String lly, String urx, String ury, String fontSize, String page, String reason, String location, String signatureImg, String withTSA, String tsaURL, String tsaUser, String tsaPassword) throws IOException, DocumentException, GeneralSecurityException {
		
		// Init font
    	//Font font = new Font(BaseFont.createFont(BaseFont.COURIER, BaseFont.CP1252, BaseFont.EMBEDDED));
    	//Font font = new Font(BaseFont.createFont("./fonts/THSarabunNew.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
		Font font = Main.font;
        font.setSize(Integer.parseInt(fontSize));
		
        
        // Reader
		PdfReader reader = new PdfReader(src, filePassword.getBytes());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfStamper stamper = PdfStamper.createSignature(reader, bos, '\0');
		
		if(filePassword.equalsIgnoreCase("") || filePassword==null || filePassword.length()==0) {
			// doesn't set password to file
		}
		else {
			stamper.setEncryption(filePassword.getBytes(), filePassword.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128 | PdfWriter.DO_NOT_ENCRYPT_METADATA);
		}
			
		/*File tmp = File.createTempFile("TMP-", ".tmp", new File(Constance.TMP_FOLDER_PATH));
		PdfStamper stamper = PdfStamper.createSignature(reader, new FileOutputStream(tmp), '\0');*/
		
		// get page for stamp signature
		int integerPage = 1;
       	if(page.equalsIgnoreCase("first")){
       		integerPage = 1;
       	}
       	else if(page.equalsIgnoreCase("last")){
       		integerPage = reader.getNumberOfPages();
       	}
       	else{
       		integerPage = Integer.parseInt(page);
       	}
		
       	appearance = stamper.getSignatureAppearance();
	    appearance.setLayer2Font(font);
	    appearance.setVisibleSignature(new Rectangle(Integer.parseInt(llx), Integer.parseInt(lly), Integer.parseInt(urx), Integer.parseInt(ury)), integerPage, FIELD_NAME);
	    appearance.setCertificate(chain[0]);
	    //appearance.setCertificationLevel(1);
	    
	    if(Integer.parseInt(fontSize) == 0) {
	    	appearance.setLayer2Text("");
	    }
	    
	    if(!location.equalsIgnoreCase(""))
    		appearance.setLocation(location);
    	
    	if(!reason.equalsIgnoreCase(""))
    		appearance.setReason(reason);
        
    	if(!signatureImg.equalsIgnoreCase(""))
    		appearance.setImage(Image.getInstance(Constance.SIGNATURE_IMG_FOLDER_PATH+signatureImg));
    	
    	// Sign
    	if(withTSA.equalsIgnoreCase(Boolean.TRUE.toString()) && Main.LicenseFile.getProgramEdition().equalsIgnoreCase(Constance.PROGRAM_EDITION_ENTERPRISE) && Main.LicenseFile.getProgramType().equalsIgnoreCase(Constance.PROGRAM_TYPE_PLUS)) {
    		MakeSignature.signDetached(appearance, new BouncyCastleDigest(), new PrivateKeySignature(privateKey, Constance.DIGEST_ALG, null), chain, null, null, new TSAClientBouncyCastle(tsaURL, tsaUser, tsaPassword), 0, CryptoStandard.CMS);
    	}
    	else {
    		MakeSignature.signDetached(appearance, new BouncyCastleDigest(), new PrivateKeySignature(privateKey, Constance.DIGEST_ALG, null), chain, null, null, null, 0, CryptoStandard.CMS);
    	}
    	
	    //rangeStream = appearance.getRangeStream();
	    
	    return bos.toByteArray();
	    //return tmp;
		
	}
	
	
	
}
