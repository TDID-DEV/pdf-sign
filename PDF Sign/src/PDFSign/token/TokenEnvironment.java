package PDFSign.token;

import iaik.pkcs.pkcs11.DefaultInitializeArgs;
import iaik.pkcs.pkcs11.Module;
import iaik.pkcs.pkcs11.Slot;
import iaik.pkcs.pkcs11.TokenException;
import iaik.pkcs.pkcs11.Module.WaitingBehavior;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import PDFSign.Constance;
import PDFSign.properties.PropertiesManagement;
import PDFSign.token.Token;


public class TokenEnvironment {
	
	private Slot ActiveSlot;
	private Module module;
	
	
	/*public static void installDll(String DllName,String ServerProtocol) {
		
		InputStream is;
		//String java_home = System.getProperty("java.home");
		String file_sep = System.getProperty("file.separator");
		
		try {
			//URL u=new URL(ServerProtocol+"://"+WcrAppletConst.WcrServerDomainName+"/"+"wcr"+"/"+"iaikDll"+"/"+DllName);
			URL u=new URL(ServerProtocol+"://"+WcrAppletConst.WcrServerDomainName+"/"+"wee"+"/"+"iaikDll"+"/"+DllName);
			
			is = u.openStream();		
			FileOutputStream fos =new FileOutputStream(System.getenv("windir")+"\\system32"+file_sep+DllName);
			
			byte[] buf = new byte[1024];

	        int len;
	 
	      while((len = is.read(buf)) > 0){
	 
	        fos.write(buf, 0, len);

	      }
	      
	      fos.close();
	      is.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
	}*/
	
	
	
	public ArrayList<String> FindAvailableDriverToken() {
		
		PropertiesManagement prop = new PropertiesManagement();
		ArrayList<String> AvailableDll = new ArrayList<String>();
		String tokenDLL = null;
		try {
			tokenDLL = prop.getProperties(Constance.PROVIDER_CON_PATH, "program.login.tokenDLL");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		File file = new File(tokenDLL);
		
		if(file.exists()) {
			//System.out.println(tokenDLL);
			AvailableDll.add(tokenDLL);
		
		}
		
		
		return AvailableDll;
	}
	
	
	
	public ArrayList<String> FindAvailableDriver(String dll) {
		
		ArrayList<String> AvailableDll = new ArrayList<String>();
	
		File file = new File(dll);
		
		if(file.exists()) {
			//System.out.println(dll);
			AvailableDll.add(dll);
		
		}
		
		
		return AvailableDll;
		
	}
	
	
	
	
	public void LoadDll(String ProviderDll) {
		
		try {
			module = Module.getInstance(ProviderDll);
			module.initialize (new DefaultInitializeArgs());	
					
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TokenException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	
	public Token ConnectToken() {
		
		try {
			
			try {
				Thread.sleep(1000);				// delay for wait when change dll 
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
			
			
			Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
			
			int slotIndex = slotsWithToken.length;
			
			if(slotIndex>=1) {
				ActiveSlot = slotsWithToken[0];	
				return new Token(ActiveSlot);
				
			}
			else
				return null;
				
				
		} catch (TokenException e) {
			//e.printStackTrace();
		} 
				
		return null;
		
	}
	
	
	
	
	public Token ConnectTokenWithActiveSlot() {
		
		return new Token(ActiveSlot);
		
	}
	
	
	
	
	
	public boolean TokenRemoveListener() {
		Slot slot;
		
		try {
			
			slot = module.waitForSlotEvent(WaitingBehavior.DONT_BLOCK , null);
			if(ActiveSlot.equals(slot)) {
				//System.out.println("Same Token");
				return true;
				
			}
			/*else 
				System.out.println("Not Same");*/
				
			
			
		} catch (TokenException e) {
			return false;
		} 
		
		return false;
		
	}
	
	
	
	
	public boolean TokenInsertListener() {
		
		try {
			module.waitForSlotEvent(WaitingBehavior.DONT_BLOCK , null);
			
			return true;
		
		} catch (TokenException e) {
			return false;
		} 
		
	}
	
	

	
	public void ClearSlotActive() {
		ActiveSlot=null;
		
	}
	
	
	
	
	public boolean isSlotActive() {
		if(ActiveSlot!=null)
			return true;
		else 
			return false;
		
	}
	
	
	
	public void SetSlotActive(Slot sl){
		this.ActiveSlot = sl;
		
	}
	
	
	
	
	public boolean isTokenOnSlot() {
		
		try {
			Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
			
			
			
			if(slotsWithToken.length>0) { 
				//System.out.println("slot with token");
				SetSlotActive(slotsWithToken[0]);
				return true;
			}
			
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return false;
		
	}
	
	
	
	public Slot[] getSlotsList() {
		
		Slot[] slotsWithToken = null;
		
		try {
			slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
			
		} catch (TokenException e) {
			e.printStackTrace();
		}
		
		return slotsWithToken;
		
	}
	
	
	
	public Slot getSlotWithTokenSerial(String tokenSerial){
		
		// Get slot with serial number
		Slot[] slotList = getSlotsList();
		Slot slot = null;
		
		for (int j = 0; j < slotList.length; j++) {
			
			try {
				
				if(slotList[j].getToken().getTokenInfo().getSerialNumber().trim().equals(tokenSerial)) {
					slot = slotList[j];
					break;
				}
				
			} catch (TokenException e) {
				e.printStackTrace();
			}
			
		}
		
		return slot;
		
	}
	
	
	
	public Module getModule() {
		
		return module;
		
	}
	
	
	
	
	public void DestroyModule() {
		
		try {
			module.finalize(null);
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
	}
	
	
	
	

}
