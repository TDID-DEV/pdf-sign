package PDFSign.token;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import PDFSign.Constance;
import iaik.pkcs.pkcs11.Slot;
import iaik.pkcs.pkcs11.Session;
import iaik.pkcs.pkcs11.TokenException;
import iaik.pkcs.pkcs11.objects.Object;
import iaik.pkcs.pkcs11.objects.X509PublicKeyCertificate;

public class Token extends iaik.pkcs.pkcs11.Token{
	
	private Session TokenSession;

	protected Token(Slot SlotWithToken) {
		super(SlotWithToken);
		try {
			TokenSession=super.openSession(Token.SessionType.SERIAL_SESSION,Token.SessionReadWriteBehavior.RO_SESSION,null,null);
			
			
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		// TODO Auto-generated constructor stub
	}
	
	public String Authentication(String InputPin) {
		
		
		try {
			TokenSession.login(Session.UserType.USER, InputPin.toCharArray());
			
			
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			
			return e.getMessage();
			//e.printStackTrace();
			
		} 
		
		return null;
		
	}


	public ArrayList<X509Certificate> getCertificate() {
		
		ArrayList<X509Certificate> cert = new ArrayList<X509Certificate>();
		X509PublicKeyCertificate tempCert = new X509PublicKeyCertificate();
		try {
			
			TokenSession.findObjectsInit(tempCert);
			iaik.pkcs.pkcs11.objects.Object[] foundCerts = TokenSession.findObjects(Constance.MAX_FIND_OBJECT);
			if(foundCerts.length>0) {
				for (int i = 0; i < foundCerts.length; i++) {
					X509PublicKeyCertificate foundCert= (X509PublicKeyCertificate) foundCerts[i];
					CertificateFactory certFac = CertificateFactory.getInstance("X.509");
					byte[] encCert = foundCert.getValue().getByteArrayValue();
					cert.add((X509Certificate)certFac.generateCertificate(new ByteArrayInputStream(encCert)));
				}
			}
		
			TokenSession.findObjectsFinal();
			
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		return cert;
		
	}
	

	public String getManufacture() {
		
		
		try {
			return super.getTokenInfo().getManufacturerID();
			
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		return null;
		
	}
	
	public String getSerialNumber() {
		try {
			return super.getTokenInfo().getSerialNumber();
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	public void LogoutSession() {
		try {
			TokenSession.logout();
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
	}
	
	public static String getNewPin(String tokkenNo) {
		String NewPin = "";
		Integer temp;
		
		for(int i=0;i<tokkenNo.length();i++) {
			temp=Character.getNumericValue(tokkenNo.charAt(i));
			if(temp==9)
				temp=0;
			else
				temp++;
			
			
			NewPin+=temp.toString();
			
		}	
		return NewPin;
	}
	
	
	public boolean IsUserPinLock() {
		
		try {
			
			boolean test=super.getTokenInfo().isUserPinLocked();
			//System.out.println(test);
			//System.out.println(super.getTokenInfo());
			return test;
			//return token.getTokenInfo().isUserPinLocked();
			
			
		} catch (TokenException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return true;
			
		}
		
		
	}
	
	
	public Object[] FindObject(Object ObjectTemplate) throws TokenException {
		
		Object[] matchingObjects;
		
		
		TokenSession.findObjectsInit(ObjectTemplate);
		matchingObjects = TokenSession.findObjects(Constance.MAX_FIND_OBJECT);
		TokenSession.findObjectsFinal();
		
		
		
		if (matchingObjects.length > 0) {
			
			return matchingObjects;
			
		
		}
		return null;
		
	}
	
	

}
