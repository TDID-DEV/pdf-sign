package PDFSign.token;

import iaik.pkcs.pkcs11.Slot;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;

import PDFSign.Main;
import PDFSign.properties.PropertiesManagement;
import sun.security.pkcs11.SunPKCS11;


public class TokenProvider {

	public TokenEnvironment tokenEnv = new TokenEnvironment();
	public Provider provider;
	
	
	public Provider addTokenProvider(String name, String DLL, String slotID) {
		
		String configs = "name=" + name + "\n"
	        + "library=" + DLL + "\n"
	        + "slot = " + slotID;
		
		ByteArrayInputStream bais = new ByteArrayInputStream(configs.getBytes());
		Provider provider = new SunPKCS11(bais);
		Security.addProvider(provider);
		
		return provider;
		
	}
	
	
	
	public ArrayList<Provider> addAllTokenProviderInProp() throws NumberFormatException, FileNotFoundException, IOException {
		
		PropertiesManagement prop = new PropertiesManagement();
		ArrayList<Provider> provider = new ArrayList<Provider>();
		
		// Add security provider
		int count = Integer.parseInt(prop.getProperties("digsig.count"));
		
		for (int i = 1; i <= count; i++) {
			
			String hsmSerial = prop.getProperties("digsig."+i+".hsmSerial").trim();
			
			if(getTokenProviderWithHSMSerial(hsmSerial) == null) {

				// Set token environment
				String hsmDLL = prop.getProperties("digsig."+i+".hsmDLL");
				tokenEnv.LoadDll(hsmDLL);
				
				// Get slot with token serial number
				Slot slot = tokenEnv.getSlotWithTokenSerial(hsmSerial);
				
				// Add token provider and create key store
				provider.add(addTokenProvider(hsmSerial, hsmDLL, Long.toString(slot.getSlotID())));
				
			}
			else {
				// Do nothing
			}
			
		}
		
		return provider;
		
	}
	
	
	
	public boolean addTokenProviderByRecordNumber(int recordNumber) throws FileNotFoundException, IOException {
		
		boolean result = false;
		PropertiesManagement prop = new PropertiesManagement();
		
		// Add security provider
		String hsmSerial = prop.getProperties("digsig."+recordNumber+".hsmSerial").trim();
		
		if(getTokenProviderWithHSMSerial(hsmSerial) == null) {

			// Set token environment
			String hsmDLL = prop.getProperties("digsig."+recordNumber+".hsmDLL");
			tokenEnv.LoadDll(hsmDLL);
			
			// Get slot with token serial number
			Slot slot = tokenEnv.getSlotWithTokenSerial(hsmSerial);
			
			// Add token provider and create key store
			provider = addTokenProvider(hsmSerial, hsmDLL, Long.toString(slot.getSlotID()));
			
			result = true;
			
		}
		else {
			// Do nothing
		}
		
		return result;
		
	}
	
	
	
	public void retryAddTokenProvider(Provider provider, int recordNumber) throws FileNotFoundException, IOException {
		
		/*PropertiesManagement prop = new PropertiesManagement();
		removeTokenProvider(provider);
		
		String hsmSerial = prop.getProperties("digsig."+recordNumber+".hsmSerial").trim();
		
		if(getTokenProviderWithHSMSerial(hsmSerial) == null) {

			// Set token environment
			String hsmDLL = prop.getProperties("digsig."+recordNumber+".hsmDLL");
			tokenEnv.LoadDll(hsmDLL);
			
			// Get slot with token serial number
			Slot slot = tokenEnv.getSlotWithTokenSerial(hsmSerial);
			
			// Add token provider and create key store
			addTokenProvider(hsmSerial, hsmDLL, Long.toString(slot.getSlotID()));
			
		}
		else {
			// Do nothing
		}*/
		
		
		PropertiesManagement prop = new PropertiesManagement();
		
		String hsmSerial = prop.getProperties("digsig."+recordNumber+".hsmSerial").trim();
		String hsmDLL = prop.getProperties("digsig."+recordNumber+".hsmDLL");
		
		int tokenProviderSize = Main.tokenProviderList.size();
		
		for (int i = 0; i < tokenProviderSize; i++) {
			
			if(Main.tokenProviderList.get(i).provider.getName().equalsIgnoreCase(provider.getName())) {
				Main.tokenProviderList.get(i).tokenEnv.DestroyModule();
				Main.tokenProviderList.get(i).tokenEnv.LoadDll(hsmDLL);
				
				if(Main.tokenProviderList.get(i).tokenEnv.isTokenOnSlot()) {
					
					
					
					Main.tokenProviderList.get(i).removeTokenProvider(provider);
					
					// Get slot with token serial number
					Slot slot = Main.tokenProviderList.get(i).tokenEnv.getSlotWithTokenSerial(hsmSerial);
					
					// Add token provider and create key store
					Main.tokenProviderList.get(i).addTokenProvider(hsmSerial, hsmDLL, Long.toString(slot.getSlotID()));
					break;
				}
				
			}
			
		}
		
	}
	
	
	
	public Provider getTokenProviderWithHSMSerial(String hsmSerial) {
		
		return Security.getProvider("SunPKCS11-"+hsmSerial);
		
	}
	
	
	
	public void removeTokenProvider(Provider provider) {
		
		Security.removeProvider(provider.getName());
		
	}
	
	
	
	public void removeThisTokenProvider() {
		
		Security.removeProvider(provider.getName());
		
	}
	
	

}
