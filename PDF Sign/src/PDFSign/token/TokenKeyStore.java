package PDFSign.token;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.cert.CertificateException;

public class TokenKeyStore {

	public KeyStore createTokenKeyStore(Provider provider, String password) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException{
		
		KeyStore ks = KeyStore.getInstance("PKCS11", provider);
		ks.load(null, password.toCharArray()); 
		
		return ks;
		
	}
	
	
}
