# README #

### repository นี้ใช้สำหรับอะไร ###

* Repository นี้ใช้สำหรับจัดเก็บ source code ของ PDF Sign

### ต้องทำอะไรบ้างในการ commit ###

* 1) ในการจะ Commit มาที่ REPO นี้จะต้องตรวจสอบดังนี้
* 1.1) หากมี folder เปล่าที่ต้องการ commit จะต้องสร้างไฟล์ .gitkeep ภายใน folder นั้น
* 1.2) เพิ่มไฟล์ .gitignore ภายใน root path ของโปรเจค โดยดูรายละเอียดได้ที่ [https://www.atlassian.com/git/tutorials/gitignore#git-ignore-patterns]
* 2) ใส่รายละเอียดในตอน commit ดังนี้
* 2.1) ในการ commit ครั้งแรก "Initial commit by Name @ 31/12/2560 23.59.59"
* 2.2) ในการ commit ครั้งต่อไป "commit by Name @ 31/12/2560 23.59.59"